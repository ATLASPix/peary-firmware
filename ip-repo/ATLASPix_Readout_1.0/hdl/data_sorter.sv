`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 02.03.2018 09:35:55
// Design Name: 
// Module Name: data_sorter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module data_sorter(
    input clk,
    input rst,
    
    input  trigger_armed,
    input  trigger_pulse,
    input  t0_in,
    output busy,
    
    input [1:0] rx_error_code, // i [1:0]
    input       frame_start,
    input       conf_ts_on_each_frame,
    
    input [31:0] trig_counter, // i [31:0]
    input [63:0] fpga_timestamp, // i [63:0]
    
    input [23:0] chip_timestamp, // i [23:0]
    input        chip_timestamp_valid,   // i 
    
    input [5:0] column_addr,    // i [5:0]
    input [8:0] row_addr,       // i [8:0]
    input [9:0] rise_timestamp, // i [9:0]
    input [5:0] fall_timestamp, // i [5:0]
    input       pixel_data_in_valid,     // i
    
    //input       conf_use_48bit_counter, // i
    
    output [31:0] data_out,
    output        data_out_valid,
    input         fifo_full,
    input         fifo_empty
    
    );



typedef enum {
        s_Init,
        s_Send_Data,
        s_Inner_TSWait,
        s_Inner_TS1,
        s_Inner_TS2,
        s_Inner_TS3,
        s_T0,
        s_Busy_Write,
        s_Busy_Flush,
        s_Busy_Wait
        //s_Busy_Clear
    } t_State;
    
t_State State, NextState;

logic output_fifo_full;
logic output_fifo_empty;
logic busy_sig;
//logic busy_set;
logic busy_reset;
logic t0_was;
logic t0_clear;
logic t0_prev;


logic trigger_data_lock;
//logic busy_timestamp_lock;

logic [31:0] data_out_reg;
logic data_write_enable;

logic [31:0] trig_counter_buf;
logic [63:0] fpga_timestamp_buf;
logic [23:0] fpga_timestamp_busy;
//logic        new_trig;
//logic        new_trig_hold;
//logic        new_trig_clear;

//logic timestamp_valid_prev;
//logic new_timestamp;
//logic new_timestamp_hold;
//logic new_timestamp_clear;

//logic chip_data_capture_enable;
logic [31:0] chip_data;
logic chip_data_available;
logic chip_data_read;


/*
logic [2:0] mux_out_select;
*/

assign data_out_valid    = data_write_enable;
assign data_out          = data_out_reg;
assign output_fifo_full  = fifo_full;
assign output_fifo_empty = fifo_empty;
assign busy              = busy_sig;

//assign chip_data_capture_enable = trigger_armed;


always_ff @(posedge clk) begin
        t0_prev <= t0_in;
end

always_ff @(posedge clk) begin
    if (t0_in && !t0_prev)
        t0_was <= 1'b1;
    if (t0_clear)
        t0_was <= 1'b0;
end


always_ff @(posedge clk) begin
    if (rst)
        State <= s_Init;
    else
        State <= NextState;
end


// Next state logic
always_comb begin
    // default is to stay in current state
    NextState = State;
    case (State)
        s_Init:
            if (output_fifo_full)
                NextState = s_Init;
            else
                NextState = s_Send_Data;
        
        s_Send_Data:
                if (trigger_pulse)
                    if (output_fifo_full)
                        NextState = s_Inner_TSWait;
                    else
                        NextState = s_Inner_TS1;
                else if (trigger_pulse)
                    if (output_fifo_full)
                        NextState = s_Inner_TSWait;
                    else
                        NextState = s_Inner_TS1;
                        
                else if (frame_start && conf_ts_on_each_frame)
                    if (output_fifo_full)
                        NextState = s_Inner_TS2;
                    else
                        NextState = s_Inner_TS3;
                else
                    if (output_fifo_full)
                        NextState = s_Busy_Write;
                    else
                        NextState = s_Send_Data;
        
        s_Inner_TSWait:
            if (output_fifo_full)
                NextState = s_Inner_TSWait;
            else
                NextState = s_Inner_TS1;
        
        s_Inner_TS1:
            if (output_fifo_full)
                NextState = s_Inner_TS1;
            else
                NextState = s_Inner_TS2;
                
        s_Inner_TS2:
            if (output_fifo_full) 
                NextState = s_Inner_TS2;
            else
                NextState = s_Inner_TS3;
        
        s_Inner_TS3: 
            if (output_fifo_full)
                NextState = s_Inner_TS3;
            else 
                if (t0_was)
                    NextState = s_T0;
                else 
                    if (busy_sig)
                        NextState = s_Busy_Write;
                    else
                        NextState = s_Send_Data;
        s_T0: 
            if (output_fifo_full)
                NextState = s_T0;
            else 
                if (busy_sig)
                    NextState = s_Busy_Write;
                else
                    NextState = s_Send_Data;

        s_Busy_Write:
            if (output_fifo_full)
                NextState = s_Busy_Write;
            else
                NextState = s_Busy_Flush;

        s_Busy_Flush:
            if (chip_data_available)
                NextState = s_Busy_Flush;
            else
                NextState = s_Busy_Wait;
                
        s_Busy_Wait:
            if (output_fifo_empty)
                NextState = s_Init;
            else
                NextState = s_Busy_Wait;
            
        default:
            NextState = s_Init;
    endcase
end




// output logic 
always_comb begin
    // default values
    //busy_set          = 1'b0;
    busy_reset          = 1'b0;
    chip_data_read      = 1'b0;
    trigger_data_lock   = 1'b0;
    data_write_enable   = 1'b0;
    //busy_timestamp_lock = 1'b0;
    data_out_reg        = 32'bX;
    t0_clear            = 1'b0;
    
    
    case (State)
        s_Init: 
            begin
                data_write_enable = 1'b0;
                /*
                if (output_fifo_full || rst)
                    begin
                        data_write_enable = 1'b0;
                    end
                else
                    begin
                        data_out_reg = {8'b00000110, fpga_timestamp_busy[23:0]}; // BUSY was de-asserted + LSBs of FPGA timestamp when it happened
                        data_write_enable = 1'b1;
                    end
                    */
            end
            
        s_Send_Data:
                begin
                    if (trigger_pulse) // new trigger came, we need to send trig. counter and timestamp
                        begin
                            trigger_data_lock = 1'b1; // freeze the trigger timestamp and counter
                            // sending FIRST word of a TRIGGER ID - low bits of a trigger counter
                            data_out_reg = {8'b00010000, trig_counter_buf[23:0]}; // OUTPUT trigger counter from FPGA [23:0]  
                            if (output_fifo_full)
                                begin
                                    //busy_set          = 1'b1;
                                    data_write_enable   = 1'b0;
                                end
                            else
                                begin
                                    data_write_enable   = 1'b1;
                                end
                        end
                    
                    else if (frame_start && conf_ts_on_each_frame) // send only 48 LS bits of FPGA timestamp
                        begin 
                            trigger_data_lock = 1'b1;
                            // sending THIRD word of a TRIGGER ID 
                            data_out_reg = {8'b00100000, fpga_timestamp_buf[47:24]}; // OUTPUT timestamp from FPGA [47:24] 
                            if (output_fifo_full)
                                begin
                                    //busy_set          = 1'b1;
                                    data_write_enable = 1'b0;
                                end
                            else
                                begin
                                    data_write_enable = 1'b1;
                                end
                        end
                    
                    else  //-> if(!trigger_pulse), output chip data
						begin
							// sending CHIP DATA from a buffer: 
							data_out_reg = chip_data;
							if (output_fifo_full) // no space to write 
								begin
									//busy_set          = 1'b1;
									data_write_enable = 1'b0;
								end
							else // write chip data to fifo
								begin
									data_write_enable = chip_data_available; // set write enable only when there is data available
									chip_data_read    = chip_data_available;
								end
						end
                end

        s_Inner_TSWait:
            // The FIFO was full, we are waiting for some space to write in it...
            // The behavior of this state is the same as in s_Send_Data, when trigger_pulse == 1'b1.
            begin 
                trigger_data_lock = 1'b1; // freeze the trigger timestamp and counter
                // sending FIRST word of a TRIGGER ID - low bits of a trigger counter
                data_out_reg = {8'b00010000, trig_counter_buf[23:0]}; // OUTPUT trigger counter from FPGA [23:0]  
                if (output_fifo_full)
                    begin
                        //busy_set          = 1'b1;
                        data_write_enable = 1'b0;
                    end
                else
                    begin
                        data_write_enable = 1'b1;
                    end
            end
                
        s_Inner_TS1:
            begin
                trigger_data_lock = 1'b1;
                // sending SECOND word of a TRIGGER ID  
                data_out_reg = {8'b00110000, trig_counter_buf[31:24], fpga_timestamp_buf[63:48]}; // OUTPUT trigger counter from FPGA [31:24] and timestamp from FPGA [63:48] 
                if (output_fifo_full)
                    begin
                        //busy_set          = 1'b1;
                        data_write_enable = 1'b0;
                    end
                else
                    begin
                        data_write_enable = 1'b1;
                    end
            end
            
        s_Inner_TS2:
            begin 
                trigger_data_lock = 1'b1;
                // sending THIRD word of a TRIGGER ID 
                data_out_reg = {8'b00100000, fpga_timestamp_buf[47:24]}; // OUTPUT timestamp from FPGA [47:24] 
                if (output_fifo_full)
                    begin
                        //busy_set          = 1'b1;
                        data_write_enable = 1'b0;
                    end
                else
                    begin
                        data_write_enable = 1'b1;
                    end
            end
        s_Inner_TS3:
            begin
                trigger_data_lock = 1'b1;
                // sending FOURTH word of a TRIGGER ID 
                data_out_reg = {8'b01100000, fpga_timestamp_buf[23:0]}; // OUTPUT timestamp from FPGA [23:0] 
                if (output_fifo_full)
                    begin
                        //busy_set          = 1'b1;
                        data_write_enable = 1'b0;
                    end
                else
                    begin
                        data_write_enable = 1'b1;
                    end
            end
        s_T0:
            begin
                data_out_reg = {8'b01110000, fpga_timestamp_buf[23:0]}; // OUTPUT T0 received flag 
                if (output_fifo_full)
                    begin
                        //busy_set          = 1'b1;
                        data_write_enable = 1'b0;
                    end
                else
                    begin
                        data_write_enable = 1'b1;
                        t0_clear          = 1'b1;
                    end
            end
        
        s_Busy_Write:    
            begin
                // sending information that there was a BUSY signal triggered 
                data_out_reg = {8'b00000010, fpga_timestamp_busy[23:0]}; // BUSY was asserted due to FIFO_FULL + LSBs of FPGA timestamp when it happened 
                 if (!output_fifo_full)
                    begin
                        data_write_enable = 1'b1;
                    end               
            end
            
        s_Busy_Flush:
            begin
                data_out_reg      = chip_data;
                if (!output_fifo_full)
                    begin
                        data_write_enable = chip_data_available;
                        chip_data_read    = chip_data_available;
                    end
            end

        s_Busy_Wait:
            begin
                if (output_fifo_empty)
                    begin
                        busy_reset        = 1'b1;
                    end
            end
            
        default:
            begin
                
            end

    endcase
end


// capture the counters when trigger arrives
always_ff @(posedge clk) begin
    if (rst)
        begin
            trig_counter_buf    <= 32'b0;
            fpga_timestamp_buf  <= 64'b0;
        end
    else 
        if (!trigger_data_lock) // && trigger_pulse)
            begin
                trig_counter_buf    <= trig_counter;
                fpga_timestamp_buf  <= fpga_timestamp;
            end
        else 
            begin
                trig_counter_buf   <= trig_counter_buf;
                fpga_timestamp_buf <= fpga_timestamp_buf;
            end 
end

// capture timestamp when fifo becomes full
always_ff @(posedge clk) begin
    if (rst)
        begin
            fpga_timestamp_busy <= 24'b0;
        end
    else 
        if (!busy_sig)
            begin
                fpga_timestamp_busy <= fpga_timestamp[23:0];        
            end
        else 
            begin
                fpga_timestamp_busy   <= fpga_timestamp_busy;
            end 
end

// busy flag handling
always_ff @(posedge clk) begin
    if (rst)
        busy_sig  <= 1'b0;
    else 
        if (output_fifo_full)//(busy_set)
            busy_sig  <= 1'b1;
        else if (busy_reset)
            busy_sig  <= 1'b0;
        else 
            busy_sig  <= busy_sig;
end





chip_data_buf data_buf_inst(
    .clk(clk), // i
    .rst(rst), // i
    .trig_armed(trigger_armed), // i
    .readout_busy(busy_sig), //i
    .rx_error_code(rx_error_code),    
    
    .chip_data_read(chip_data_read),  // i 
    .chip_data_out(chip_data),  // o [31:0] 
    .chip_data_valid(chip_data_available), // o
    
    .readout_timestamp(chip_timestamp),     // i [23:0]
    .timestamp_valid(chip_timestamp_valid), // i 
    
    .column_addr(column_addr),              // i [5:0]
    .row_addr(row_addr),                    // i [8:0]
    .rise_timestamp(rise_timestamp),        // i [9:0]
    .fall_timestamp(fall_timestamp),        // i [5:0]
    .pixel_data_valid(pixel_data_in_valid)  // i
);

/*

logan logan_inst (
	.clk(clk), // input wire clk

	.probe0(data_out_reg), // input wire [31:0]  probe0  
	.probe1(chip_data), // input wire [31:0]  probe1 
	.probe2(data_write_enable), // input wire [0:0]  probe2 
	.probe3(chip_data_available), // input wire [0:0]  probe3  
	.probe4(chip_data_read), // input wire [0:0]  probe4 
	.probe5(output_fifo_full), // input wire [0:0]  probe5 
	.probe6(trigger_pulse), // input wire [0:0]  probe6 
	.probe7(busy_sig), // input wire [0:0]  probe7 
	.probe8(output_fifo_empty), // input wire [0:0]  probe8 
	.probe9(trigger_armed), // input wire [0:0]  probe9 
	.probe10(fpga_timestamp_buf), //  
	.probe11(chip_timestamp), //  
	.probe12(chip_timestamp_valid), //  
	.probe13(frame_start), //  
	.probe14(trigger_data_lock) //  
	

);

*/

endmodule