`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2018 15:15:00
// Design Name: 
// Module Name: pulse_sync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module delay_reset_sync
(
    input clk_a,
    input clk_b,
    input rst_rq,
    input rst_b_hold_release,         
    output rst_out_a,  
    output rst_out_b_delayed    
    );

(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic reset_a;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_ax1;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_ax2;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_ax3;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_ax4;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_bx1;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_bx2;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_bx3;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_bx4;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_by1;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_by2;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_by3;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_by4;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_by5;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic rst_sync_by6;


    always_ff @(posedge clk_a, posedge rst_rq) 
        begin
            if (rst_rq == 1'b1)
                begin
                    rst_sync_ax1 <= 1'b1;
                    rst_sync_ax2 <= 1'b1;
                    rst_sync_ax3 <= 1'b1;
                    rst_sync_ax4 <= 1'b1;
                end
            else 
                begin
                    rst_sync_ax1 <= 1'b0;
                    rst_sync_ax2 <= rst_sync_ax1;
                    rst_sync_ax3 <= rst_sync_ax2;
                    rst_sync_ax4 <= rst_sync_ax3;
                end 
        end 

    always_ff @(posedge clk_b, posedge rst_rq) 
        begin
            if (rst_rq == 1'b1)
                begin
                    rst_sync_bx1 <= 1'b1;
                    rst_sync_bx2 <= 1'b1;
                    rst_sync_bx3 <= 1'b1;
                    rst_sync_bx4 <= 1'b1;
                end
            else 
                begin
                    rst_sync_bx1 <= 1'b0;
                    rst_sync_bx2 <= rst_sync_bx1;
                    rst_sync_bx3 <= rst_sync_bx2;
                    rst_sync_bx4 <= rst_sync_bx3;
                end 
        end 

assign reset_a = rst_sync_bx4 || rst_sync_ax4 || rst_rq;


    always_ff @(posedge clk_b, posedge reset_a) 
        begin
            if (reset_a == 1'b1) 
                begin
                    rst_sync_by1 <= 1'b1;
                    rst_sync_by2 <= 1'b1;
                    rst_sync_by3 <= 1'b1;
                    rst_sync_by4 <= 1'b1;
                    rst_sync_by5 <= 1'b1;
                    rst_sync_by6 <= 1'b1;
                end
            else if (rst_b_hold_release == 1'b0)
                begin
                    rst_sync_by1 <= 1'b0;
                    rst_sync_by2 <= rst_sync_by1;
                    rst_sync_by3 <= rst_sync_by2;
                    rst_sync_by4 <= rst_sync_by3;
                    rst_sync_by5 <= rst_sync_by4;
                    rst_sync_by6 <= rst_sync_by5;
                end 
        end 

assign rst_out_a = reset_a;
assign rst_out_b_delayed = rst_sync_by6;


endmodule
