
`timescale 1 ns / 1 ps

 module ATLASPix_Readout_v1_0 #
	(
		// Users to add parameters here
		
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 5
	)
	(
		// Users to add ports here
        
        input  wire pdata_clk, // // parallel data (slow) clock input
        //output wire readout_rst_out,

        // data from ATLASPix:
        input  wire [8:0] decoded_atp_data_in,
        input  wire rx_locked,

        input  wire trigger_in,        // trigger input
        input  wire t0_in,  
        output wire t0_out,  
        
        //output wire [31:0] data_out,
        output wire busy_out,
        
        output wire reset_out,
        
        //output wire conf_clock_source,
        
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
	
    //wire rst_pdata_clk;
    
    wire readout_reset_request;
    wire readout_reset_request_pulse;
    
    wire receiver_reset_request;
    wire receiver_reset_request_pulse;
    
    wire reset_fifo;
    wire reset_readout_sync_pdata;
    wire reset_receiver_sync_pdata;
        
    wire conf_enable_sync_axi;
    wire conf_enable_sync_pdata;
    
    wire conf_busy_when_armed_sync_axi;
    wire conf_busy_when_armed_sync_pdata;
                
    wire conf_t0_enable_sync_axi;
    wire conf_t0_enable_sync_pdata;
            
    wire conf_trig_enable_sync_axi;
    wire conf_trig_enable_sync_pdata;
    
    wire conf_trig_edge_sel_sync_axi;
    wire conf_trig_edge_sel_sync_pdata;
    
    wire conf_trig_always_armed_sync_axi;
    wire conf_trig_always_armed_sync_pdata;

    wire conf_trig_count_rst_sync_axi;
    wire conf_trig_count_rst_sync_pdata;
    
    wire conf_enable_gray_decode_sync_axi;
    wire conf_enable_gray_decode_sync_pdata;    

    wire conf_ts_on_each_frame_sync_axi;
    wire conf_ts_on_each_frame_sync_pdata;
        
    wire [23:0] conf_trig_duration_sync_axi;
    wire [23:0] conf_trig_duration_sync_pdata;
            
    wire [7:0] conf_timestamp_div_sync_axi;
    wire [7:0] conf_timestamp_div_sync_pdata;

	wire fifo_empty_sync_axi;
	wire fifo_empty_sync_pdata;
	
	//wire reset_busy_sync_axi;
	//wire reset_busy_sync_pdata;
	
	wire        fifo_full;
	wire [31:0] fifo_data_in;
	wire        fifo_data_write;
	wire [31:0] fifo_data_out;
	wire        fifo_data_read;
	
	wire t0_pulse;
	wire busy_out_sig;
	
    assign readout_reset_request  = readout_reset_request_pulse || (!s00_axi_aresetn);
    assign receiver_reset_request = readout_reset_request || receiver_reset_request_pulse;
    
    assign reset_out = reset_receiver_sync_pdata;
    assign busy_out  = busy_out_sig;

// Instantiation of Axi Bus Interface S00_AXI
	ATLASPix_Readout_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) AXI_interface_inst (

        
        .fifo_data_out           (fifo_data_out),                    // i [31:0] data from FIFO to be read by software 
        .fifo_data_out_valid     (!fifo_empty_sync_axi),             // i flag signalising that the fifo_data_out can be read
        .stat_rx_locked          (rx_locked),                        // i 1 = receiver serdes locked
        .stat_busy               (busy_out_sig),                     // i 1 = readout busy
        .fifo_data_read          (fifo_data_read),                   // o read acknowledge flag - next data can be asserted
        .readout_reset           (readout_reset_request_pulse),      // o reset pulse (1 clk period) 
        .receiver_reset          (receiver_reset_request_pulse),     // o reset pulse (1 clk period) 
        .conf_enable             (conf_enable_sync_axi),             // o 1 = enable readout, 0 = no output is given
        .conf_busy_when_armed    (conf_busy_when_armed_sync_axi),    // o 1 = busy is asserted also when trigger is armed, 0 = busy is asserted only when FIFO is full
        .conf_t0_enable          (conf_t0_enable_sync_axi),          // o 1 = enable timer counter reset on t0 signal, 0 = ignore t0 input signal
        .conf_trig_enable        (conf_trig_enable_sync_axi),        // o 1 = enable triggered readout, 0 = trigger input is ignored (also no trig. counting)
        .conf_trig_edge_sel      (conf_trig_edge_sel_sync_axi),      // o 1 = trigger on falling edge, 0 = trigger on rising edge
        .conf_trig_always_armed  (conf_trig_always_armed_sync_axi),  // o 1 = trigger always on, all data is stored
        .conf_trig_count_rst     (conf_trig_count_rst_sync_axi),     // o trigger counter reset flag
        //.conf_clock_source       (conf_clock_source),                // o configure clock source for pll: 0 = internal oscillator, 1 = external TLU clock
        .conf_enable_gray_decode (conf_enable_gray_decode_sync_axi), // o 1 = gray-encoded data are decoded in hardware
        .conf_ts_on_each_frame   (conf_ts_on_each_frame_sync_axi),   // 1 = FPGA timestamp will be set each time a chip starts a transmission
        .conf_trig_duration      (conf_trig_duration_sync_axi),      // o [23:0] number of timestamp cycles for which the trigger is active (armed)
        .conf_timestamp_div      (conf_timestamp_div_sync_axi),      // o [7:0] divides the speed of timestamp counter with respect to pdata_clk
        	
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);

// Add user logic here

// XXX !!!! add pulse generator
//assign t0_pulse = t0_in;

receive_decode receive_decode_inst(
        .pdata_clk(pdata_clk), // the whole block runs with paralel data clock, posedge active
        .rst(reset_readout_sync_pdata),       // sync. reset, active high
        
        .trigger_in(trigger_in), // trigger signal input
        .busy_out(busy_out_sig),   // busy signal output
    
        .t0_in(t0_in),        // resets time counters
        .t0_out(t0_out),        // resets chip time counters
        //sync_counter_reset, // sync pulse each time the internal time couner overflows
        
        .pdata_in(decoded_atp_data_in),  // [8:0] deserialised and decoded data from the chip including the control word flag (bit 8)
        .rx_locked(rx_locked), // flag signalising that the SERDES receiver is locked and providing valid data (i.e. pdata_in is valid)
        
        .data_out       (fifo_data_in),          // [31:0] processed data to be stored to FIFO 
        .data_out_valid (fifo_data_write),       // output flag signalising that the data_out is valid should be stored (to be connected to wr_en)
        .fifo_full      (fifo_full),             // input flag signalising that the output fifo is full (no data_out_valid should be asserted)
        .fifo_empty     (fifo_empty_sync_pdata), // input flag signalising that the output fifo is emty (busy flag can be removed)
        
        .conf_enable                  (conf_enable_sync_pdata),                   // 1 = enable readout, 0 = no output is given
        .conf_busy_when_armed         (conf_busy_when_armed_sync_pdata),          // 1 = busy is asserted also when trigger is armed, 0 = busy is asserted only when FIFO is full
        .conf_t0_enable               (conf_t0_enable_sync_pdata),                // 1 = enable timer counter reset on t0 signal, 0 = ignore t0 input signal
        .conf_trig_enable             (conf_trig_enable_sync_pdata),              // 1 = enalbe triggered readout, 0 = read all
        .conf_trig_edge_sel           (conf_trig_edge_sel_sync_pdata),            // 1 = trigger on falling edge, 0 = trigger on rising edge
        .conf_trig_always_armed       (conf_trig_always_armed_sync_pdata),        // 1 = trigger always on, all data are stored
        .conf_trig_count_rst          (conf_trig_count_rst_sync_pdata),           // trigger counter reset flag
        .conf_enable_gray_decode      (conf_enable_gray_decode_sync_pdata),       // 1 = gray-encoded data are decoded in hardware
        .conf_ts_on_each_frame        (conf_ts_on_each_frame_sync_pdata) ,        // 1 = FPGA timestamp will be set each time a chip starts a transmission
        .conf_trig_duration           (conf_trig_duration_sync_pdata),            // [23:0] number of timestamp cycles for which the trigger is active (armed)
        .conf_timestamp_div           (conf_timestamp_div_sync_pdata)             // [7:0] divides the speed of timestamp counter with respect to pdata_clk
        
);

/*
reg [31:0] dbg_counter;
reg dbg_fifowrite;

always@(posedge pdata_clk) begin
    dbg_fifowrite <= 1'b0;
    dbg_counter    <= dbg_counter;

    if (reset_readout_sync_pdata) 
        begin
            dbg_counter    <= 32'b0;
            dbg_fifowrite <=  1'b0;
        end
    else
        begin
            if (!fifo_full)
                begin
                    dbg_fifowrite <= 1'b1;
                    dbg_counter <= dbg_counter + 1;
                end
        end
end
*/

data_output_fifo data_output_fifo_inst(
    .wr_clk(pdata_clk),             //: IN STD_LOGIC;
    .rst(reset_fifo), //: IN STD_LOGIC;
    .rd_clk(s00_axi_aclk),          //: IN STD_LOGIC;
    .din(fifo_data_in),             //: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    .wr_en(fifo_data_write),        //: IN STD_LOGIC;
    //.din(dbg_counter),             //: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    //.wr_en(dbg_fifowrite),        //: IN STD_LOGIC;
    .rd_en(fifo_data_read),         //: IN STD_LOGIC;
    .dout(fifo_data_out),           //: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    .full(fifo_full),               //: OUT STD_LOGIC;
    .empty(fifo_empty_sync_axi)     //: OUT STD_LOGIC
);        

// sync of configuration/control signals from AXI bus to PDATA clock domain
sync_signal  # ( 
    .WIDTH(8),
    .STAGES(2)
) conf_syncer(
    .out_clk(pdata_clk),
    .signal_in ({conf_enable_sync_axi,   conf_busy_when_armed_sync_axi,   conf_t0_enable_sync_axi,   conf_trig_enable_sync_axi,   conf_trig_edge_sel_sync_axi,   conf_trig_always_armed_sync_axi,   conf_enable_gray_decode_sync_axi,   conf_ts_on_each_frame_sync_axi}),   
    .signal_out({conf_enable_sync_pdata, conf_busy_when_armed_sync_pdata, conf_t0_enable_sync_pdata, conf_trig_enable_sync_pdata, conf_trig_edge_sel_sync_pdata, conf_trig_always_armed_sync_pdata, conf_enable_gray_decode_sync_pdata, conf_ts_on_each_frame_sync_pdata})  
);

/*
sync_signal syncer_conf_enable(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(conf_enable_sync_axi),    // i, sync to wr_clk 
    .signal_out(conf_enable_sync_pdata)  // o, sync to rd_clk
);

sync_signal syncer_conf_busy_when_armed(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(conf_busy_when_armed_sync_axi),    // i, sync to wr_clk 
    .signal_out(conf_busy_when_armed_sync_pdata)  // o, sync to rd_clk
);

sync_signal syncer_conf_t0_enable(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(conf_t0_enable_sync_axi),    // i, sync to wr_clk 
    .signal_out(conf_t0_enable_sync_pdata)  // o, sync to rd_clk
);

sync_signal syncer_conf_trig_enable(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(conf_trig_enable_sync_axi),    // i, sync to wr_clk 
    .signal_out(conf_trig_enable_sync_pdata)  // o, sync to rd_clk
);

sync_signal syncer_conf_trig_edge_sel(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(conf_trig_edge_sel_sync_axi),    // i, sync to wr_clk 
    .signal_out(conf_trig_edge_sel_sync_pdata)  // o, sync to rd_clk
);

sync_signal syncer_conf_trig_always_armed(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(conf_trig_always_armed_sync_axi),    // i, sync to wr_clk 
    .signal_out(conf_trig_always_armed_sync_pdata)  // o, sync to rd_clk
);

*/

sync_pulse syncer_conf_trig_count_rst(
    .in_clk(s00_axi_aclk),
    .out_clk(pdata_clk),
    .pulse_in(conf_trig_count_rst_sync_axi),    // i, sync to wr_clk 
    .pulse_out(conf_trig_count_rst_sync_pdata)  // o, sync to rd_clk
);

sync_bus #(
    .WIDTH(24),
    .STAGES(2)
) syncer_conf_trig_duration (
    .in_clk(s00_axi_aclk),
    .out_clk(pdata_clk),
    .bus_in(conf_trig_duration_sync_axi),
    .bus_out(conf_trig_duration_sync_pdata)
);

sync_bus #(
    .WIDTH(8),
    .STAGES(2)
) syncer_conf_timestamp_div (
    .in_clk(s00_axi_aclk),
    .out_clk(pdata_clk),
    .bus_in(conf_timestamp_div_sync_axi),
    .bus_out(conf_timestamp_div_sync_pdata)
);

sync_reset syncer_receiver_reset(
    .clk(pdata_clk),
    //.rd_clk(pdata_clk),
    .arst_in(receiver_reset_request),    // i, sync to wr_clk 
    .rst_out(reset_receiver_sync_pdata)  // o, sync to rd_clk
);

sync_signal syncer_fifo_empty(
    .out_clk(s00_axi_aclk),
    //.rd_clk(pdata_clk),
    .signal_in(fifo_empty_sync_axi),    // i, sync to wr_clk 
    .signal_out(fifo_empty_sync_pdata)  // o, sync to rd_clk
);


delay_reset_sync syncer_readout_reset (
    .clk_a(s00_axi_aclk),
    .clk_b(pdata_clk),
    .rst_rq(readout_reset_request),    
    .rst_b_hold_release(fifo_full),    
    .rst_out_a(reset_fifo),
    .rst_out_b_delayed(reset_readout_sync_pdata)
);



/*

ila_ATP_Readout ATP_Readout_LogicAnalyzer (
	.clk(pdata_clk), // input wire clk

	.probe0(decoded_atp_data_in), // input wire [8:0]  probe0  
	.probe1(rx_locked), // input wire [0:0]  probe1
    .probe2(fifo_data_in),          // [31:0] processed data to be stored to FIFO 
    .probe3(fifo_data_write),       // output flag signalising that the data_out is valid should be stored (to be connected to wr_en)
    .probe4(fifo_full),             // input flag signalising that the output fifo is full (no data_out_valid should be asserted)
    .probe5(fifo_empty_sync_pdata)

	
);

*/
// User logic ends




	endmodule
