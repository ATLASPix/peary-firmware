`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.03.2018 18:00:59
// Design Name: 
// Module Name: fpga_timestamp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fpga_timestamp(
    input clk,
    input rst,
    
    input t0_in,
    output t0_out,
    
    input enable,
    //input t0_enable,
    output [63:0] timestamp,
    input [7:0] timestamp_div
);
    
    

logic [63:0] timestamp_reg;
logic t0_out_sig;
logic t0_in_sig;
    
assign t0_in_sig = t0_in; //&& t0_enable;
assign t0_out    = t0_out_sig;
assign timestamp = timestamp_reg;
    
always_ff @(posedge clk) begin
    if (rst || t0_in_sig)
        timestamp_reg <= 64'b0;
    else
        if (enable)
            timestamp_reg <= timestamp_reg + 1;
        else
            timestamp_reg <= timestamp_reg; 
end  
  
always_comb begin
    if ((timestamp_reg[23:0] == 24'hffffff) || t0_in_sig ) //|| (timestamp_reg[23:0] == 64'hfffffe) ))
        t0_out_sig = 1'b1;
    else
        t0_out_sig = 1'b0;
end  
 
    
    
    
endmodule
