`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 
// Design Name: 
// Module Name: sync_reset
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// reset sync
module sync_reset #(
    parameter NEGATIVE_IN = 0,
    parameter STAGES      = 2 
)(
    input  wire clk,
    input  wire arst_in,  // i, async 
    output wire rst_out   // o, sync to clk
    );



    (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) reg reset_sync [STAGES : 0]; // = {{STAGES{0}}};;
    
    wire arst;
    
    //VIVADO does not support assertions... #%*#&$!
    //CHECK_MIN_STAGES: assert (STAGES >= 2) else $error("STAGES must be >= 2");   

    generate
        if (NEGATIVE_IN == 1)
            always @(posedge clk, negedge arst_in) 
                if (!arst_in)
                    reset_sync[0] <= 1'b1;
                else
                    reset_sync[0] <= 1'b0;
        else
            always @(posedge clk, posedge arst_in) 
                if (arst_in)
                    reset_sync[0] <= 1'b1;
                else
                    reset_sync[0] <= 1'b0;
    
    
    endgenerate
    
    genvar i;
    generate
        for (i=1; i <= STAGES; i=i+1)
            if (NEGATIVE_IN == 1)
                always @(posedge clk, negedge arst_in) 
                    if (!arst_in)
                        reset_sync[i] <= 1'b1;
                    else
                        reset_sync[i] <= reset_sync[i-1];
            else 
                always @(posedge clk, posedge arst_in) 
                    if (arst_in)
                        reset_sync[i] <= 1'b1;
                    else
                        reset_sync[i] <= reset_sync[i-1];
    endgenerate

    assign rst_out = reset_sync[STAGES];
    
endmodule
