`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 22.02.2018 15:23:53
// Design Name: 
// Module Name: trigger
// Project Name: ATLASPIX Data Readout Module IP core 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
      

module trigger(
    input clk,   // clk
    input rst,   // reset

    //input         enable,
    
    input         trigger_in,      // trigger signal - must be synchronous to clk   
    input         count_enable,         // count enable input. if 0, does not register the trigger.  
    input         busy_fifo_full,         // busy signal input - in case it is 1, triggers are ignored - must be sync. to clk 
    output [31:0] trigger_counter, // output of the trigger counter value
    output        trigger_armed,   // 1'b1 when trigger is active and data can be stored
    output        trigger_pulse_out,   // 1'b1 when trigger is active and data can be stored
    
    
	input         conf_trig_edge_sel,     // 1 = rising edge active, 0 = falling edge active
	input         conf_trig_enable,       // 0 = trigger input is ignored
	input  [23:0] conf_trig_duration,     // 24 bits
	input         conf_trig_always_armed, // 1 = trigger always on, all data are stored
	input         conf_trig_count_rst     // trigger counter reset flag
    );




logic [31:0] trig_counter_reg;
logic [23:0] arm_counter_reg;
logic        armed;

logic trigger_pulse;
logic trigger_prev;


assign trigger_counter   = trig_counter_reg;
assign trigger_pulse_out = trigger_pulse;

assign trigger_armed = armed; 
assign armed = ((|arm_counter_reg) || (conf_trig_always_armed && !busy_fifo_full)); // if arm_counter!=0, trigger is armed. Can be also armed manually


// trigger active edge detector
always_ff @(posedge clk) begin
    trigger_prev <= trigger_in;
end

// if trigger is enabled and readout is not busy and if the active edge comes, trigger_pulse = 1
assign trigger_pulse = (conf_trig_enable && !busy_fifo_full && (trigger_in^~conf_trig_edge_sel) && (trigger_prev^conf_trig_edge_sel));
/*
//the above assign should be same as:
always_comb begin
    if (conf_trig_enable && !busy_fifo_full) // only if trigger is enabled and readout is not busy
        if ((trigger_in == conf_trig_edge_sel) && (trigger_prev == ~conf_trig_edge_sel)) // if the active edge comes
            trigger_pulse = 1'b1;
        else
            trigger_pulse = 1'b0;
     else
        trigger_pulse = 1'b0;
end
*/

// trigger counter    
always_ff @(posedge clk) begin
    if (rst || conf_trig_count_rst)
        trig_counter_reg <= 32'b0;
    else
        if (trigger_pulse && count_enable)
            trig_counter_reg <= trig_counter_reg + 1;
        else
            trig_counter_reg <= trig_counter_reg;
end


// trigger arm duration counter    
always_ff @(posedge clk) begin
    if (rst)
        arm_counter_reg <= 24'b0;
    else
        if (trigger_pulse)
            arm_counter_reg <= conf_trig_duration;
        else
            if (|arm_counter_reg) // if (arm_counter_reg != 0) 
                arm_counter_reg <= arm_counter_reg - 1;
            else
                arm_counter_reg <= 24'b0; 
end



endmodule