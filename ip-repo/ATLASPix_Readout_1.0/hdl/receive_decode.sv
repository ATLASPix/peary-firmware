`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 22.02.2018 10:44:53
// Design Name: 
// Module Name: receive_decode
// Project Name: ATLASPIX Data Readout Module IP core 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module receive_decode(
    input pdata_clk, // the whole block runs with parallel data clock, posedge active
    input rst,       // sync. reset, active high
    
    input  trigger_in, // trigger signal input
    output busy_out,   // busy signal output

    input  t0_in,          // resets time counters
    output t0_out,         // resets chip time counters
    //output sync_counter_reset, // sync pulse each time the internal time counter overflows
    
    input  [8:0] pdata_in,  // deserialised and decoded data from the chip including the control word flag (bit 8)
    input        rx_locked, // flag signalizing that the SERDES receiver is locked and providing valid data (i.e. pdata_in is valid)
    
    output [31:0] data_out,       // processed data to be stored to FIFO 
    output        data_out_valid, // output flag signalizing that the data_out is valid should be stored (to be connected to wr_en)
    input         fifo_full,      // input flag signalizing that the output fifo is full (no data_out_valid should be asserted)
    input         fifo_empty,     // input flag signalizing that the output fifo is empty (busy flag can be removed)
    
    input        conf_enable,                  // 1 = enable readout, 0 = no output is given
    input        conf_busy_when_armed,         // 1 = busy is asserted also when trigger is armed, 0 = busy is asserted only when FIFO is full
    input        conf_t0_enable,               // 1 = enable timer counter reset on t0 signal, 0 = ignore t0 input signal
    input        conf_trig_enable,             // 1 = enable triggered readout, 0 = trigger input is ignored (also no trig. counting)
    input        conf_trig_edge_sel,           // 1 = trigger on falling edge, 0 = trigger on rising edge
    input        conf_trig_always_armed,       // 1 = trigger always on, all data is stored
    input        conf_trig_count_rst,          // trigger counter reset flag
    input        conf_enable_gray_decode,      // 1 = gray-encoded data are decoded in hardware
    input        conf_ts_on_each_frame,        // 1 = FPGA timestamp will be set each time a chip starts a transmission
    input [23:0] conf_trig_duration,           // number of timestamp cycles for which the trigger is active (armed)
    input  [7:0] conf_timestamp_div            // divides the speed of timestamp counter with respect to pdata_clk
    
    );



logic busy_fifo_full;
logic [31:0] trigger_count;
logic        trigger_armed;
logic        trigger_pulse;

logic        frame_start;
logic  [1:0] rx_error_code;
logic [23:0] chip_timestamp;
logic        chip_timestamp_valid;
logic  [5:0] column_addr;
logic  [8:0] row_addr;
logic  [9:0] rise_timestamp;
logic  [5:0] fall_timestamp;
logic        pixel_data_valid;
logic        t0_in_enabled;

logic [63:0] fpga_timestamp;

logic [31:0] data_out_sig;

assign data_out = data_out_sig;

assign busy_out = busy_fifo_full || (conf_busy_when_armed && trigger_armed);

assign t0_in_enabled = t0_in && conf_t0_enable;


rx_data_fsm rx_data_fsm_inst (
    .clk(pdata_clk),       // i
    .rst(rst),             // i
    .rx_locked(rx_locked), // i
    .data_in(pdata_in),    // i [8:0]
    
    .conf_enable_gray_decode(conf_enable_gray_decode), // 1 = gray-encoded data are decoded in hardware
    .rx_error_code(rx_error_code), // serdes lock status change or unexpected data came 
    .frame_start(frame_start), // indicates a start of a new transmission from a chip
    
    .readout_timestamp(chip_timestamp), // o [23:0]
    .timestamp_valid(chip_timestamp_valid),   // o 
    
    .column_addr(column_addr),    // o [5:0]
    .row_addr(row_addr),       // o [8:0]
    .rise_timestamp(rise_timestamp), // o [9:0]
    .fall_timestamp(fall_timestamp), // o [5:0]
    .pixel_data_valid(pixel_data_valid)      // o
);

trigger trigger_inst(
    .clk(pdata_clk),   // clk
    .rst(rst),   // reset

    .trigger_in(trigger_in),         // trigger signal - must be synchronous to clk
    .count_enable(rx_locked),   
    .busy_fifo_full(busy_fifo_full),                  // busy signal input - in case it is 1, triggers are ignored - must be sync. to clk 
    .trigger_counter(trigger_count), // 32 bits output of the trigger counter value
    .trigger_armed(trigger_armed),   // 1'b1 when trigger is active and data can be stored
    .trigger_pulse_out(trigger_pulse),
    
	.conf_trig_edge_sel(conf_trig_edge_sel),     // 1 = rising edge active, 0 = falling edge active
	.conf_trig_enable(conf_trig_enable && conf_enable),       // 0 = trigger input is ignored
	.conf_trig_duration(conf_trig_duration),     // 24 bits
	.conf_trig_always_armed(conf_trig_always_armed), // 1 = trigger always on, all data are stored
	.conf_trig_count_rst(conf_trig_count_rst)     // trigger counter reset flag
);

data_sorter data_sorter_inst(
    .clk(pdata_clk), // i
    .rst(rst), // i
    
    .trigger_armed(trigger_armed && conf_enable), // i
    .trigger_pulse(trigger_pulse), // i
    .t0_in(t0_in_enabled),
    .busy(busy_fifo_full), // o
    
    .rx_error_code(rx_error_code), // serdes lock status change or unexpected data came 
    .frame_start(frame_start),
    .conf_ts_on_each_frame(conf_ts_on_each_frame),
    
    .trig_counter(trigger_count), // i [31:0]
    .fpga_timestamp(fpga_timestamp), // i [63:0]
    
    .chip_timestamp(chip_timestamp), // i [23:0]
    .chip_timestamp_valid(chip_timestamp_valid),   // i 
    
    .column_addr(column_addr),    // i [5:0]
    .row_addr(row_addr),       // i [8:0]
    .rise_timestamp(rise_timestamp), // i [9:0]
    .fall_timestamp(fall_timestamp), // i [5:0]
    .pixel_data_in_valid(pixel_data_valid),     // i
    
    .data_out(data_out_sig), // o [31:0]
    .data_out_valid(data_out_valid), // o
    .fifo_full(fifo_full), // i
    .fifo_empty(fifo_empty) // i
);


fpga_timestamp fpga_timestamp_inst(
    .clk(pdata_clk), // i
    .rst(rst), // i
    .t0_in(t0_in_enabled), // i
    .t0_out(t0_out),
    .enable(conf_enable), // i
    //.t0_enable(conf_t0_enable),
    
    .timestamp(fpga_timestamp), // o
    
    .timestamp_div(conf_timestamp_div)
);

/*
data_output_fifo data_output_fifo_inst(
    .rst(rst),    //: IN STD_LOGIC;
    .wr_clk(), //: IN STD_LOGIC;
    .rd_clk(), //: IN STD_LOGIC;
    .din(),    //: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    .wr_en(),  //: IN STD_LOGIC;
    .rd_en(),  //: IN STD_LOGIC;
    .dout(),   //: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    .full(),   //: OUT STD_LOGIC;
    .empty()   //: OUT STD_LOGIC
);
*/




endmodule