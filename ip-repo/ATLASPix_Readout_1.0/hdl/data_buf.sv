`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.03.2018 11:22:19
// Design Name: 
// Module Name: data_buf
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module chip_data_buf(
    input clk,
    input rst,
    input trig_armed,
    input readout_busy,
    
    input [1:0] rx_error_code, // i [1:0]
    
    input         chip_data_read,
    output [31:0] chip_data_out,
    output        chip_data_valid,
    //output        chip_data_overflow,
    
    input [23:0] readout_timestamp, // i [23:0]
    input        timestamp_valid,   // i 
    
    input [5:0] column_addr,      // i [5:0]
    input [8:0] row_addr,         // i [8:0]
    input [9:0] rise_timestamp,   // i [9:0]
    input [5:0] fall_timestamp,   // i [5:0]
    input       pixel_data_valid  // i
    
    );
    
    
logic fifo_overflow;
logic fifo_empty;
logic fifo_full;

logic fifo_rd_en;
logic fifo_wr_en;
//logic fifo_data_valid;
//logic fifo_data_valid_reg;
logic [31:0] fifo_data_in;
//logic [31:0] fifo_data_out;
//logic [31:0] fifo_data_out_reg;
logic [1:0] rx_error_code_buf;
logic       error_code_sent;
    
typedef enum {
        s_Idle_TS,
        s_RX_Data,
        s_FIFOOverflow,
        s_OvfWait,
        s_ReportStatus
    } t_State;
    
    t_State State, NextState;


assign chip_data_valid = !fifo_empty;
//assign chip_data_valid = fifo_data_valid_reg;
//assign chip_data_out   = fifo_data_out_reg;

assign fifo_rd_en = chip_data_read; // || (!fifo_empty && !fifo_data_valid_reg) || (!fifo_data_valid_reg && fifo_data_valid);

/*
always_ff @(posedge clk) begin
    if (rst)
        begin
            fifo_data_out_reg   <= 32'b0;
            fifo_data_valid_reg <=  1'b0;
        end
    else if (fifo_rd_en)
        begin
            fifo_data_out_reg   <= fifo_data_out;
            fifo_data_valid_reg <= fifo_data_valid;
        end
        
end
*/


always_ff @(posedge clk) begin
    if (rst)
        rx_error_code_buf <= 2'b0;
    else if ((|rx_error_code) | error_code_sent)
        rx_error_code_buf <= rx_error_code;
end

always_ff @(posedge clk) begin
    if (rst)
        State <= s_Idle_TS;
    else
        State <= NextState;
end

// Next state logic
always_comb begin
    // default is to stay in current state
    NextState = State;
    case (State)
        s_Idle_TS: 
            if (trig_armed && timestamp_valid)
                NextState = s_RX_Data;
            else
                NextState = s_Idle_TS;
    
        s_RX_Data: 
            if (timestamp_valid)
                NextState = s_RX_Data; 
            else //(!timestamp_valid)
                NextState = s_Idle_TS;
                      
        s_FIFOOverflow:
            if (!fifo_full)
                NextState = s_OvfWait;
        
        s_OvfWait:
            if (fifo_empty && !readout_busy)
                NextState = s_Idle_TS;
                
        s_ReportStatus:
            if (!fifo_full)
                NextState = s_Idle_TS;
                
        default:
            NextState = s_Idle_TS;
    endcase
    
     // in all states, if there was an error, we need to signalise it 
    if (|rx_error_code)
        NextState = s_ReportStatus;
        
    // in all states, if there was an overflow, we need to signalise it 
    if (fifo_overflow)
        NextState = s_FIFOOverflow;

end


// output logic 
always_comb begin
    // default values
    fifo_wr_en   = 1'b0;
    fifo_data_in = 32'b0;
    error_code_sent = 1'b0;
    
    case (State)
        s_Idle_TS:
            begin
                fifo_data_in = {8'b01000000, readout_timestamp[23:0]}; // WRITE chip readout timestamp
                if (trig_armed && timestamp_valid)
                    fifo_wr_en = 1'b1;
            end
            
        s_RX_Data:
            begin
                fifo_data_in = {1'b1, column_addr[5:0], row_addr[8:0], rise_timestamp[9:0], fall_timestamp[5:0]}; // WRITE pixel data
                if (pixel_data_valid) // && timestamp_valid)    
                    fifo_wr_en = 1'b1;
            end
            
        s_FIFOOverflow:
            begin
                fifo_data_in = {4'b0000, rx_error_code_buf, 2'b01, 24'b0}; // WRITE overflow mark
                if (!fifo_full) begin
                    fifo_wr_en = 1'b1;
                    error_code_sent = 1'b1;
                 end
            end
            
        s_OvfWait:
            begin
                fifo_wr_en   = 1'b0;
                fifo_data_in = 32'b0;
            end
            
        s_ReportStatus:
            begin
                fifo_data_in = {4'b0000, rx_error_code_buf, 2'b00, 24'b0}; // WRITE RX error code
                if (!fifo_full) begin
                    fifo_wr_en = 1'b1;
                    error_code_sent = 1'b1;
                end
            end
            
        default:
            begin
                fifo_wr_en   = 1'b0;
                fifo_data_in = 32'b0;
            end
    endcase
end


chip_data_fifo chip_data_fifo_inst(
    .clk(clk),  // : IN STD_LOGIC;
    .srst(rst), // : IN STD_LOGIC;
    .din(fifo_data_in), // : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    .wr_en(fifo_wr_en), // : IN STD_LOGIC;
    .rd_en(fifo_rd_en), // : IN STD_LOGIC;
    .dout(chip_data_out), // : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    .full(fifo_full), // : OUT STD_LOGIC;
    .overflow(fifo_overflow), // : OUT STD_LOGIC;
    //.valid(fifo_data_valid), // : OUT STD_LOGIC
    .empty(fifo_empty) // : OUT STD_LOGIC;
  );    
  

endmodule