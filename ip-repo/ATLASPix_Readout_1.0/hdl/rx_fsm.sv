 `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 02.02.2018 11:24:09
// Design Name: 
// Module Name: rx_fsm
// Project Name: 
// Target Devices:   
// Tool Versions:    
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module rx_data_fsm(
    input  clk,
    input  rst,
    input  rx_locked,
    input  [8:0] data_in,
    
    input  conf_enable_gray_decode, // 1 = gray-encoded data are decoded in hardware
    
    output [1:0] rx_error_code,
    output       frame_start,
    
    output [23:0] readout_timestamp,
    output        timestamp_valid,

    output [5:0] column_addr, 
    output [8:0] row_addr, 
    output [9:0] rise_timestamp, 
    output [5:0] fall_timestamp,
    output       pixel_data_valid

);
    
    
    
localparam COMMA_WORD = 9'h1BC;
localparam START_WORD = 9'h11C;
    
    
logic is_comma_word;
logic is_start_word;
logic is_control_word;
//logic [7:0]data_in_current;
//logic [7:0]data_in_prev;
logic armed;

logic [1:0]error_code_int;

logic write_BinCntHi;
logic write_BinCntLo;
logic write_GrayCnt;
logic write_Col;
logic write_TS1;
logic write_TS2;
logic write_RowAddr;
logic upper_row_addr;
logic data_valid_next;
logic data_valid_int;
logic timestamp_valid_int;
logic frame_start_int;

logic [7:0] gray_decode;
logic [7:0] gray_decode_optional;
//logic [1:0] gray_decode2;
logic gray_decode_prev;
//logic gray_prev_store;
logic [7:0] data_in_swapped;
//logic [7:0] data_in_sub;

logic [23:0]readout_timestamp_int;
logic [5:0]column_addr_int;
logic [8:0]row_addr_int;
logic [9:0]rise_timestamp_int;
logic [5:0]fall_timestamp_int;

typedef enum {
    s_WaitForLock,
    s_StartR0,
    s_StartR1,
    s_StartR2,
    s_StartR3,
    s_ReadD0,
    s_ReadD1,
    s_ReadD2,
    s_ReadD3,
    s_ReadD4,
    s_ReadD5,
    s_ReadD6,
    s_ReadD7,
    s_Error
} t_State;

t_State State, NextState;
localparam t_State InitialState = s_StartR0;


/* 
//store last received word
always_ff @(posedge clk) begin
    data_in_prev <= data_in[7:0];
    
end
*/

// assign data outputs:
assign readout_timestamp = readout_timestamp_int;
assign column_addr       = {1'b0, column_addr_int[4:0]};
assign row_addr          = row_addr_int;
assign rise_timestamp    = rise_timestamp_int;
assign fall_timestamp    = fall_timestamp_int;
assign pixel_data_valid  = data_valid_int;
assign timestamp_valid   = timestamp_valid_int;
assign rx_error_code     = error_code_int;
assign frame_start       = frame_start_int;

// comparators for COMMA word or START word (announces new transmission)
assign is_control_word = data_in[8];

always_comb begin 
    if (data_in == COMMA_WORD)
        is_comma_word = 1'b1;
    else
        is_comma_word = 1'b0;

    if (data_in == START_WORD)
        is_start_word = 1'b1;
    else
        is_start_word = 1'b0;
end       

// FSM transition    
always_ff @(posedge clk) begin
    if (rst)
        State <= InitialState;
    else
        State <= NextState;
end

// Next state logic
always_comb begin
    // default is to stay in current state
    NextState = State;
    case (State)
        s_WaitForLock:
            if (rx_locked) // serdes locked
                NextState = s_StartR0;
            else // not locked yet 
                NextState = s_WaitForLock; 
                
        s_StartR0: 
            if (!rx_locked) // serdes lock lost
                NextState = s_Error;
            else
                if (is_start_word) // serdes locked and a new data transmission is announced
                    NextState = s_StartR1;
                else // serdes locked and no data is comming yet
                    NextState = s_StartR0; 

        s_StartR1: 
            if (!rx_locked) // serdes lock lost
                NextState = s_Error;
            else // ignore word (0AA)
                NextState = s_ReadD0;

        s_ReadD0: 
            if (!rx_locked) // serdes lock lost
                NextState = s_Error;
            else 
                if (!is_control_word) // first counter word came {9'h000}
                    NextState = s_ReadD1;
                else if (is_comma_word) // wait for counter data
                    NextState = s_ReadD0;
                else if (is_start_word) // restart transmission (will occur once on each tx start)
                    NextState = s_StartR1;
                else // something unexpected came (unkown control word)
                    NextState = s_Error;
           
        s_ReadD1: 
            if (!rx_locked || is_control_word) // serdes lock lost or unexpected control word came
                NextState = s_Error;
            else // second counter word came {1'b0,BinCnt(15:8)}
                NextState = s_ReadD2;
        
        s_ReadD2: 
            if (!rx_locked || is_control_word) // serdes lock lost or unexpected control word came
                NextState = s_Error;
            else // third counter word came {1'b0,BinCnt(7:0)}
                NextState = s_ReadD3;
        
        s_ReadD3:
            if (!rx_locked || is_control_word) // serdes lock lost or unexpected control word came
                NextState = s_Error;
            else // fourth counter word came {1'b0,TsToDet(7:0)}
                NextState = s_ReadD4;
                
        s_ReadD4: 
            if (!rx_locked) // serdes lock lost
                NextState = s_Error;
            else 
                if (!is_control_word) // first pixel data word came {1'b0,Col(7:0)}
                    NextState = s_ReadD5;
                else if (is_comma_word) // wait for pixel data (comma words are being received until then)
                    NextState = s_ReadD4;
                else if (is_start_word)  // pixel data not coming, new counter value will be sent
                    NextState = s_StartR1;
                else // something unexpected came (unkown control word)
                    NextState = s_Error;
        
        s_ReadD5:
            if (!rx_locked || is_control_word) // serdes lock lost or unexpected control word came
                NextState = s_Error;
            else // second pixel data word came {1'b0,Row(5:0),TS(9:8)}
                NextState = s_ReadD6;
        
        s_ReadD6:
            if (!rx_locked || is_control_word) // serdes lock lost or unexpected control word came
                NextState = s_Error;
            else // third pixel data word came {1'b0,TS(7:0)}
                NextState = s_ReadD7;
        
        s_ReadD7: 
            if (!rx_locked || is_control_word) // serdes lock lost or unexpected control word came
                NextState = s_Error;
            else // fourth pixel data word came {1'b0,PixAddr(7:0)}
                NextState = s_ReadD4; // next pixel data or new counter value might came next
        
        s_Error:  // serdes lock lost or unexpected data came
            if (!rx_locked)
                NextState = s_WaitForLock;
            else
                NextState = s_StartR0;
                        
        default:
            NextState = InitialState;
    endcase
end

// output logic
always_comb begin
    // default output values:
    write_BinCntHi       = 1'b0;
    write_BinCntLo       = 1'b0;
    write_GrayCnt        = 1'b0;
    write_Col            = 1'b0;
    write_TS1            = 1'b0;
    write_TS2            = 1'b0;
    write_RowAddr        = 1'b0;
    timestamp_valid_int  = 1'b0;
    data_valid_next      = 1'b0;
    frame_start_int      = 1'b0;
    error_code_int       = 2'b00;
    //gray_prev_store      = 1'b0;
    
    case (State)
        s_WaitForLock: 
        begin
            if (rx_locked)
                error_code_int = 2'b10;
        end
    ////
        s_StartR0: 
        begin
        end
    ////
        s_StartR1:
        begin
        end
    ////
        s_ReadD0:
        begin
            if (rx_locked && !is_control_word)
                frame_start_int = 1'b1;
        end       
    ////
        s_ReadD1: 
        begin
            if (rx_locked && !is_control_word) // second counter word came {1'b0,BinCnt(15:8)}
                begin
                    write_BinCntHi       = 1'b1;
                    //gray_prev_store      = 1'b1;
                end
        end
    ////
        s_ReadD2: 
        begin
            if (rx_locked && !is_control_word) // third counter word came {1'b0,BinCnt(7:0)}
                begin
                    write_BinCntLo       = 1'b1;
                    //gray_prev_store      = 1'b1;
                end
        end
    ////
        s_ReadD3: 
        begin
            if (rx_locked && !is_control_word) // fourth counter word came {1'b0,TsToDet(7:0)}
            begin 
                write_GrayCnt        = 1'b1;
            end
        end
    ////
        s_ReadD4: 
        begin
            timestamp_valid_int = 1'b1;
            if (rx_locked && !is_control_word) // first pixel data word came {1'b0,Col(7:0)}
            begin
                write_Col = 1'b1;
            end
        end
    ////
        s_ReadD5: 
        begin
            timestamp_valid_int = 1'b1;
            if (rx_locked && !is_control_word) // second pixel data word came {1'b0,Row(5:0),TS(9:8)}
            begin
                write_TS1       = 1'b1;
                //gray_prev_store = 1'b1;
            end
        end
    ////
        s_ReadD6: 
        begin
            timestamp_valid_int = 1'b1;
            if (rx_locked && !is_control_word) // third pixel data word came {1'b0,TS(7:0)}
            begin
                write_TS2 = 1'b1;
            end
        end
    ////
        s_ReadD7: 
        begin
            timestamp_valid_int = 1'b1;
            if (rx_locked && !is_control_word) // fourth pixel data word came {1'b0,PixAddr(7:0)}
            begin
                write_RowAddr   = 1'b1;
                data_valid_next = 1'b1;
            end
        end
    ////
        s_Error:
        begin
            if (!rx_locked) // SERDES lock lost
                error_code_int = 2'b11;
            else // other error (unexpected data came)
                error_code_int = 2'b01;  
        end

    endcase
end
    
// Set data valid flag in next clock cycle (if(data_valid_next), last data part is coming right now)
always_ff @(posedge clk) begin
    if (rst) begin
        data_valid_int <= 1'b0;
    end
    else begin
        data_valid_int <= data_valid_next;
    end
end


// registers for storing the received data
always_ff @(posedge clk) begin
    if (rst) begin
        readout_timestamp_int <= 24'b0;
        column_addr_int       <=  6'b0;
        row_addr_int          <=  9'b0;
        upper_row_addr        <=  1'b0;
        fall_timestamp_int    <=  6'b0;
        rise_timestamp_int    <= 10'b0;
    end
    else begin
        // default is to keep the value
        readout_timestamp_int <= readout_timestamp_int;
        column_addr_int       <= column_addr_int;
        row_addr_int          <= row_addr_int;
        upper_row_addr        <= upper_row_addr;
        fall_timestamp_int    <= fall_timestamp_int;
        rise_timestamp_int    <= rise_timestamp_int;
    
        // counter value
        if (write_BinCntHi) begin
            readout_timestamp_int[23:16] <= data_in[7:0];
        end
        
        if (write_BinCntLo) begin
            readout_timestamp_int[15:8] <= data_in[7:0];
        end
        
        if (write_GrayCnt) begin
            readout_timestamp_int[7:0] <= gray_decode_optional[7:0];
        end
        
        // pixel data
        if (write_Col) begin
            column_addr_int[5:0] <= 6'd24 - {data_in[6:1]}; //6'd24 - data_in[7:2];
            upper_row_addr   <= data_in[0];
        end
        
        if (write_TS1) begin
            fall_timestamp_int[5:0] <= gray_decode_optional[7:2];
            rise_timestamp_int[9:8] <= {data_in[1], (conf_enable_gray_decode & data_in[1])^data_in[0]};
        end
        
        if (write_TS2) begin
            rise_timestamp_int[7:0] <= gray_decode_optional[7:0];
        end
        
        if (write_RowAddr) begin
            if (upper_row_addr)
                row_addr_int[8:0] <= {1'b0, 8'd255 - data_in_swapped[7:0]};
            else
                row_addr_int[8:0] <= 9'd455 - {1'b0, data_in_swapped[7:0]};
        end
    end
end

assign data_in_swapped[7:0] = {data_in[0], data_in[1], data_in[2], data_in[3], data_in[4], data_in[5], data_in[6], data_in[7]};
//assign data_in_sub[7:0]     = 8'd255 - data_in_swapped[7:0];

/*
// data_out mux
assign data_out = push_counter ? {8'b0, readout_timestamp} : {1'b1, column_addr, row_addr, rise_timestamp, fall_timestamp};

// data_out valid flag 
assign data_out_valid = push_counter | push_data;
*/
/*
always_ff @(posedge clk) begin 
    if (gray_prev_store)
        gray_decode_prev <= gray_decode[0];
    else
        gray_decode_prev <= 1'b0;
end
*/ 
    assign gray_decode_prev = write_TS2 & rise_timestamp_int[8];
    
    assign gray_decode_optional = conf_enable_gray_decode ? gray_decode : data_in;

// decode currently received byte from gray to bin

    assign gray_decode[7] = gray_decode_prev^data_in[7];
    assign gray_decode[6] = gray_decode_prev^data_in[7]^data_in[6];
    assign gray_decode[5] = gray_decode_prev^data_in[7]^data_in[6]^data_in[5];
    assign gray_decode[4] = gray_decode_prev^data_in[7]^data_in[6]^data_in[5]^data_in[4];
    assign gray_decode[3] = gray_decode_prev^data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3];
    assign gray_decode[2] = gray_decode_prev^data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3]^data_in[2];
    assign gray_decode[1] = gray_decode_prev^data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3]^data_in[2]^data_in[1];
    assign gray_decode[0] = gray_decode_prev^data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3]^data_in[2]^data_in[1]^data_in[0];

/*
    assign gray_decode2[1] = data_in[1];
    assign gray_decode2[0] = data_in[1]^data_in[0];
    

    assign gray_decode[7] = data_in[7];
    assign gray_decode[6] = data_in[7]^data_in[6];
    assign gray_decode[5] = data_in[7]^data_in[6]^data_in[5];
    assign gray_decode[4] = data_in[7]^data_in[6]^data_in[5]^data_in[4];
    assign gray_decode[3] = data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3];
    assign gray_decode[2] = data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3]^data_in[2];
    assign gray_decode[1] = data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3]^data_in[2]^data_in[1];
    assign gray_decode[0] = data_in[7]^data_in[6]^data_in[5]^data_in[4]^data_in[3]^data_in[2]^data_in[1]^data_in[0];

*/
endmodule