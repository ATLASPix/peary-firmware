`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2018 15:15:00
// Design Name: 
// Module Name: sync_pulse
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// generates a pulse for one out_clk cycle each time the signal_in is 1
module sync_pulse #(
    int WIDTH  = 1,
    int STAGES = 2 
)(
    input in_clk,
    input out_clk,
    input  [WIDTH-1 :0] pulse_in,  // i, sync to in_clk 
    output [WIDTH-1 :0] pulse_out // o, sync to out_clk
    );


    logic [WIDTH-1 :0] T_FF        = 1'b0;
    logic [WIDTH-1 :0] level_out;
    logic [WIDTH-1 :0] level_out_prev;

    // pulse -> level change
    always_ff @(posedge in_clk)
        for (int i=0; i < WIDTH; i=i+1)
            if (pulse_in[i])
                T_FF[i] <= ~T_FF[i];

    // level syncer
    sync_signal #(
        .WIDTH(WIDTH),
        .STAGES(STAGES)
    ) i_LevelSyncer (
        .out_clk(out_clk),
        .signal_in(T_FF),
        .signal_out(level_out)
    );

    always_ff @(posedge out_clk)
        level_out_prev <= level_out;

    assign pulse_out = level_out ^ level_out_prev;

endmodule
