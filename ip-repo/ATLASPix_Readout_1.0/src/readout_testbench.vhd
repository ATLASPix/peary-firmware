library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
--use work.ATLASPix_Readout_v1_0;

entity readout_testbench is

end readout_testbench;

architecture Behavioral of readout_testbench is
   
    signal pdata_clk  : std_logic;
    signal rst  : std_logic;
    signal enable : std_logic;
    
    signal dataout : std_logic_vector(63 downto 0);
    signal fifo_wr_en : std_logic;
    signal fifo_empty : std_logic;
    signal almostfull : std_logic;
    
    signal decoded_data_in : std_logic_vector(8 downto 0);

    signal fast_syncrst : std_logic;
    signal counteroffset : std_logic_vector(3 downto 0);
    signal trigger : std_logic;
    signal exttrigedgeselection : std_logic;
    signal timestampdiv : std_logic_vector(7 downto 0);
    signal timestampphase : std_logic_vector(7 downto 0);
    signal injection : std_logic;
    signal triggercontrol : std_logic_vector(7 downto 0);
    signal busy : std_logic;
    signal armduration : std_logic_vector(23 downto 0);

    
    Constant ClockPeriod : TIME := 31.25 ns;
    Constant hold : TIME := ClockPeriod/5;
    
    shared variable ClockCount : integer range 0 to 50_000 := 10;

begin


  ATPdecode: entity work.ATPdecode

    port map (
        pdata_clk => pdata_clk,
        rst => rst,
        enable => enable,
        
        dataout => dataout,
        fifo_wr_en => fifo_wr_en,
        fifo_empty => fifo_empty,
        almostfull => almostfull,
        
        decoded_data_in => decoded_data_in,
    
        fast_syncrst => fast_syncrst,
        counteroffset => counteroffset,
        trigger => trigger,
        exttrigedgeselection => exttrigedgeselection,
        timestampdiv => timestampdiv,
        timestampphase => timestampphase,
        injection => injection,
        triggercontrol => triggercontrol,
        busy => busy,
        armduration => armduration
    );



    

 -- Generate clock signal
 GENERATE_REFCLOCK : process
 begin
   wait for (ClockPeriod / 2);
   ClockCount:= ClockCount+1;
   pdata_clk <= '1';
   wait for (ClockPeriod / 2);
   pdata_clk <= '0';
 end process;

 


 -- 
 tb : PROCESS
 BEGIN
         
         counteroffset <= (others =>'0');
         trigger  <= '0';
         exttrigedgeselection <= '0';
         timestampdiv <= (others =>'0');
         timestampphase <= (others =>'0');
         triggercontrol <= (others =>'0');
         armduration <= (others =>'0');
 
        rst <= '1';
        enable <= '0';
        fifo_empty <= '1'; 
        almostfull <= '0';
        decoded_data_in <= (others => '0');
        wait for hold;
        wait for ClockPeriod*10;
        rst <='0';
        triggercontrol(1) <= '1';
        enable <= '1';
        --0
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --1
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"1c";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"aa";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"1c";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"aa";
        --2
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --3
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"0f";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"56";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"fd";
        --4
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --5
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"06";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"dc";
        --6
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --7
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"10";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"66";
        --8
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --9     
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"18";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"31";
        --10
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --11
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"30";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"00";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"9a";
        --12
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --13
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --14
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --15
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --16
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --17
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --18
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        --19
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"1c";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"aa";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"1c";
        wait for ClockPeriod;
        decoded_data_in <= '0' & x"aa";
        --20
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";
        wait for ClockPeriod;
        decoded_data_in <= '1' & x"bc";


     wait; -- will wait forever
 END PROCESS tb;

end Behavioral;
