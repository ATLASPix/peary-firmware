library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;

use ieee.std_logic_textio.all;

library work;
use work.ATLASPix_Readout_v1_0;

entity testbench is
generic
(
  C_S_AXI_DATA_WIDTH             : integer              := 32;
  C_S_AXI_ADDR_WIDTH             : integer              := 5
);

end testbench;

architecture Behavioral of testbench is
    signal pdata_clk : std_logic;
    signal decoded_atp_data_in : std_logic_vector(8 downto 0);
    signal rx_locked : std_logic;
    signal trigger_in : std_logic;
    signal t0_in : std_logic;
    signal busy_out : std_logic;

    signal S_AXI_ACLK                     :  std_logic;
    signal S_AXI_ARESETN                  :  std_logic;
    signal S_AXI_AWADDR                   :  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    signal S_AXI_AWVALID                  :  std_logic;
    signal S_AXI_WDATA                    :  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal S_AXI_WSTRB                    :  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
    signal S_AXI_WVALID                   :  std_logic;
    signal S_AXI_BREADY                   :  std_logic;
    signal S_AXI_ARADDR                   :  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    signal S_AXI_ARVALID                  :  std_logic;
    signal S_AXI_RREADY                   :  std_logic;
    signal S_AXI_ARREADY                  : std_logic;
    signal S_AXI_RDATA                    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal S_AXI_RRESP                    : std_logic_vector(1 downto 0);
    signal S_AXI_RVALID                   : std_logic;
    signal S_AXI_WREADY                   : std_logic;
    signal S_AXI_BRESP                    : std_logic_vector(1 downto 0);
    signal S_AXI_BVALID                   : std_logic;
    signal S_AXI_AWREADY                  : std_logic;
    signal S_AXI_AWPROT                   : std_logic_vector(2 downto 0);
    signal S_AXI_ARPROT                   : std_logic_vector(2 downto 0);

    
    Constant ClockPeriod_AXI   : TIME := 5 ns;
    Constant ClockPeriod_pdata : TIME := 31.25 ns;
    Constant ClockHold_AXI     : TIME := 1 ns;
    Constant ClockHold_pdata   : TIME := 5 ns;
    shared variable ClockCount : integer range 0 to 50_000 := 10;
    signal sendIt : std_logic := '0';
    signal readIt : std_logic := '0';

begin

  -- instance ""
  ATLASPix_Readout_v1_0: entity work.ATLASPix_Readout_v1_0
    generic map (
      C_S00_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
      C_S00_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH)
    port map (
    
    pdata_clk => pdata_clk,
    decoded_atp_data_in => decoded_atp_data_in,
    rx_locked => rx_locked,
    trigger_in => trigger_in, 
    t0_in => t0_in,
    busy_out => busy_out, 
    
      s00_axi_aclk    => S_AXI_ACLK,
      s00_axi_aresetn => S_AXI_ARESETN,
      s00_axi_awaddr  => S_AXI_AWADDR,
      s00_axi_awprot  => S_AXI_AWPROT,
      s00_axi_awvalid => S_AXI_AWVALID,
      s00_axi_awready => S_AXI_AWREADY,
      s00_axi_wdata   => S_AXI_WDATA,
      s00_axi_wstrb   => S_AXI_WSTRB,
      s00_axi_wvalid  => S_AXI_WVALID,
      s00_axi_wready  => S_AXI_WREADY,
      s00_axi_bresp   => S_AXI_BRESP,
      s00_axi_bvalid  => S_AXI_BVALID,
      s00_axi_bready  => S_AXI_BREADY,
      s00_axi_araddr  => S_AXI_ARADDR,
      s00_axi_arprot  => S_AXI_ARPROT,
      s00_axi_arvalid => S_AXI_ARVALID,
      s00_axi_arready => S_AXI_ARREADY,
      s00_axi_rdata   => S_AXI_RDATA,
      s00_axi_rresp   => S_AXI_RRESP,
      s00_axi_rvalid  => S_AXI_RVALID,
      s00_axi_rready  => S_AXI_RREADY);

    rx_locked <= S_AXI_ARESETN;
    --trigger_in <= '0';
    t0_in <= '0';


 -- Generate S_AXI_ACLK signal
 GENERATE_REFCLOCK : process
 begin
   wait for (ClockPeriod_AXI / 2);
   ClockCount:= ClockCount+1;
   S_AXI_ACLK <= '1';
   wait for (ClockPeriod_AXI / 2);
   S_AXI_ACLK <= '0';
 end process;
 
  -- Generate pdata_clk signal
 GENERATE_REFCLOCK2 : process
 begin
   wait for (ClockPeriod_pdata / 2);
   pdata_clk <= '1';
   wait for (ClockPeriod_pdata / 2);
   pdata_clk <= '0';
 end process;

 -- Initiate process which simulates a master wanting to write.
 -- This process is blocked on a "Send Flag" (sendIt).
 -- When the flag goes to 1, the process exits the wait state and
 -- execute a write transaction.
 send : PROCESS
 BEGIN
    S_AXI_AWVALID<='0';
    S_AXI_WVALID<='0';
    S_AXI_BREADY<='0';
    loop
        wait until sendIt = '1';
        wait until S_AXI_ACLK= '1';
        wait for ClockHold_AXI;
            S_AXI_AWVALID<='1';
            S_AXI_WVALID<='1';
        wait until (S_AXI_AWREADY and S_AXI_WREADY) = '1';  --Client ready to read address/data        
            S_AXI_BREADY<='1';
        wait until S_AXI_BVALID = '1';  -- Write result valid
            assert S_AXI_BRESP = "00" report "AXI data not written" severity failure;
            S_AXI_AWVALID<='0';
            S_AXI_WVALID<='0';
            S_AXI_BREADY<='1';
        wait until S_AXI_BVALID = '0';  -- All finished
            S_AXI_BREADY<='0';
    end loop;
 END PROCESS send;

  -- Initiate process which simulates a master wanting to read.
  -- This process is blocked on a "Read Flag" (readIt).
  -- When the flag goes to 1, the process exits the wait state and
  -- execute a read transaction.
  read : PROCESS
  BEGIN
    S_AXI_ARVALID<='0';
    S_AXI_RREADY<='0';
     loop
         wait until readIt = '1';
         wait until S_AXI_ACLK= '1';
         wait for ClockHold_AXI;
             S_AXI_ARVALID<='1';
             S_AXI_RREADY<='1';
         wait until (S_AXI_ARREADY) = '1';  --Client accepted address
         wait until (S_AXI_RVALID) = '1';  --Client provided data
         wait until S_AXI_ACLK= '1';
         wait for ClockHold_AXI;
            assert S_AXI_RRESP = "00" report "AXI data not written" severity failure;
             S_AXI_ARVALID<='0';
            S_AXI_RREADY<='0';
     end loop;
  END PROCESS read;


 -- 
 AXItb : PROCESS
 BEGIN
        S_AXI_ARESETN<='0';
        sendIt<='0';
    wait for ClockPeriod_AXI * 5;
        S_AXI_ARESETN<='1';
    wait for ClockPeriod_AXI * 10;
    

        
            S_AXI_AWADDR<="01000"; -- slv_reg2
            -- 0000 0000 | 0000 0000 | 0000 0000 | 0000 0011
            S_AXI_WDATA<=x"00000003"; -- enable readout and enable trigger
            S_AXI_WSTRB<=b"1111";
            sendIt<='1';                --Start AXI Write to Slave
            wait for 1 ns; sendIt<='0'; --Clear Start Send Flag
        wait until S_AXI_BVALID = '1';
        wait until S_AXI_BVALID = '0';  --AXI Write finished
            S_AXI_WSTRB<=b"0000";
    
    
            S_AXI_AWADDR<="01100"; -- slv_reg3
            -- 0000 0000 | 0000 0000 | 0000 0000 | 0000 0000
            S_AXI_WDATA<=x"00000010"; -- 
            S_AXI_WSTRB<=b"1111";
            sendIt<='1';                --Start AXI Write to Slave
            wait for 1 ns; sendIt<='0'; --Clear Start Send Flag
        wait until S_AXI_BVALID = '1';
        wait until S_AXI_BVALID = '0';  --AXI Write finished
            S_AXI_WSTRB<=b"0000";
    
    wait until busy_out = '1';
    
    wait for ClockPeriod_pdata * 256;
    
    loop            
            
            S_AXI_ARADDR<="00000";
            readIt<='1';                --Start AXI Read from Slave
            wait for 1 ns; readIt<='0'; --Clear "Start Read" Flag
        wait until S_AXI_RVALID = '1';
        wait until S_AXI_RVALID = '0';
        
            wait for ClockPeriod_AXI * 50;
        
    end loop;


               
        S_AXI_AWADDR<="01000";
        S_AXI_WDATA<=x"00000001";
        S_AXI_WSTRB<=b"1111";
        sendIt<='1';                --Start AXI Write to Slave
        wait for 1 ns; sendIt<='0'; --Clear Start Send Flag
    wait until S_AXI_BVALID = '1';
    wait until S_AXI_BVALID = '0';  --AXI Write finished
        S_AXI_WSTRB<=b"0000";
        
        S_AXI_ARADDR<="01000";
        readIt<='1';                --Start AXI Read from Slave
        wait for 1 ns; readIt<='0'; --Clear "Start Read" Flag
    wait until S_AXI_RVALID = '1';
    wait until S_AXI_RVALID = '0';
       
        S_AXI_AWADDR<="01000";
        S_AXI_WDATA<=x"A5A5A5A5";
        S_AXI_WSTRB<=b"1111";
        sendIt<='1';                --Start AXI Write to Slave
        wait for 1 ns; sendIt<='0'; --Clear Start Send Flag
    wait until S_AXI_BVALID = '1';
    wait until S_AXI_BVALID = '0';  --AXI Write finished
        S_AXI_WSTRB<=b"0000";
        
        S_AXI_ARADDR<="00000";
        readIt<='1';                --Start AXI Read from Slave
        wait for 1 ns; readIt<='0'; --Clear "Start Read" Flag
    wait until S_AXI_RVALID = '1';
    wait until S_AXI_RVALID = '0';
    
    
        S_AXI_ARADDR<="00000";
        readIt<='1';                --Start AXI Read from Slave
        wait for 1 ns; readIt<='0'; --Clear "Start Read" Flag
    wait until S_AXI_RVALID = '1';
    wait until S_AXI_RVALID = '0';
    
    
        S_AXI_ARADDR<="00000";
        readIt<='1';                --Start AXI Read from Slave
        wait for 1 ns; readIt<='0'; --Clear "Start Read" Flag
    wait until S_AXI_RVALID = '1';
    wait until S_AXI_RVALID = '0';
    
        S_AXI_ARADDR<="01000";
        readIt<='1';                --Start AXI Read from Slave
        wait for 1 ns; readIt<='0'; --Clear "Start Read" Flag
    wait until S_AXI_RVALID = '1';
    wait until S_AXI_RVALID = '0';


            
            
        
     wait; -- will wait forever
 END PROCESS AXItb;
 
 
 
 ---------------------------------------------------------------
 --chip data
 
CHIPtb:   process
   variable v_ILINE     : line;
   variable v_IDATA  : std_logic_vector(8 downto 0);
   file file_VECTORS : text;
    
 begin

   file_open(file_VECTORS, "chip_vectors.mem",  read_mode);

   while not endfile(file_VECTORS) loop
     readline(file_VECTORS, v_ILINE);
     hread(v_ILINE, v_IDATA);

     wait until rising_edge(pdata_clk);
     wait for ClockHold_pdata;
     
     decoded_atp_data_in <= v_IDATA;

   end loop;

   file_close(file_VECTORS);
    
   wait;
 end process CHIPtb;
 
 ---------------------------------------------------------------
 --trigger
 
trig:   process
 begin
    trigger_in <= '0';
    
    wait until S_AXI_BVALID = '1';
    wait for ClockPeriod_pdata * 50;
    wait for ClockHold_pdata;
     
    trigger_in <= '1';

    wait for ClockPeriod_pdata * 5;
    wait for ClockHold_pdata;
    
    trigger_in <= '0';

    
    wait;
 end process trig;
 

end Behavioral;
