----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.09.2017 15:45:36
-- Design Name: 
-- Module Name: PulseCounter_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PulseCounter_wrapper is
  PORT(
        CLK  :in std_logic;
        RST  :in std_logic;

        CNT_RST:in std_logic_vector(3 downto 0);

        MON1_IN : in std_logic;
        MON2_IN : in std_logic;
        MON3_IN : in std_logic;
        
        INJ_IN : in std_logic;

        INJ_CNT_OUT :out std_logic_vector(15 downto 0);
        MON1_CNT_OUT :out std_logic_vector(15 downto 0);
        MON2_CNT_OUT :out std_logic_vector(15 downto 0);
        MON3_CNT_OUT :out std_logic_vector(15 downto 0)

        );
end PulseCounter_wrapper;

architecture Behavioral of PulseCounter_wrapper is

begin


ATLASPix_PulseCounter:entity  work.PulseCounter_core

port map (
        CLK  => clk,
        RST  => rst,

        CNT_RST => cnt_rst,

        MON1_IN => mon1_in,
        MON2_IN => mon2_in,
        MON3_IN => mon3_in,
        
        INJ_IN => inj_in,

        INJ_CNT_OUT => inj_cnt_out,
        MON1_CNT_OUT => mon1_cnt_out,
        MON2_CNT_OUT => mon2_cnt_out,
        MON3_CNT_OUT => mon3_cnt_out


);
			



end Behavioral;
