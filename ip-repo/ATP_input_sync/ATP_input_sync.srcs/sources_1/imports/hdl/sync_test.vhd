library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library work;
use work.serdes_sync;

entity sync_testbench is

end sync_testbench;

architecture Behavioral of sync_testbench is

    signal clk       : std_logic;
    signal rst       : std_logic;
    signal delay_inc : std_logic;
    signal delay_ce  : std_logic;
    signal delay_rst : std_logic;
    signal bitslip   : std_logic;
    signal locked    : std_logic;
    
    signal data_in   : std_logic_vector(9 downto 0);
    signal tap_delay : std_logic_vector(4 downto 0);
    
    signal bitslip_count : std_logic_vector(3 downto 0);
    
    Constant ClockPeriod : TIME := 5 ns;
    Constant ClockHold   : TIME := 0.5 ns;

begin


  -- instance "led_controller_v1_0_1"
  i_serdes_sync: entity work.serdes_sync

    port map (
    
        clk_slow => clk,
        rst      => rst,
        
        data_in      => data_in,
        delay_tap_in => tap_delay,
        
        delay_inc => delay_inc,
        delay_ce  => delay_ce,
        delay_rst => delay_rst,
        bitslip   => bitslip,
        locked    => locked
    
    );

data_in <= "0101111100" when (

                            (( tap_delay = "00110" 
                            or tap_delay = "00111"
                            or tap_delay = "01000"
                            or tap_delay = "01001" )
                           and bitslip_count = "0101") 
                           or 
                            (( tap_delay = "00000" 
                            or tap_delay = "00001"
                            or tap_delay = "01000"
                            or tap_delay = "01001" )
                           and bitslip_count = "0110")
                           
                           )
                           else "1101110001"; 

 -- Generate clock signal
  GENERATE_CLOCK: process
 begin
   wait for (ClockPeriod / 2);
   clk <= '1';
   wait for (ClockPeriod / 2);
   clk <= '0';
 end process;
  
  -- tap counter
 i_tap_count  : process (clk, rst)
 begin
    if (rst='1') then 
        tap_delay <= (others => '0');
    elsif (clk'event and clk='1') then
        if  (delay_ce = '1') then
            if (delay_inc = '1') then
                tap_delay <= tap_delay + 1;
            else
                tap_delay <= tap_delay - 1;
            end if;
        else 
            tap_delay <= tap_delay;
        end if;
    end if;
 end process;
 

  -- bitslip counter
 i_bitslip_count  : process (clk, rst)
 begin
    if (rst='1') then 
        bitslip_count <= (others => '0');
    elsif (clk'event and clk='1') then
        if  (bitslip = '1') then
            bitslip_count <= bitslip_count + 1;
            if  (bitslip_count = "1001") then
                bitslip_count <= (others => '0');
            end if;
        else 
            bitslip_count <= bitslip_count;
        end if;
    end if;
 end process;

 


 -- 
 tb : PROCESS
 BEGIN
    rst <= '1';
    wait for ClockHold + (ClockPeriod * 5);
    rst <= '0';
    

     wait until (locked = '1'); -- will wait forever
     
     wait for ClockPeriod;
     
     
     wait; -- will wait forever
 END PROCESS tb;

end Behavioral;
