set_false_path -from [get_cells { control/axi_fsm/registers_reg[*] }] -to [get_clocks -of_objects [get_pins -hierarchical */CLICpix2_slow_clock]]
set_false_path -from [get_cells { control/axi_fsm/registers_reg[*] }] -to [get_pins {control/waveGenerator/CLICpix2_pins\.*reg/D}]
set_false_path -from [get_cells { control/axi_fsm/registers_reg[*] }] -to [get_pins {control/waveGenerator/timeCnt_reg[*]/D}]
