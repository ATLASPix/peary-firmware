----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.09.2017 16:06:19
-- Design Name: 
-- Module Name: pulser_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pulser_sim is
--  Port ( );
end pulser_sim;

architecture Behavioral of pulser_sim is
	
signal ccpd_inj_pulse_1       :std_logic;
signal ccpd_inj_pulse_2       :std_logic;
signal ccpd_inj_pulse_3       :std_logic;
signal ccpd_inj_pulse_4       :std_logic;
signal ccpd_inj_ref           :std_logic;
signal ccpd_inj_flg :std_logic;
signal ccpd_inj_pls_cnt :std_logic_vector(15 downto 0);
signal ccpd_inj_high_cnt :std_logic_vector(31 downto 0);
signal ccpd_inj_low_cnt :std_logic_vector(31 downto 0);
signal ccpd_inj_out_en  :std_logic_vector(3 downto 0);
signal global_reset :std_logic;
signal clk      :std_logic; -- H35DEMO config. LD_CMOS

constant PERIOD : time := 6.25ns;

begin

     clk_process : process
   begin
       clk <= '0';
       wait for PERIOD /2; 
       clk <= '1';
       wait for PERIOD/2;   
end process;

ccpd_inj_out_en <= B"1111";


h35demo_inj_gen:entity work.pulse_gen
    generic map(
      input_clk_freq => 100_000_000
    )
    port map( 
      RST            => global_reset,
      SYSCLK         => clk,
      
      INJ_FLG      => ccpd_inj_flg,
      INJ_PLS_CNT  => ccpd_inj_pls_cnt,
      INJ_HIGH_CNT => ccpd_inj_high_cnt,
      INJ_LOW_CNT  => ccpd_inj_low_cnt,
      INJ_OUT_EN   => ccpd_inj_out_en,
      
      --pulse output
      PULSE_OUT1     => ccpd_inj_pulse_1,
      PULSE_OUT2     => ccpd_inj_pulse_2,
      PULSE_OUT3     => ccpd_inj_pulse_3,
      PULSE_OUT4     => ccpd_inj_pulse_4,
      PULSE_OUT_REF  => ccpd_inj_ref
    );  
    
    stim_proc : process
    begin
       
       global_reset <= '1';
       wait for 100 ns;
       global_reset <= '0';
       ccpd_inj_flg <= '0';      

       ccpd_inj_pls_cnt <= X"000F";
       wait for 100 ns;
       ccpd_inj_high_cnt <= X"0000000F";
       wait for 100 ns; 
       ccpd_inj_low_cnt <= X"000000FF";
       wait for 100 ns;
       wait for 100 ns;         
       ccpd_inj_flg <= '1';      
       wait for 200 ns;
       ccpd_inj_flg <= '0';      
       wait for 2000000000 ns;

    end process;
    


end Behavioral;
