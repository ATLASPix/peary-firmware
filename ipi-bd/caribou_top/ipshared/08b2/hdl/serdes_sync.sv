`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.12.2017 10:57:09
// Design Name: 
// Module Name: serdes_sync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module serdes_sync(
//    input clk_din,
    input clk_slow,
    input rst,
    input [9:0] data_in,
    input [4:0] delay_tap_in,
    output delay_inc,
    output delay_ce,
    output delay_rst,
    output bitslip,
    output locked
    //output [7:0] debug
    );
    

    typedef enum {
        s_Init,       //  
        s_WaitBitScan,
        s_BitScan,    // 
        s_WaitDelayScan, 
        s_DelayScan,   // 
        s_DelayReturn,   // 
        s_DelayDec,   // 
        s_WaitCheck,   // 
        s_Check,
        s_Locked   // 
    } t_State;

    t_State State, NextState;
    localparam t_State InitialState = s_Init;

    logic rstFSM;
    logic rstDelayed;
    logic rstDelayedReg;
    logic rstResync;
    
    logic patternFound;
    logic patternFoundN;
    logic patternFoundP;
    
    logic bitSlipRequest;
    logic bitScanComplete;
    logic [9:0] bitScanReg = 0;
    
    logic waitRequest;
    logic waitComplete;
    logic [1:0] waitReg;
    
    logic delayUp;
    logic delayCountUp;
    logic delayDown;
    logic delayCountDown;
    logic delayCountHalf;
    logic delaySave;
    logic delayCountZero;
    logic delayMinMatchOK;
    logic [4:0] delayCountReg = 0;
    logic [3:0] syncDelayReg  = 0;

    logic FSMLocked; 
    
    //localparam logic word_length = 9;
    localparam minimum_delay_match_range = 3;
    localparam align_pattern = 10'b0101111100;

////////////////////////////
// debug output
/*
assign debug[7] = 1'b0;
assign debug[6] = FSMLocked;
assign debug[5] = patternFoundP;
assign debug[4] = patternFoundN;
assign debug[3] = delayDown;
assign debug[2] = delayUp;
assign debug[1] = bitSlipRequest;
assign debug[0] = clk_slow;
*/
////////////////////////////
	
// output assignment:
	assign bitslip   = bitSlipRequest;
	assign locked    = FSMLocked;
	assign delay_rst = rstFSM;
	
	assign  delay_inc = delayUp;
    assign  delay_ce  = (delayUp || delayDown);
	
// Reset logic - delays deassertion of reset signal for one clk_slow cycle      
    always_ff @(posedge clk_slow) begin
        if (rst) rstDelayedReg <= 1'b1;
        else     rstDelayedReg <= 1'b0;
    end
    assign rstDelayed = rstDelayedReg || rst || rstResync;

// wait two cycles - latency of bitslip module
    always_ff @(posedge clk_slow) begin
        if (rst || !waitRequest) begin 
            waitComplete <= 0;
            waitReg      <= 2'b00;
        end
        else begin 
            waitReg      <= {waitRequest, waitReg[1]}; 
            waitComplete <= waitReg[0];
        end
    end
    
// comparator looking for the comma word pattern    
    always_comb begin
        if (data_in == align_pattern) 
            patternFoundP = 1;
        else  
            patternFoundP = 0;
            
        if (data_in == ~align_pattern) 
            patternFoundN = 1;
        else  
            patternFoundN = 0;
    end
    
    assign patternFound = patternFoundN || patternFoundP;
    
// Bit Slip scan      
    always_ff @(posedge clk_slow) begin
        if (rstFSM) begin
            bitScanReg      <= 10'b1000000000;
        end
        else if (bitSlipRequest) begin
            bitScanReg[9:0] <= {bitScanReg[0], bitScanReg[9:1]};  // rotate
        end
    end
    assign bitScanComplete = bitScanReg[0]; // && bitSlipRequest;
    
// Delay match registry
    always_ff @(posedge clk_slow) begin
        if (rstFSM) begin 
            //delayFirstOK <= 0;
            delayCountReg <= 0;
        end
        else begin
            //delayFirstOK <= delayFirstOK;
            delayCountReg <= delayCountReg;
            if (delaySave) begin
                //delayFirstOK <= delay_tap_in;
                delayCountReg <= 1;
            end
            else if (delayCountUp) begin
                delayCountReg <= delayCountReg + 1;
            end
			else if (delayCountHalf) begin
				delayCountReg <= delayCountReg >> 1;
			end
			else if (delayCountDown) begin
			    delayCountReg <= delayCountReg - 1;
			end
        end
    end

    // minimal acceptable delay range is 3. That corresponds to delayMinMatchOK greater than 5'b00010
    //assign delayMinMatchOK = !(delayCountReg[4] || delayCountReg[3] || delayCountReg[2] || delayCountReg[1]) 
	always_comb begin
		if (delayCountReg >= minimum_delay_match_range) delayMinMatchOK = 1;
		else delayMinMatchOK = 0;
	end

	always_comb begin
		if (delayCountReg == 0) delayCountZero = 1;
		else delayCountZero = 0;
	end
	
	
// FSM transition    
    always_ff @(posedge clk_slow) begin
        if (rstDelayed)
            State <= InitialState;
        else
            State <= NextState;
    end

// Next state logic
    always_comb begin
        // default is to stay in current state
        NextState = State;
        case (State)
            
            s_Init: 
                NextState = s_WaitBitScan;
            
            s_WaitBitScan:
                if (waitComplete)
                    NextState = s_BitScan;
                else
                    NextState = s_WaitBitScan;
            
            s_BitScan:
                if (patternFound)
                    NextState = s_WaitDelayScan;
                else 
                    NextState = s_WaitBitScan;
            
            s_WaitDelayScan:
                if (waitComplete)
                    NextState = s_DelayScan;
                else
                    NextState = s_WaitDelayScan;            
            
            s_DelayScan:
                // if pattern was still present and we are not at the end of the range
                if (patternFound && (delay_tap_in != 5'b11111))
                    // try one more
                    NextState = s_WaitDelayScan;
                // if we are already fine with what we found
                else if (delayMinMatchOK)
                    // return to the middle of the matching delay interval
					NextState = s_DelayDec;
				// if there is no pattern and the delay range was too low	
				else
				    // return back to bitslip scan and try further 
					NextState = s_DelayReturn;
                        
            s_DelayReturn:
                if (delayCountZero)
                    NextState = s_WaitBitScan;
                else 
                    NextState = s_DelayReturn;
                    
            s_DelayDec:
                if (delayCountZero)
                    NextState = s_WaitCheck;
                else 
                    NextState = s_DelayDec;
                            
            s_WaitCheck:
                if (waitComplete)
                    NextState = s_Check;
                else
                    NextState = s_WaitCheck; 

            s_Check:
                if (patternFound)
                    NextState = s_Locked;
                else 
                    NextState = s_Init;
                    
            s_Locked:  
                NextState = s_Locked;
                            
            default:
                NextState = InitialState;
        endcase
    end


// output logic
    always_comb begin
        // default FSM outputs state
        rstFSM          = 1'b0;
        delayUp         = 1'b0;
        delaySave       = 1'b0;
        bitSlipRequest  = 1'b0;
        delayCountUp    = 1'b0;
        delayCountHalf  = 1'b0;
        delayDown       = 1'b0;
        delayCountDown  = 1'b0;
        FSMLocked       = 1'b0;
        waitRequest     = 1'b0;
        // FSM in reset 
        if (rstDelayed) begin
            rstFSM = 1'b1;
        end
        // output logic states depending on current state and input (Mealy)
        else begin
            case (State)
                s_Init: begin
                    rstFSM          = 1'b1;
                end
                    
                s_WaitBitScan: begin
                    waitRequest     = 1'b1;
                end
                
                s_BitScan: begin
                    // pattern matches, scan delay
                    if (patternFound) begin
                        delaySave      = 1'b1;
                        delayUp        = 1'b1;                       
                    end
                    else begin
                        // no pattern match found, continue bitslip scan
                        bitSlipRequest = 1'b1;
                        // all bitslip combinations tested, increase delay for the new bitslip scan 
                        if (bitScanComplete) 
                            delayUp        = 1'b1;
                    end
                end
             
                s_WaitDelayScan: begin
                    waitRequest     = 1'b1;
                end            
                       
                s_DelayScan: begin
                    // if pattern was still present and we are not at the end of the range
                    if (patternFound && (delay_tap_in != 5'b11111)) begin
                        // try one more
                        delayCountUp   = 1'b1;
                        delayUp        = 1'b1;
                    end
                    // if we are already fine with what we found
                    else 
                        if (delayMinMatchOK)
                            // return to the middle of the matching delay interval
                            delayCountHalf  = 1'b1;
                end

                s_DelayReturn:
                    begin
                        if (!delayCountZero) begin
                            delayDown      = 1'b1;
                            delayCountDown = 1'b1;
                        end
                        else begin
                            bitSlipRequest = 1'b1;
                        end
                    end 
                    
                s_DelayDec:
                    begin
                        if (!delayCountZero) begin
                            delayDown      = 1'b1;
                            delayCountDown = 1'b1;
                        end
                    end   
                                
                s_WaitCheck: begin
                    waitRequest     = 1'b1;
                end
                    
                s_Check:
                    begin
                        
                    end
                                
                s_Locked:  
                    begin
                        FSMLocked = 1'b1;
                    end
                                
            endcase
        end
    end


// force SERDES resync if no comma word is seen for some time (syncDelayReg counter)
    always_comb begin
        if (&syncDelayReg) 
            rstResync = 1;
        else  
            rstResync = 0;
    end  
// reset syncDelayReg counter each time a comma word is seen or if reset or sync is in progress
    always_ff @(posedge clk_slow) begin
        if (rstDelayed || !FSMLocked || patternFound)
            syncDelayReg <= 0;
        else
            syncDelayReg <= syncDelayReg + 1;
    end



    
endmodule
