----------------------------------------------------------------------------------
-- Company: BNL
-- Engineer: Hongbin Liu
-- 
-- Create Date: 08/05/2016 03:47:59 PM
-- Design Name: CaRIBOU
-- Module Name: h35demo_cfg_core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity h35demo_cfg_core is
    generic(
    INPUT_CLK_FRE  : INTEGER := 200_000_000; --INPUT CLOCK SPEED FROM USER LOGIC IN HZ
    CFG_CLK_FREQ   : INTEGER := 10_000_000);   --SPEED THE I2C BUS (SCL) WILL RUN AT IN HZ
    PORT (
    CLK             :IN STD_LOGIC;
    RST             :IN STD_LOGIC;
    
    START_FLG       :IN STD_LOGIC;
    REG_LIMIT       :IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    SHIFT_LIMIT     :IN STD_LOGIC_VECTOR(4 DOWNTO 0);
--    OUT_EN          :in std_logic_vector(3 downto 0);   
        
    RAM_ADDR        :IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    RAM_WR_DAT      :IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    RAM_RD_DAT      :OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    RAM_WR_EN       :IN STD_LOGIC;
    RAM_WR_CLK      :IN STD_LOGIC;
            
    SIN             :OUT STD_LOGIC;
    CK1             :OUT STD_LOGIC;
    CK2             :OUT STD_LOGIC;
    LD              :OUT STD_LOGIC
     );
end h35demo_cfg_core;

architecture Behavioral of h35demo_cfg_core is

constant divider  :  integer := (input_clk_fre/cfg_clk_freq)/2;
constant divider_2  :  integer := (input_clk_fre/(cfg_clk_freq*4))/2;
type ram_type is array(0 to 127) of std_logic_vector(31 downto 0);
signal DAT_RAM :ram_type := (others => (others => '0'));
--(
--X"11111111",X"22222222",X"33333333",X"44444444",
--X"55555555",X"66666666",X"77777777",X"88888888",
--X"99999999",X"aaaaaaaa",X"bbbbbbbb",X"cccccccc",
--X"dddddddd",X"eeeeeeee",X"ffffffff",X"00000000",
--X"11111111",X"22222222",X"33333333",X"44444444",
--X"55555555",X"66666666",X"77777777",X"88888888",
--X"99999999",X"aaaaaaaa",X"bbbbbbbb",X"cccccccc",
--X"dddddddd",X"eeeeeeee",X"ffffffff",X"00000000",
--X"11111111",X"22222222",X"33333333",X"44444444",
--X"55555555",X"66666666",X"77777777",X"88888888",
--X"99999999",X"aaaaaaaa",X"bbbbbbbb",X"cccccccc",
--X"dddddddd",X"eeeeeeee",X"ffffffff",X"00000000",
--X"11111111",X"22222222",X"33333333",X"44444444",
--X"55555555",X"66666666",X"77777777",X"88888888",
--X"99999999",X"aaaaaaaa",X"bbbbbbbb",X"cccccccc",
--X"dddddddd",X"eeeeeeee",X"ffffffff",X"00000000");

signal start_flg_prev :std_logic;
signal shift_cnt :integer range 0 to 31 := 0;
signal reg_cnt   :integer range 0 to 127 := 0;  -- Counter for # of word shift out
signal ld_cnt    :integer range 0 to 31 := 0;
signal shift_out_reg :std_logic_vector(31 downto 0);

signal reg_cnt_limit :integer range 0 to 127:= 0;
signal shift_cnt_limit:integer range 0 to 31 := 0;

TYPE machine IS(idle, start, shift1, shift2, load, finish); --needed states
signal state :machine;

signal cfg_clk_2:std_logic;
signal cfg_clk_3:std_logic;
signal cfg_clk_2_i:std_logic;
signal cfg_clk_3_i:std_logic;
signal cfg_clk_prev :std_logic;

signal cfg_clk_en :std_logic;

signal sin_i :std_logic  := '0';
signal sin_ii :std_logic := '0';
signal ckc_i :std_logic;
signal cko   :std_logic_vector(3 downto 0) := (others => '0');

signal ld_i  :std_logic;

--attribute MARK_DEBUG : string;
--attribute MARK_DEBUG of state,reg_cnt_limit,shift_cnt_limit,shift_cnt,reg_cnt  : signal is "TRUE";

signal ram_wr_en_r        : std_logic;
signal ram_wr_en_2r       : std_logic;
signal ram_addr_r         : std_logic_vector(6 downto 0);
signal ram_wr_dat_r       : std_logic_vector(31 downto 0);
signal reg_limit_r        : std_logic_vector(6 downto 0);
signal shift_limit_r      : std_logic_vector(4 downto 0);
signal start_flg_prev_i   : std_logic;

begin

RAM_RD_DAT <= DAT_RAM(conv_integer(RAM_ADDR));
data_ram:process(RAM_WR_CLK)
begin
  if rising_edge(RAM_WR_CLK) then
    ram_wr_en_r <= RAM_WR_EN;
    ram_wr_en_2r <= ram_wr_en_r;
    ram_addr_r <= RAM_ADDR;
    ram_wr_dat_r <= RAM_WR_DAT;
    if ram_wr_en_2r = '0' and ram_wr_en_r = '1' then  --ram_wr_en = '1' then
      DAT_RAM(conv_integer(ram_addr_r)) <= ram_wr_dat_r;
    end if;
  end if;
end process;

----generate the timing for the bus clock (scl_clk) and the data clock (data_clk)
--cfg_clk_gen:process(CLK, RST)
--  variable count  :  integer range 0 to divider*2;  --timing for clock generation
--begin
--  if(RST = '1') then                --reset asserted
--    count := 0;
--    cfg_clk <= '0';
--    cfg_clk_prev <= '0';
--  elsif rising_edge(CLK) then
--    cfg_clk_prev <= cfg_clk;            --store previous value of data clock
--    if(count = divider*2-1) then        --end of timing cycle
--      count := 0;                       --reset timer
--    else                                
--      count := count + 1;               --continue clock generation timing
--    end if;
    
--    case count is
--      when 0 TO divider-1 =>            --first 1/2 cycle of clocking
--        cfg_clk <= '0';
--      when divider TO divider*2-1 =>    --second 1/2 cycle of clocking
--        cfg_clk <= '1';       
--      when others =>                    
--        null;
--    end case;
    
--  end if;
--end process;

cfg_clk2_gen:process(CLK, RST)
  variable count  :  integer range 0 to divider_2*8;  --timing for clock generation
begin
  if(RST = '1') then                --reset asserted
    count := 0;
    cfg_clk_2 <= '0';
    cfg_clk_3 <= '0';
    cfg_clk_prev <= '0';
  elsif rising_edge(CLK) then
  cfg_clk_prev <= cfg_clk_3;   
    if(count = divider_2*8-1) then        --end of timing cycle
      count := 0;                       --reset timer
    else                                
      count := count + 1;               --continue clock generation timing
    end if;
    
    case count is
      when 0 TO divider_2-1 =>            --first 1/2 cycle of clocking
        cfg_clk_2 <= '0';
        cfg_clk_3 <= '1';
      when divider_2 TO divider_2*2-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '1';  
        cfg_clk_3 <= '1';     
      when divider_2*2 TO divider_2*3-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '0'; 
        cfg_clk_3 <= '0';
      when divider_2*3 TO divider_2*4-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '1'; 
        cfg_clk_3 <= '0';
      when divider_2*4 TO divider_2*5-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '0';
        cfg_clk_3 <= '0';
      when divider_2*5 TO divider_2*6-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '1';  
        cfg_clk_3 <= '0'; 
      when divider_2*6 TO divider_2*7-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '0';    
        cfg_clk_3 <= '0';
      when divider_2*7 TO divider_2*8-1 =>    --second 1/2 cycle of clocking
        cfg_clk_2 <= '1';   
        cfg_clk_3 <= '0';   
      when others =>                    
        null;
    end case;
  end if;
end process;

process(CLK)
begin
if rising_edge(CLK) then
  shift_limit_r <= SHIFT_LIMIT;
  reg_limit_r <= REG_LIMIT;
end if;
end process;

shift_fsm:process(CLK, RST)
begin
if RST = '1' then
    state <= idle;
    shift_cnt <= 0;
    reg_cnt <= 0;
    shift_out_reg <= (others => '0');
    sin_i <= '0';
    ld_i  <= '0';
    cfg_clk_en <= '0';
    ld_cnt <= 0;
    
elsif rising_edge(CLK) then
  if cfg_clk_3 = '1' and cfg_clk_prev = '0' then
    start_flg_prev <= START_FLG;
    start_flg_prev_i <= start_flg_prev;
      
    case state is     
      when idle =>
        ld_i <= '0';
        if start_flg_prev_i = '0' and start_flg_prev = '1' then
          state <= start;
          shift_cnt_limit <= conv_integer(shift_limit_r);
          reg_cnt_limit <= conv_integer(reg_limit_r);         
          shift_cnt <= 0;
          reg_cnt <= 0;
          ld_cnt <= 0;
        end if;
        cfg_clk_en <= '0';
        
      when start =>
        cfg_clk_en <= '1';
        if reg_cnt_limit = 0 then
            shift_out_reg <= DAT_RAM(0);
            sin_i <= DAT_RAM(0)(0);
            state <= shift2;
        else
            shift_out_reg <= DAT_RAM(0);
            sin_i <= DAT_RAM(0)(0);
            state <= shift1;
        end if;
      
      when shift1 =>

      if shift_cnt = 31 then
        if reg_cnt = reg_cnt_limit - 1 then
          if shift_cnt_limit = 0 then
            state <= load;
            sin_i <= '0';
            cfg_clk_en <= '0';
            ld_cnt <= 0;
          else
            state <= shift2;
            shift_cnt <= 0;
            sin_i <= DAT_RAM(reg_cnt_limit)(0);
            shift_out_reg <= DAT_RAM(reg_cnt_limit);
          end if; 
        else 
          reg_cnt <= reg_cnt + 1;
          sin_i <= DAT_RAM(reg_cnt+1)(0);
          shift_cnt <= 0;
          shift_out_reg <= DAT_RAM(reg_cnt+1);
        end if;
      else
        sin_i <= shift_out_reg(shift_cnt+1);
        shift_cnt <= shift_cnt +1;
      end if;      
      
      when shift2 =>
        if shift_cnt = shift_cnt_limit - 1 then
           state <= load;
           sin_i <= '0';
           cfg_clk_en <= '0';
           ld_cnt <= 0;
        else
           shift_cnt <= shift_cnt +1;
           sin_i <= shift_out_reg(shift_cnt+1);
        end if;
      
      when load =>
        if ld_cnt = 5 then
            ld_i    <= '0';
            state <= finish;
        else
            ld_i    <= '1';
            ld_cnt <= ld_cnt + 1;
        end if;
        
      when finish =>
        ld_i    <= '0';
        state <= idle;
        ld_cnt <= 0;
            
      when others =>  
       null;
      
      end case;
  end if;
  end if;
end process;

process(CLK, RST)
begin
  if RST = '1' then
    cfg_clk_2_i <= '0';
    cfg_clk_3_i <= '0';
  elsif rising_edge(CLK) then
    cfg_clk_2_i <= cfg_clk_2;
    cfg_clk_3_i <= cfg_clk_3;
  end if;
end process;

ckc_i <= cfg_clk_3_i when (cfg_clk_en = '1') else '0';

process(cfg_clk_2_i, RST)
begin
  if RST = '1' then
    sin_ii <= '0';
  elsif rising_edge(cfg_clk_2_i) then
    sin_ii <= sin_i;
  end if;
end process;

process(cfg_clk_2_i, RST)
begin
    if rst = '1' then
--        sin_ii <= '0';
        cko(3 downto 1)    <= (others => '0');
    elsif rising_edge(cfg_clk_2_i) then
        cko <= cko( 2 downto 0) & ckc_i;
--        sin_ii <= sin_i;
    end if;
end process;

CK1 <=  cko(0);
CK2 <=  cko(2);
SIN <=  sin_i;
LD  <=  ld_i;

end Behavioral;