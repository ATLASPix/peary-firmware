//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
//Date        : Tue Aug 14 15:04:20 2018
//Host        : pc-unige-peary running 64-bit Ubuntu 16.04.4 LTS
//Command     : generate_target caribou_top_wrapper.bd
//Design      : caribou_top_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module caribou_top_wrapper
   (ATLASPix_M1ISO_ClkExt_N,
    ATLASPix_M1ISO_ClkExt_P,
    ATLASPix_M1ISO_ClkRef_N,
    ATLASPix_M1ISO_ClkRef_P,
    ATLASPix_M1_ClkExt_N,
    ATLASPix_M1_ClkExt_P,
    ATLASPix_M1_ClkRef_N,
    ATLASPix_M1_ClkRef_P,
    ATLASPix_M2_ClkExt_N,
    ATLASPix_M2_ClkExt_P,
    ATLASPix_M2_ClkRef_N,
    ATLASPix_M2_ClkRef_P,
    CLK_SI_8_N,
    CLK_SI_8_P,
    DATA_IN_M1_n,
    DATA_IN_M1_p,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    EXT_CK1_M1ISO_N,
    EXT_CK1_M1ISO_P,
    EXT_CK1_M1_N,
    EXT_CK1_M1_P,
    EXT_CK1_M2_N,
    EXT_CK1_M2_P,
    EXT_CK2_M1ISO_N,
    EXT_CK2_M1ISO_P,
    EXT_CK2_M1_N,
    EXT_CK2_M1_P,
    EXT_CK2_M2_N,
    EXT_CK2_M2_P,
    EXT_LDM1ISO_N,
    EXT_LDM1ISO_P,
    EXT_LDM1_N,
    EXT_LDM1_P,
    EXT_LDM2_N,
    EXT_LDM2_P,
    EXT_PULSE_OUT_1_N,
    EXT_PULSE_OUT_1_P,
    EXT_PULSE_OUT_2_N,
    EXT_PULSE_OUT_2_P,
    EXT_PULSE_OUT_3_N,
    EXT_PULSE_OUT_3_P,
    EXT_SIN_M1ISO_N,
    EXT_SIN_M1ISO_P,
    EXT_SIN_M1_N,
    EXT_SIN_M1_P,
    EXT_SIN_M2_N,
    EXT_SIN_M2_P,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    T0_in_N,
    T0_in_P,
    busy_out,
    ext_ATLASPix_Sync_reset_M1ISO_N,
    ext_ATLASPix_Sync_reset_M1ISO_P,
    ext_ATLASPix_Sync_reset_M1_N,
    ext_ATLASPix_Sync_reset_M1_P,
    ext_ATLASPix_Sync_reset_M2_N,
    ext_ATLASPix_Sync_reset_M2_P,
    led0,
    reset,
    trigger_in);
  output [0:0]ATLASPix_M1ISO_ClkExt_N;
  output [0:0]ATLASPix_M1ISO_ClkExt_P;
  output [0:0]ATLASPix_M1ISO_ClkRef_N;
  output [0:0]ATLASPix_M1ISO_ClkRef_P;
  output [0:0]ATLASPix_M1_ClkExt_N;
  output [0:0]ATLASPix_M1_ClkExt_P;
  output [0:0]ATLASPix_M1_ClkRef_N;
  output [0:0]ATLASPix_M1_ClkRef_P;
  output [0:0]ATLASPix_M2_ClkExt_N;
  output [0:0]ATLASPix_M2_ClkExt_P;
  output [0:0]ATLASPix_M2_ClkRef_N;
  output [0:0]ATLASPix_M2_ClkRef_P;
  input CLK_SI_8_N;
  input CLK_SI_8_P;
  input DATA_IN_M1_n;
  input DATA_IN_M1_p;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  output EXT_CK1_M1ISO_N;
  output EXT_CK1_M1ISO_P;
  output EXT_CK1_M1_N;
  output EXT_CK1_M1_P;
  output EXT_CK1_M2_N;
  output EXT_CK1_M2_P;
  output EXT_CK2_M1ISO_N;
  output EXT_CK2_M1ISO_P;
  output EXT_CK2_M1_N;
  output EXT_CK2_M1_P;
  output EXT_CK2_M2_N;
  output EXT_CK2_M2_P;
  output EXT_LDM1ISO_N;
  output EXT_LDM1ISO_P;
  output EXT_LDM1_N;
  output EXT_LDM1_P;
  output EXT_LDM2_N;
  output EXT_LDM2_P;
  output [0:0]EXT_PULSE_OUT_1_N;
  output [0:0]EXT_PULSE_OUT_1_P;
  output [0:0]EXT_PULSE_OUT_2_N;
  output [0:0]EXT_PULSE_OUT_2_P;
  output [0:0]EXT_PULSE_OUT_3_N;
  output [0:0]EXT_PULSE_OUT_3_P;
  output EXT_SIN_M1ISO_N;
  output EXT_SIN_M1ISO_P;
  output EXT_SIN_M1_N;
  output EXT_SIN_M1_P;
  output EXT_SIN_M2_N;
  output EXT_SIN_M2_P;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input T0_in_N;
  input T0_in_P;
  output busy_out;
  output [0:0]ext_ATLASPix_Sync_reset_M1ISO_N;
  output [0:0]ext_ATLASPix_Sync_reset_M1ISO_P;
  output [0:0]ext_ATLASPix_Sync_reset_M1_N;
  output [0:0]ext_ATLASPix_Sync_reset_M1_P;
  output [0:0]ext_ATLASPix_Sync_reset_M2_N;
  output [0:0]ext_ATLASPix_Sync_reset_M2_P;
  output led0;
  input reset;
  input trigger_in;

  wire [0:0]ATLASPix_M1ISO_ClkExt_N;
  wire [0:0]ATLASPix_M1ISO_ClkExt_P;
  wire [0:0]ATLASPix_M1ISO_ClkRef_N;
  wire [0:0]ATLASPix_M1ISO_ClkRef_P;
  wire [0:0]ATLASPix_M1_ClkExt_N;
  wire [0:0]ATLASPix_M1_ClkExt_P;
  wire [0:0]ATLASPix_M1_ClkRef_N;
  wire [0:0]ATLASPix_M1_ClkRef_P;
  wire [0:0]ATLASPix_M2_ClkExt_N;
  wire [0:0]ATLASPix_M2_ClkExt_P;
  wire [0:0]ATLASPix_M2_ClkRef_N;
  wire [0:0]ATLASPix_M2_ClkRef_P;
  wire CLK_SI_8_N;
  wire CLK_SI_8_P;
  wire DATA_IN_M1_n;
  wire DATA_IN_M1_p;
  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire EXT_CK1_M1ISO_N;
  wire EXT_CK1_M1ISO_P;
  wire EXT_CK1_M1_N;
  wire EXT_CK1_M1_P;
  wire EXT_CK1_M2_N;
  wire EXT_CK1_M2_P;
  wire EXT_CK2_M1ISO_N;
  wire EXT_CK2_M1ISO_P;
  wire EXT_CK2_M1_N;
  wire EXT_CK2_M1_P;
  wire EXT_CK2_M2_N;
  wire EXT_CK2_M2_P;
  wire EXT_LDM1ISO_N;
  wire EXT_LDM1ISO_P;
  wire EXT_LDM1_N;
  wire EXT_LDM1_P;
  wire EXT_LDM2_N;
  wire EXT_LDM2_P;
  wire [0:0]EXT_PULSE_OUT_1_N;
  wire [0:0]EXT_PULSE_OUT_1_P;
  wire [0:0]EXT_PULSE_OUT_2_N;
  wire [0:0]EXT_PULSE_OUT_2_P;
  wire [0:0]EXT_PULSE_OUT_3_N;
  wire [0:0]EXT_PULSE_OUT_3_P;
  wire EXT_SIN_M1ISO_N;
  wire EXT_SIN_M1ISO_P;
  wire EXT_SIN_M1_N;
  wire EXT_SIN_M1_P;
  wire EXT_SIN_M2_N;
  wire EXT_SIN_M2_P;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire T0_in_N;
  wire T0_in_P;
  wire busy_out;
  wire [0:0]ext_ATLASPix_Sync_reset_M1ISO_N;
  wire [0:0]ext_ATLASPix_Sync_reset_M1ISO_P;
  wire [0:0]ext_ATLASPix_Sync_reset_M1_N;
  wire [0:0]ext_ATLASPix_Sync_reset_M1_P;
  wire [0:0]ext_ATLASPix_Sync_reset_M2_N;
  wire [0:0]ext_ATLASPix_Sync_reset_M2_P;
  wire led0;
  wire reset;
  wire trigger_in;

  caribou_top caribou_top_i
       (.ATLASPix_M1ISO_ClkExt_N(ATLASPix_M1ISO_ClkExt_N),
        .ATLASPix_M1ISO_ClkExt_P(ATLASPix_M1ISO_ClkExt_P),
        .ATLASPix_M1ISO_ClkRef_N(ATLASPix_M1ISO_ClkRef_N),
        .ATLASPix_M1ISO_ClkRef_P(ATLASPix_M1ISO_ClkRef_P),
        .ATLASPix_M1_ClkExt_N(ATLASPix_M1_ClkExt_N),
        .ATLASPix_M1_ClkExt_P(ATLASPix_M1_ClkExt_P),
        .ATLASPix_M1_ClkRef_N(ATLASPix_M1_ClkRef_N),
        .ATLASPix_M1_ClkRef_P(ATLASPix_M1_ClkRef_P),
        .ATLASPix_M2_ClkExt_N(ATLASPix_M2_ClkExt_N),
        .ATLASPix_M2_ClkExt_P(ATLASPix_M2_ClkExt_P),
        .ATLASPix_M2_ClkRef_N(ATLASPix_M2_ClkRef_N),
        .ATLASPix_M2_ClkRef_P(ATLASPix_M2_ClkRef_P),
        .CLK_SI_8_N(CLK_SI_8_N),
        .CLK_SI_8_P(CLK_SI_8_P),
        .DATA_IN_M1_n(DATA_IN_M1_n),
        .DATA_IN_M1_p(DATA_IN_M1_p),
        .DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .EXT_CK1_M1ISO_N(EXT_CK1_M1ISO_N),
        .EXT_CK1_M1ISO_P(EXT_CK1_M1ISO_P),
        .EXT_CK1_M1_N(EXT_CK1_M1_N),
        .EXT_CK1_M1_P(EXT_CK1_M1_P),
        .EXT_CK1_M2_N(EXT_CK1_M2_N),
        .EXT_CK1_M2_P(EXT_CK1_M2_P),
        .EXT_CK2_M1ISO_N(EXT_CK2_M1ISO_N),
        .EXT_CK2_M1ISO_P(EXT_CK2_M1ISO_P),
        .EXT_CK2_M1_N(EXT_CK2_M1_N),
        .EXT_CK2_M1_P(EXT_CK2_M1_P),
        .EXT_CK2_M2_N(EXT_CK2_M2_N),
        .EXT_CK2_M2_P(EXT_CK2_M2_P),
        .EXT_LDM1ISO_N(EXT_LDM1ISO_N),
        .EXT_LDM1ISO_P(EXT_LDM1ISO_P),
        .EXT_LDM1_N(EXT_LDM1_N),
        .EXT_LDM1_P(EXT_LDM1_P),
        .EXT_LDM2_N(EXT_LDM2_N),
        .EXT_LDM2_P(EXT_LDM2_P),
        .EXT_PULSE_OUT_1_N(EXT_PULSE_OUT_1_N),
        .EXT_PULSE_OUT_1_P(EXT_PULSE_OUT_1_P),
        .EXT_PULSE_OUT_2_N(EXT_PULSE_OUT_2_N),
        .EXT_PULSE_OUT_2_P(EXT_PULSE_OUT_2_P),
        .EXT_PULSE_OUT_3_N(EXT_PULSE_OUT_3_N),
        .EXT_PULSE_OUT_3_P(EXT_PULSE_OUT_3_P),
        .EXT_SIN_M1ISO_N(EXT_SIN_M1ISO_N),
        .EXT_SIN_M1ISO_P(EXT_SIN_M1ISO_P),
        .EXT_SIN_M1_N(EXT_SIN_M1_N),
        .EXT_SIN_M1_P(EXT_SIN_M1_P),
        .EXT_SIN_M2_N(EXT_SIN_M2_N),
        .EXT_SIN_M2_P(EXT_SIN_M2_P),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .T0_in_N(T0_in_N),
        .T0_in_P(T0_in_P),
        .busy_out(busy_out),
        .ext_ATLASPix_Sync_reset_M1ISO_N(ext_ATLASPix_Sync_reset_M1ISO_N),
        .ext_ATLASPix_Sync_reset_M1ISO_P(ext_ATLASPix_Sync_reset_M1ISO_P),
        .ext_ATLASPix_Sync_reset_M1_N(ext_ATLASPix_Sync_reset_M1_N),
        .ext_ATLASPix_Sync_reset_M1_P(ext_ATLASPix_Sync_reset_M1_P),
        .ext_ATLASPix_Sync_reset_M2_N(ext_ATLASPix_Sync_reset_M2_N),
        .ext_ATLASPix_Sync_reset_M2_P(ext_ATLASPix_Sync_reset_M2_P),
        .led0(led0),
        .reset(reset),
        .trigger_in(trigger_in));
endmodule
