--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
--Date        : Mon Oct  9 10:24:10 2017
--Host        : ubuntu running 64-bit Ubuntu 16.04.3 LTS
--Command     : generate_target caribou_top_wrapper.bd
--Design      : caribou_top_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity caribou_top_wrapper is
  port (
    ATLASPix_M1ISO_PLL_CLK_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M1ISO_PLL_CLK_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M1_PLL_CLK_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M1_PLL_CLK_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M2_PLL_CLK_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M2_PLL_CLK_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1ISO_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1ISO_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M2_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M2_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    EXT_PULSE_OUT_1_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_1_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_2_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_2_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_3_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_3_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    FAST_CLK_OUT_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    FAST_CLK_OUT_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    MON1_IN : in STD_LOGIC;
    MON2_IN : in STD_LOGIC;
    SI5345_CLK_OUT8_clk_n : in STD_LOGIC;
    SI5345_CLK_OUT8_clk_p : in STD_LOGIC;
    ext_ATLASPix_Sync_reset_M1ISO_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1ISO_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M2_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M2_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1ISO_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1ISO_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M2_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M2_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ck1_diff_N : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ck1_diff_P : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ck2_diff_N : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ck2_diff_P : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ldM1ISO_diff_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldM1ISO_diff_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldM2_diff_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldM2_diff_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldm1_diff_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldm1_diff_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_sin_diff_N : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_sin_diff_P : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end caribou_top_wrapper;

architecture STRUCTURE of caribou_top_wrapper is
  component caribou_top is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    SI5345_CLK_OUT8_clk_n : in STD_LOGIC;
    SI5345_CLK_OUT8_clk_p : in STD_LOGIC;
    ext_ldM2_diff_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M2_PLL_CLK_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M2_PLL_CLK_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldM2_diff_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ck2_diff_N : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ck2_diff_P : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ck1_diff_P : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ck1_diff_N : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_sin_diff_N : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_sin_diff_P : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ext_ldM1ISO_diff_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldM1ISO_diff_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldm1_diff_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ldm1_diff_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_1_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_1_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_2_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_2_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_3_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_PULSE_OUT_3_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    MON1_IN : in STD_LOGIC;
    MON2_IN : in STD_LOGIC;
    ATLASPix_M1_PLL_CLK_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M1_PLL_CLK_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M1ISO_PLL_CLK_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ATLASPix_M1ISO_PLL_CLK_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1ISO_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    FAST_CLK_OUT_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    FAST_CLK_OUT_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M2_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1ISO_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M1_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Sync_reset_M2_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1ISO_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M1ISO_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M2_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN_M2_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1ISO_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1ISO_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M2_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    ext_ATLASPix_Trigger_M2_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component caribou_top;
begin
caribou_top_i: component caribou_top
     port map (
      ATLASPix_M1ISO_PLL_CLK_N(0) => ATLASPix_M1ISO_PLL_CLK_N(0),
      ATLASPix_M1ISO_PLL_CLK_P(0) => ATLASPix_M1ISO_PLL_CLK_P(0),
      ATLASPix_M1_PLL_CLK_N(0) => ATLASPix_M1_PLL_CLK_N(0),
      ATLASPix_M1_PLL_CLK_P(0) => ATLASPix_M1_PLL_CLK_P(0),
      ATLASPix_M2_PLL_CLK_N(0) => ATLASPix_M2_PLL_CLK_N(0),
      ATLASPix_M2_PLL_CLK_P(0) => ATLASPix_M2_PLL_CLK_P(0),
      DATA_IN_M1ISO_clk_n(0) => DATA_IN_M1ISO_clk_n(0),
      DATA_IN_M1ISO_clk_p(0) => DATA_IN_M1ISO_clk_p(0),
      DATA_IN_M1_clk_n(0) => DATA_IN_M1_clk_n(0),
      DATA_IN_M1_clk_p(0) => DATA_IN_M1_clk_p(0),
      DATA_IN_M2_clk_n(0) => DATA_IN_M2_clk_n(0),
      DATA_IN_M2_clk_p(0) => DATA_IN_M2_clk_p(0),
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      EXT_PULSE_OUT_1_N(0) => EXT_PULSE_OUT_1_N(0),
      EXT_PULSE_OUT_1_P(0) => EXT_PULSE_OUT_1_P(0),
      EXT_PULSE_OUT_2_N(0) => EXT_PULSE_OUT_2_N(0),
      EXT_PULSE_OUT_2_P(0) => EXT_PULSE_OUT_2_P(0),
      EXT_PULSE_OUT_3_N(0) => EXT_PULSE_OUT_3_N(0),
      EXT_PULSE_OUT_3_P(0) => EXT_PULSE_OUT_3_P(0),
      FAST_CLK_OUT_N(0) => FAST_CLK_OUT_N(0),
      FAST_CLK_OUT_P(0) => FAST_CLK_OUT_P(0),
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      MON1_IN => MON1_IN,
      MON2_IN => MON2_IN,
      SI5345_CLK_OUT8_clk_n => SI5345_CLK_OUT8_clk_n,
      SI5345_CLK_OUT8_clk_p => SI5345_CLK_OUT8_clk_p,
      ext_ATLASPix_Sync_reset_M1ISO_N(0) => ext_ATLASPix_Sync_reset_M1ISO_N(0),
      ext_ATLASPix_Sync_reset_M1ISO_P(0) => ext_ATLASPix_Sync_reset_M1ISO_P(0),
      ext_ATLASPix_Sync_reset_M1_N(0) => ext_ATLASPix_Sync_reset_M1_N(0),
      ext_ATLASPix_Sync_reset_M1_P(0) => ext_ATLASPix_Sync_reset_M1_P(0),
      ext_ATLASPix_Sync_reset_M2_N(0) => ext_ATLASPix_Sync_reset_M2_N(0),
      ext_ATLASPix_Sync_reset_M2_P(0) => ext_ATLASPix_Sync_reset_M2_P(0),
      ext_ATLASPix_Trigger_M1ISO_clk_n(0) => ext_ATLASPix_Trigger_M1ISO_clk_n(0),
      ext_ATLASPix_Trigger_M1ISO_clk_p(0) => ext_ATLASPix_Trigger_M1ISO_clk_p(0),
      ext_ATLASPix_Trigger_M1_clk_n(0) => ext_ATLASPix_Trigger_M1_clk_n(0),
      ext_ATLASPix_Trigger_M1_clk_p(0) => ext_ATLASPix_Trigger_M1_clk_p(0),
      ext_ATLASPix_Trigger_M2_clk_n(0) => ext_ATLASPix_Trigger_M2_clk_n(0),
      ext_ATLASPix_Trigger_M2_clk_p(0) => ext_ATLASPix_Trigger_M2_clk_p(0),
      ext_ck1_diff_N(2 downto 0) => ext_ck1_diff_N(2 downto 0),
      ext_ck1_diff_P(2 downto 0) => ext_ck1_diff_P(2 downto 0),
      ext_ck2_diff_N(2 downto 0) => ext_ck2_diff_N(2 downto 0),
      ext_ck2_diff_P(2 downto 0) => ext_ck2_diff_P(2 downto 0),
      ext_ldM1ISO_diff_N(0) => ext_ldM1ISO_diff_N(0),
      ext_ldM1ISO_diff_P(0) => ext_ldM1ISO_diff_P(0),
      ext_ldM2_diff_N(0) => ext_ldM2_diff_N(0),
      ext_ldM2_diff_P(0) => ext_ldM2_diff_P(0),
      ext_ldm1_diff_N(0) => ext_ldm1_diff_N(0),
      ext_ldm1_diff_P(0) => ext_ldm1_diff_P(0),
      ext_sin_diff_N(2 downto 0) => ext_sin_diff_N(2 downto 0),
      ext_sin_diff_P(2 downto 0) => ext_sin_diff_P(2 downto 0)
    );
end STRUCTURE;
