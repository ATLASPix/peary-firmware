################################################################################

# This XDC is used only for OOC mode of synthesis, implementation
# This constraints file contains default clock frequencies to be used during
# out-of-context flows such as OOC Synthesis and Hierarchical Designs.
# This constraints file is not used in normal top-down synthesis (default flow
# of Vivado)
################################################################################
create_clock -name CLK_SI_8_N -period 10 [get_ports CLK_SI_8_N]
create_clock -name CLK_SI_8_P -period 10 [get_ports CLK_SI_8_P]
create_clock -name ZynqPSandAXIStuff_processing_system7_0_FCLK_CLK0 -period 5 [get_pins ZynqPSandAXIStuff/processing_system7_0/FCLK_CLK0]

################################################################################