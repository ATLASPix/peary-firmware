
################################################################
# This is a generated script based on design: caribou_top
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source caribou_top_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# SignalSyncer, sync_reset_1, xil_ibufds, decode, SignalSyncer, xil_obufds, xil_obufds, xil_obufds, sync_reset_1

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z045ffg900-2
   set_property BOARD_PART xilinx.com:zc706:part0:1.4 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name caribou_top

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir ./ipi-bd

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
  }
}

current_bd_design $design_name

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: buffer_Pulser
proc create_hier_cell_buffer_Pulser { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_buffer_Pulser() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N2
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N3
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N4
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P2
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P3
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P4
  create_bd_pin -dir I -from 0 -to 0 -type clk OBUF_IN2
  create_bd_pin -dir I -from 0 -to 0 -type clk OBUF_IN3
  create_bd_pin -dir I -from 0 -to 0 -type clk OBUF_IN4

  # Create instance: util_ds_buf_7, and set properties
  set util_ds_buf_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_7 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_7

  # Create instance: util_ds_buf_8, and set properties
  set util_ds_buf_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_8 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_8

  # Create instance: util_ds_buf_9, and set properties
  set util_ds_buf_9 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_9 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_9

  # Create port connections
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_1 [get_bd_pins OBUF_IN4] [get_bd_pins util_ds_buf_7/OBUF_IN]
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_2 [get_bd_pins OBUF_IN2] [get_bd_pins util_ds_buf_8/OBUF_IN]
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_3 [get_bd_pins OBUF_IN3] [get_bd_pins util_ds_buf_9/OBUF_IN]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_N [get_bd_pins OBUF_DS_N4] [get_bd_pins util_ds_buf_7/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_P [get_bd_pins OBUF_DS_P4] [get_bd_pins util_ds_buf_7/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_N [get_bd_pins OBUF_DS_N2] [get_bd_pins util_ds_buf_8/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_P [get_bd_pins OBUF_DS_P2] [get_bd_pins util_ds_buf_8/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_N [get_bd_pins OBUF_DS_N3] [get_bd_pins util_ds_buf_9/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_P [get_bd_pins OBUF_DS_P3] [get_bd_pins util_ds_buf_9/OBUF_DS_P]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ATLASPix_Readout_DataSync
proc create_hier_cell_ATLASPix_Readout_DataSync { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ATLASPix_Readout_DataSync() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -type clk CLK
  create_bd_pin -dir I -from 0 -to 0 Op1
  create_bd_pin -dir I arst_in
  create_bd_pin -dir I -type clk clk_in
  create_bd_pin -dir I -from 0 -to 0 data_in_from_pins_n
  create_bd_pin -dir I -from 0 -to 0 data_in_from_pins_p
  create_bd_pin -dir O -from 9 -to 0 -type data data_in_to_device
  create_bd_pin -dir O locked1
  create_bd_pin -dir I ref_clock

  # Create instance: c_counter_binary_0, and set properties
  set c_counter_binary_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_counter_binary:12.0 c_counter_binary_0 ]
  set_property -dict [ list \
   CONFIG.CE {true} \
   CONFIG.Load {false} \
   CONFIG.Output_Width {5} \
   CONFIG.Restrict_Count {false} \
   CONFIG.SCLR {true} \
   CONFIG.Sync_Threshold_Output {true} \
   CONFIG.Threshold_Value {1E} \
 ] $c_counter_binary_0

  # Create instance: selectio_wiz_0, and set properties
  set selectio_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:selectio_wiz:5.1 selectio_wiz_0 ]
  set_property -dict [ list \
   CONFIG.BUS_IO_STD {LVDS_25} \
   CONFIG.BUS_SIG_TYPE {DIFF} \
   CONFIG.CLK_EN {false} \
   CONFIG.CLK_FWD_IO_STD {LVDS_25} \
   CONFIG.CLK_FWD_SIG_TYPE {DIFF} \
   CONFIG.SELIO_ACTIVE_EDGE {DDR} \
   CONFIG.SELIO_BUS_IN_DELAY {VAR_LOADABLE} \
   CONFIG.SELIO_CLK_BUF {MMCM} \
   CONFIG.SELIO_CLK_IO_STD {LVDS_25} \
   CONFIG.SELIO_CLK_SIG_TYPE {DIFF} \
   CONFIG.SELIO_INTERFACE_TYPE {NETWORKING} \
   CONFIG.SERIALIZATION_FACTOR {10} \
   CONFIG.SYSTEM_DATA_WIDTH {1} \
   CONFIG.USE_SERIALIZATION {true} \
 ] $selectio_wiz_0

  # Create instance: serdes_sync_0, and set properties
  set serdes_sync_0 [ create_bd_cell -type ip -vlnv cern.ch:user:serdes_sync:1.0 serdes_sync_0 ]

  # Create instance: sync_reset_1_0, and set properties
  set block_name sync_reset_1
  set block_cell_name sync_reset_1_0
  if { [catch {set sync_reset_1_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $sync_reset_1_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.NEGATIVE_IN {1} \
 ] $sync_reset_1_0

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_2, and set properties
  set util_vector_logic_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_2 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_2

  # Create instance: util_vector_logic_3, and set properties
  set util_vector_logic_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_3 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {or} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_orgate.png} \
 ] $util_vector_logic_3

  # Create instance: xlconstant_2, and set properties
  set xlconstant_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_2 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {5} \
 ] $xlconstant_2

  # Create port connections
  connect_bd_net -net Op1_1 [get_bd_pins Op1] [get_bd_pins util_vector_logic_3/Op1]
  connect_bd_net -net arst_in_1 [get_bd_pins arst_in] [get_bd_pins sync_reset_1_0/arst_in]
  connect_bd_net -net c_counter_binary_0_THRESH0 [get_bd_pins c_counter_binary_0/THRESH0] [get_bd_pins util_vector_logic_0/Op1]
  connect_bd_net -net clk_wiz_0_DIN_CLK [get_bd_pins clk_in] [get_bd_pins selectio_wiz_0/clk_in]
  connect_bd_net -net clk_wiz_0_PDATA_CLK [get_bd_pins CLK] [get_bd_pins c_counter_binary_0/CLK] [get_bd_pins selectio_wiz_0/clk_div_in] [get_bd_pins serdes_sync_0/clk_slow] [get_bd_pins sync_reset_1_0/clk]
  connect_bd_net -net data_in_from_pins_n_1 [get_bd_pins data_in_from_pins_n] [get_bd_pins selectio_wiz_0/data_in_from_pins_n]
  connect_bd_net -net data_in_from_pins_p_1 [get_bd_pins data_in_from_pins_p] [get_bd_pins selectio_wiz_0/data_in_from_pins_p]
  connect_bd_net -net ref_clock_1 [get_bd_pins ref_clock] [get_bd_pins selectio_wiz_0/ref_clock]
  connect_bd_net -net selectio_wiz_0_data_in_to_device [get_bd_pins data_in_to_device] [get_bd_pins selectio_wiz_0/data_in_to_device] [get_bd_pins serdes_sync_0/data_in]
  connect_bd_net -net selectio_wiz_0_delay_locked [get_bd_pins selectio_wiz_0/delay_locked] [get_bd_pins util_vector_logic_2/Op1]
  connect_bd_net -net selectio_wiz_0_in_delay_tap_out [get_bd_pins selectio_wiz_0/in_delay_tap_out] [get_bd_pins serdes_sync_0/delay_tap_in]
  connect_bd_net -net serdes_sync_0_bitslip [get_bd_pins selectio_wiz_0/bitslip] [get_bd_pins serdes_sync_0/bitslip]
  connect_bd_net -net serdes_sync_0_delay_ce [get_bd_pins selectio_wiz_0/in_delay_data_ce] [get_bd_pins serdes_sync_0/delay_ce]
  connect_bd_net -net serdes_sync_0_delay_inc [get_bd_pins selectio_wiz_0/in_delay_data_inc] [get_bd_pins serdes_sync_0/delay_inc]
  connect_bd_net -net serdes_sync_0_delay_rst [get_bd_pins selectio_wiz_0/in_delay_reset] [get_bd_pins serdes_sync_0/delay_rst]
  connect_bd_net -net serdes_sync_0_locked [get_bd_pins locked1] [get_bd_pins serdes_sync_0/locked]
  connect_bd_net -net sync_reset_1_0_rst_out [get_bd_pins sync_reset_1_0/rst_out] [get_bd_pins util_vector_logic_3/Op2]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins c_counter_binary_0/CE] [get_bd_pins selectio_wiz_0/io_reset] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net util_vector_logic_2_Res [get_bd_pins serdes_sync_0/rst] [get_bd_pins util_vector_logic_2/Res]
  connect_bd_net -net util_vector_logic_3_Res [get_bd_pins c_counter_binary_0/SCLR] [get_bd_pins util_vector_logic_3/Res]
  connect_bd_net -net xlconstant_2_dout [get_bd_pins selectio_wiz_0/in_delay_tap_in] [get_bd_pins xlconstant_2/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: clock_buffers
proc create_hier_cell_clock_buffers { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_clock_buffers() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N1
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N2
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N3
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N4
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N5
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P1
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P2
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P3
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P4
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P5
  create_bd_pin -dir I -from 0 -to 0 -type clk OBUF_IN

  # Create instance: util_ds_buf_7, and set properties
  set util_ds_buf_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_7 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_7

  # Create instance: util_ds_buf_8, and set properties
  set util_ds_buf_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_8 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_8

  # Create instance: util_ds_buf_9, and set properties
  set util_ds_buf_9 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_9 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_9

  # Create instance: util_ds_buf_10, and set properties
  set util_ds_buf_10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_10 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_10

  # Create instance: util_ds_buf_11, and set properties
  set util_ds_buf_11 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_11 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_11

  # Create instance: util_ds_buf_12, and set properties
  set util_ds_buf_12 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_12 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
   CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_12

  # Create port connections
  connect_bd_net -net OBUF_IN_1 [get_bd_pins OBUF_IN] [get_bd_pins util_ds_buf_10/OBUF_IN] [get_bd_pins util_ds_buf_11/OBUF_IN] [get_bd_pins util_ds_buf_12/OBUF_IN] [get_bd_pins util_ds_buf_7/OBUF_IN] [get_bd_pins util_ds_buf_8/OBUF_IN] [get_bd_pins util_ds_buf_9/OBUF_IN]
  connect_bd_net -net util_ds_buf_10_OBUF_DS_N [get_bd_pins OBUF_DS_N3] [get_bd_pins util_ds_buf_10/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_10_OBUF_DS_P [get_bd_pins OBUF_DS_P3] [get_bd_pins util_ds_buf_10/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_11_OBUF_DS_N [get_bd_pins OBUF_DS_N4] [get_bd_pins util_ds_buf_11/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_11_OBUF_DS_P [get_bd_pins OBUF_DS_P4] [get_bd_pins util_ds_buf_11/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_12_OBUF_DS_N [get_bd_pins OBUF_DS_N5] [get_bd_pins util_ds_buf_12/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_12_OBUF_DS_P [get_bd_pins OBUF_DS_P5] [get_bd_pins util_ds_buf_12/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_N1 [get_bd_pins OBUF_DS_N2] [get_bd_pins util_ds_buf_7/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_P1 [get_bd_pins OBUF_DS_P2] [get_bd_pins util_ds_buf_7/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_N1 [get_bd_pins OBUF_DS_N] [get_bd_pins util_ds_buf_8/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_P1 [get_bd_pins OBUF_DS_P] [get_bd_pins util_ds_buf_8/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_N1 [get_bd_pins OBUF_DS_N1] [get_bd_pins util_ds_buf_9/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_P1 [get_bd_pins OBUF_DS_P1] [get_bd_pins util_ds_buf_9/OBUF_DS_P]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ZynqPSandAXIStuff
proc create_hier_cell_ZynqPSandAXIStuff { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ZynqPSandAXIStuff() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR
  create_bd_intf_pin -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M01_AXI
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M02_AXI
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M03_AXI
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M04_AXI

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 -type rst ARESETN
  create_bd_pin -dir O -type clk FCLK_CLK0
  create_bd_pin -dir O -from 0 -to 0 -type rst M03_ARESETN

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
   CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {800.000000} \
   CONFIG.PCW_ACT_CAN0_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN1_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_DCI_PERIPHERAL_FREQMHZ {10.158730} \
   CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {25.000000} \
   CONFIG.PCW_ACT_ENET1_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_I2C_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_PCAP_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_SMC_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_TPIU_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_ACT_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {133.333344} \
   CONFIG.PCW_APU_CLK_RATIO_ENABLE {6:2:1} \
   CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {800} \
   CONFIG.PCW_ARMPLL_CTRL_FBDIV {48} \
   CONFIG.PCW_CAN0_BASEADDR {0xE0008000} \
   CONFIG.PCW_CAN0_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN0_HIGHADDR {0xE0008FFF} \
   CONFIG.PCW_CAN0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN0_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN1_BASEADDR {0xE0009000} \
   CONFIG.PCW_CAN1_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN1_HIGHADDR {0xE0009FFF} \
   CONFIG.PCW_CAN1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN1_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_CAN_PERIPHERAL_VALID {0} \
   CONFIG.PCW_CLK0_FREQ {200000000} \
   CONFIG.PCW_CLK1_FREQ {10000000} \
   CONFIG.PCW_CLK2_FREQ {10000000} \
   CONFIG.PCW_CLK3_FREQ {10000000} \
   CONFIG.PCW_CORE0_FIQ_INTR {0} \
   CONFIG.PCW_CORE0_IRQ_INTR {0} \
   CONFIG.PCW_CORE1_FIQ_INTR {0} \
   CONFIG.PCW_CORE1_IRQ_INTR {0} \
   CONFIG.PCW_CPU_CPU_6X4X_MAX_RANGE {800} \
   CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {1600.000} \
   CONFIG.PCW_CPU_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_CPU_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
   CONFIG.PCW_DCI_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR0 {15} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR1 {7} \
   CONFIG.PCW_DCI_PERIPHERAL_FREQMHZ {10.159} \
   CONFIG.PCW_DDRPLL_CTRL_FBDIV {32} \
   CONFIG.PCW_DDR_DDR_PLL_FREQMHZ {1066.667} \
   CONFIG.PCW_DDR_HPRLPR_QUEUE_PARTITION {HPR(0)/LPR(32)} \
   CONFIG.PCW_DDR_HPR_TO_CRITICAL_PRIORITY_LEVEL {15} \
   CONFIG.PCW_DDR_LPR_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DDR_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DDR_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_DDR_PORT0_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT1_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT2_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT3_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_RAM_BASEADDR {0x00100000} \
   CONFIG.PCW_DDR_RAM_HIGHADDR {0x3FFFFFFF} \
   CONFIG.PCW_DDR_WRITE_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DM_WIDTH {4} \
   CONFIG.PCW_DQS_WIDTH {4} \
   CONFIG.PCW_DQ_WIDTH {32} \
   CONFIG.PCW_DUAL_PARALLEL_QSPI_DATA_MODE {x8} \
   CONFIG.PCW_ENET0_BASEADDR {0xE000B000} \
   CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} \
   CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
   CONFIG.PCW_ENET0_GRP_MDIO_IO {MIO 52 .. 53} \
   CONFIG.PCW_ENET0_HIGHADDR {0xE000BFFF} \
   CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {8} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {5} \
   CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {100 Mbps} \
   CONFIG.PCW_ENET0_RESET_ENABLE {1} \
   CONFIG.PCW_ENET0_RESET_IO {MIO 47} \
   CONFIG.PCW_ENET1_BASEADDR {0xE000C000} \
   CONFIG.PCW_ENET1_GRP_MDIO_ENABLE {0} \
   CONFIG.PCW_ENET1_HIGHADDR {0xE000CFFF} \
   CONFIG.PCW_ENET1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_ENET1_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET1_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_ENABLE {1} \
   CONFIG.PCW_ENET_RESET_POLARITY {Active Low} \
   CONFIG.PCW_ENET_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_EN_4K_TIMER {0} \
   CONFIG.PCW_EN_CAN0 {0} \
   CONFIG.PCW_EN_CAN1 {0} \
   CONFIG.PCW_EN_CLK0_PORT {1} \
   CONFIG.PCW_EN_CLK1_PORT {0} \
   CONFIG.PCW_EN_CLK2_PORT {0} \
   CONFIG.PCW_EN_CLK3_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG0_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG1_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG2_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG3_PORT {0} \
   CONFIG.PCW_EN_DDR {1} \
   CONFIG.PCW_EN_EMIO_CAN0 {0} \
   CONFIG.PCW_EN_EMIO_CAN1 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_ENET0 {0} \
   CONFIG.PCW_EN_EMIO_ENET1 {0} \
   CONFIG.PCW_EN_EMIO_GPIO {0} \
   CONFIG.PCW_EN_EMIO_I2C0 {0} \
   CONFIG.PCW_EN_EMIO_I2C1 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART0 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART1 {0} \
   CONFIG.PCW_EN_EMIO_PJTAG {0} \
   CONFIG.PCW_EN_EMIO_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_SPI0 {0} \
   CONFIG.PCW_EN_EMIO_SPI1 {0} \
   CONFIG.PCW_EN_EMIO_SRAM_INT {0} \
   CONFIG.PCW_EN_EMIO_TRACE {0} \
   CONFIG.PCW_EN_EMIO_TTC0 {1} \
   CONFIG.PCW_EN_EMIO_TTC1 {0} \
   CONFIG.PCW_EN_EMIO_UART0 {0} \
   CONFIG.PCW_EN_EMIO_UART1 {0} \
   CONFIG.PCW_EN_EMIO_WDT {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO1 {0} \
   CONFIG.PCW_EN_ENET0 {1} \
   CONFIG.PCW_EN_ENET1 {0} \
   CONFIG.PCW_EN_GPIO {1} \
   CONFIG.PCW_EN_I2C0 {1} \
   CONFIG.PCW_EN_I2C1 {0} \
   CONFIG.PCW_EN_MODEM_UART0 {0} \
   CONFIG.PCW_EN_MODEM_UART1 {0} \
   CONFIG.PCW_EN_PJTAG {0} \
   CONFIG.PCW_EN_PTP_ENET0 {0} \
   CONFIG.PCW_EN_PTP_ENET1 {0} \
   CONFIG.PCW_EN_QSPI {1} \
   CONFIG.PCW_EN_RST0_PORT {1} \
   CONFIG.PCW_EN_RST1_PORT {0} \
   CONFIG.PCW_EN_RST2_PORT {0} \
   CONFIG.PCW_EN_RST3_PORT {0} \
   CONFIG.PCW_EN_SDIO0 {1} \
   CONFIG.PCW_EN_SDIO1 {0} \
   CONFIG.PCW_EN_SMC {0} \
   CONFIG.PCW_EN_SPI0 {0} \
   CONFIG.PCW_EN_SPI1 {0} \
   CONFIG.PCW_EN_TRACE {0} \
   CONFIG.PCW_EN_TTC0 {1} \
   CONFIG.PCW_EN_TTC1 {0} \
   CONFIG.PCW_EN_UART0 {0} \
   CONFIG.PCW_EN_UART1 {1} \
   CONFIG.PCW_EN_USB0 {1} \
   CONFIG.PCW_EN_USB1 {0} \
   CONFIG.PCW_EN_WDT {0} \
   CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK2_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK_CLK0_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK1_BUF {FALSE} \
   CONFIG.PCW_FCLK_CLK2_BUF {FALSE} \
   CONFIG.PCW_FCLK_CLK3_BUF {FALSE} \
   CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {30} \
   CONFIG.PCW_FPGA2_PERIPHERAL_FREQMHZ {10} \
   CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA_FCLK0_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK1_ENABLE {0} \
   CONFIG.PCW_FPGA_FCLK2_ENABLE {0} \
   CONFIG.PCW_FPGA_FCLK3_ENABLE {0} \
   CONFIG.PCW_FTM_CTI_IN0 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN1 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN2 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN3 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT0 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT1 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT2 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT3 {<Select>} \
   CONFIG.PCW_GP0_EN_MODIFIABLE_TXN {0} \
   CONFIG.PCW_GP0_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP0_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GP1_EN_MODIFIABLE_TXN {0} \
   CONFIG.PCW_GP1_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP1_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GPIO_BASEADDR {0xE000A000} \
   CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {0} \
   CONFIG.PCW_GPIO_EMIO_GPIO_WIDTH {64} \
   CONFIG.PCW_GPIO_HIGHADDR {0xE000AFFF} \
   CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
   CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
   CONFIG.PCW_GPIO_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C0_BASEADDR {0xE0004000} \
   CONFIG.PCW_I2C0_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C0_HIGHADDR {0xE0004FFF} \
   CONFIG.PCW_I2C0_I2C0_IO {MIO 50 .. 51} \
   CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C0_RESET_ENABLE {1} \
   CONFIG.PCW_I2C0_RESET_IO {MIO 46} \
   CONFIG.PCW_I2C1_BASEADDR {0xE0005000} \
   CONFIG.PCW_I2C1_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C1_HIGHADDR {0xE0005FFF} \
   CONFIG.PCW_I2C1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C1_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {133.333328} \
   CONFIG.PCW_I2C_RESET_ENABLE {1} \
   CONFIG.PCW_I2C_RESET_POLARITY {Active Low} \
   CONFIG.PCW_I2C_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_IMPORT_BOARD_PRESET {None} \
   CONFIG.PCW_INCLUDE_ACP_TRANS_CHECK {0} \
   CONFIG.PCW_INCLUDE_TRACE_BUFFER {0} \
   CONFIG.PCW_IOPLL_CTRL_FBDIV {30} \
   CONFIG.PCW_IO_IO_PLL_FREQMHZ {1000.000} \
   CONFIG.PCW_IRQ_F2P_INTR {1} \
   CONFIG.PCW_IRQ_F2P_MODE {DIRECT} \
   CONFIG.PCW_MIO_0_DIRECTION {out} \
   CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_0_PULLUP {enabled} \
   CONFIG.PCW_MIO_0_SLEW {slow} \
   CONFIG.PCW_MIO_10_DIRECTION {inout} \
   CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_10_PULLUP {enabled} \
   CONFIG.PCW_MIO_10_SLEW {slow} \
   CONFIG.PCW_MIO_11_DIRECTION {inout} \
   CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_11_PULLUP {enabled} \
   CONFIG.PCW_MIO_11_SLEW {slow} \
   CONFIG.PCW_MIO_12_DIRECTION {inout} \
   CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_12_PULLUP {enabled} \
   CONFIG.PCW_MIO_12_SLEW {slow} \
   CONFIG.PCW_MIO_13_DIRECTION {inout} \
   CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_13_PULLUP {enabled} \
   CONFIG.PCW_MIO_13_SLEW {slow} \
   CONFIG.PCW_MIO_14_DIRECTION {in} \
   CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_14_PULLUP {enabled} \
   CONFIG.PCW_MIO_14_SLEW {slow} \
   CONFIG.PCW_MIO_15_DIRECTION {in} \
   CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_15_PULLUP {enabled} \
   CONFIG.PCW_MIO_15_SLEW {slow} \
   CONFIG.PCW_MIO_16_DIRECTION {out} \
   CONFIG.PCW_MIO_16_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_16_PULLUP {disabled} \
   CONFIG.PCW_MIO_16_SLEW {slow} \
   CONFIG.PCW_MIO_17_DIRECTION {out} \
   CONFIG.PCW_MIO_17_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_17_PULLUP {disabled} \
   CONFIG.PCW_MIO_17_SLEW {slow} \
   CONFIG.PCW_MIO_18_DIRECTION {out} \
   CONFIG.PCW_MIO_18_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_18_PULLUP {disabled} \
   CONFIG.PCW_MIO_18_SLEW {slow} \
   CONFIG.PCW_MIO_19_DIRECTION {out} \
   CONFIG.PCW_MIO_19_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_19_PULLUP {disabled} \
   CONFIG.PCW_MIO_19_SLEW {slow} \
   CONFIG.PCW_MIO_1_DIRECTION {out} \
   CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_1_PULLUP {enabled} \
   CONFIG.PCW_MIO_1_SLEW {slow} \
   CONFIG.PCW_MIO_20_DIRECTION {out} \
   CONFIG.PCW_MIO_20_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_20_PULLUP {disabled} \
   CONFIG.PCW_MIO_20_SLEW {slow} \
   CONFIG.PCW_MIO_21_DIRECTION {out} \
   CONFIG.PCW_MIO_21_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_21_PULLUP {disabled} \
   CONFIG.PCW_MIO_21_SLEW {slow} \
   CONFIG.PCW_MIO_22_DIRECTION {in} \
   CONFIG.PCW_MIO_22_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_22_PULLUP {disabled} \
   CONFIG.PCW_MIO_22_SLEW {slow} \
   CONFIG.PCW_MIO_23_DIRECTION {in} \
   CONFIG.PCW_MIO_23_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_23_PULLUP {disabled} \
   CONFIG.PCW_MIO_23_SLEW {slow} \
   CONFIG.PCW_MIO_24_DIRECTION {in} \
   CONFIG.PCW_MIO_24_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_24_PULLUP {disabled} \
   CONFIG.PCW_MIO_24_SLEW {slow} \
   CONFIG.PCW_MIO_25_DIRECTION {in} \
   CONFIG.PCW_MIO_25_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_25_PULLUP {disabled} \
   CONFIG.PCW_MIO_25_SLEW {slow} \
   CONFIG.PCW_MIO_26_DIRECTION {in} \
   CONFIG.PCW_MIO_26_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_26_PULLUP {disabled} \
   CONFIG.PCW_MIO_26_SLEW {slow} \
   CONFIG.PCW_MIO_27_DIRECTION {in} \
   CONFIG.PCW_MIO_27_IOTYPE {HSTL 1.8V} \
   CONFIG.PCW_MIO_27_PULLUP {disabled} \
   CONFIG.PCW_MIO_27_SLEW {slow} \
   CONFIG.PCW_MIO_28_DIRECTION {inout} \
   CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_28_PULLUP {disabled} \
   CONFIG.PCW_MIO_28_SLEW {slow} \
   CONFIG.PCW_MIO_29_DIRECTION {in} \
   CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_29_PULLUP {disabled} \
   CONFIG.PCW_MIO_29_SLEW {slow} \
   CONFIG.PCW_MIO_2_DIRECTION {inout} \
   CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_2_PULLUP {disabled} \
   CONFIG.PCW_MIO_2_SLEW {slow} \
   CONFIG.PCW_MIO_30_DIRECTION {out} \
   CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_30_PULLUP {disabled} \
   CONFIG.PCW_MIO_30_SLEW {slow} \
   CONFIG.PCW_MIO_31_DIRECTION {in} \
   CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_31_PULLUP {disabled} \
   CONFIG.PCW_MIO_31_SLEW {slow} \
   CONFIG.PCW_MIO_32_DIRECTION {inout} \
   CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_32_PULLUP {disabled} \
   CONFIG.PCW_MIO_32_SLEW {slow} \
   CONFIG.PCW_MIO_33_DIRECTION {inout} \
   CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_33_PULLUP {disabled} \
   CONFIG.PCW_MIO_33_SLEW {slow} \
   CONFIG.PCW_MIO_34_DIRECTION {inout} \
   CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_34_PULLUP {disabled} \
   CONFIG.PCW_MIO_34_SLEW {slow} \
   CONFIG.PCW_MIO_35_DIRECTION {inout} \
   CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_35_PULLUP {disabled} \
   CONFIG.PCW_MIO_35_SLEW {slow} \
   CONFIG.PCW_MIO_36_DIRECTION {in} \
   CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_36_PULLUP {disabled} \
   CONFIG.PCW_MIO_36_SLEW {slow} \
   CONFIG.PCW_MIO_37_DIRECTION {inout} \
   CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_37_PULLUP {disabled} \
   CONFIG.PCW_MIO_37_SLEW {slow} \
   CONFIG.PCW_MIO_38_DIRECTION {inout} \
   CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_38_PULLUP {disabled} \
   CONFIG.PCW_MIO_38_SLEW {slow} \
   CONFIG.PCW_MIO_39_DIRECTION {inout} \
   CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_39_PULLUP {disabled} \
   CONFIG.PCW_MIO_39_SLEW {slow} \
   CONFIG.PCW_MIO_3_DIRECTION {inout} \
   CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_3_PULLUP {disabled} \
   CONFIG.PCW_MIO_3_SLEW {slow} \
   CONFIG.PCW_MIO_40_DIRECTION {inout} \
   CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_40_PULLUP {disabled} \
   CONFIG.PCW_MIO_40_SLEW {slow} \
   CONFIG.PCW_MIO_41_DIRECTION {inout} \
   CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_41_PULLUP {disabled} \
   CONFIG.PCW_MIO_41_SLEW {slow} \
   CONFIG.PCW_MIO_42_DIRECTION {inout} \
   CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_42_PULLUP {disabled} \
   CONFIG.PCW_MIO_42_SLEW {slow} \
   CONFIG.PCW_MIO_43_DIRECTION {inout} \
   CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_43_PULLUP {disabled} \
   CONFIG.PCW_MIO_43_SLEW {slow} \
   CONFIG.PCW_MIO_44_DIRECTION {inout} \
   CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_44_PULLUP {disabled} \
   CONFIG.PCW_MIO_44_SLEW {slow} \
   CONFIG.PCW_MIO_45_DIRECTION {inout} \
   CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_45_PULLUP {disabled} \
   CONFIG.PCW_MIO_45_SLEW {slow} \
   CONFIG.PCW_MIO_46_DIRECTION {out} \
   CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_46_PULLUP {enabled} \
   CONFIG.PCW_MIO_46_SLEW {slow} \
   CONFIG.PCW_MIO_47_DIRECTION {out} \
   CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_47_PULLUP {enabled} \
   CONFIG.PCW_MIO_47_SLEW {slow} \
   CONFIG.PCW_MIO_48_DIRECTION {out} \
   CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_48_PULLUP {disabled} \
   CONFIG.PCW_MIO_48_SLEW {slow} \
   CONFIG.PCW_MIO_49_DIRECTION {in} \
   CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_49_PULLUP {disabled} \
   CONFIG.PCW_MIO_49_SLEW {slow} \
   CONFIG.PCW_MIO_4_DIRECTION {inout} \
   CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_4_PULLUP {disabled} \
   CONFIG.PCW_MIO_4_SLEW {slow} \
   CONFIG.PCW_MIO_50_DIRECTION {inout} \
   CONFIG.PCW_MIO_50_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_50_PULLUP {enabled} \
   CONFIG.PCW_MIO_50_SLEW {slow} \
   CONFIG.PCW_MIO_51_DIRECTION {inout} \
   CONFIG.PCW_MIO_51_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_51_PULLUP {enabled} \
   CONFIG.PCW_MIO_51_SLEW {slow} \
   CONFIG.PCW_MIO_52_DIRECTION {out} \
   CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_52_PULLUP {disabled} \
   CONFIG.PCW_MIO_52_SLEW {slow} \
   CONFIG.PCW_MIO_53_DIRECTION {inout} \
   CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_53_PULLUP {disabled} \
   CONFIG.PCW_MIO_53_SLEW {slow} \
   CONFIG.PCW_MIO_5_DIRECTION {inout} \
   CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_5_PULLUP {disabled} \
   CONFIG.PCW_MIO_5_SLEW {slow} \
   CONFIG.PCW_MIO_6_DIRECTION {out} \
   CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_6_PULLUP {disabled} \
   CONFIG.PCW_MIO_6_SLEW {slow} \
   CONFIG.PCW_MIO_7_DIRECTION {out} \
   CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_7_PULLUP {disabled} \
   CONFIG.PCW_MIO_7_SLEW {slow} \
   CONFIG.PCW_MIO_8_DIRECTION {out} \
   CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_8_PULLUP {disabled} \
   CONFIG.PCW_MIO_8_SLEW {slow} \
   CONFIG.PCW_MIO_9_DIRECTION {out} \
   CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_9_PULLUP {enabled} \
   CONFIG.PCW_MIO_9_SLEW {slow} \
   CONFIG.PCW_MIO_PRIMITIVE {54} \
   CONFIG.PCW_MIO_TREE_PERIPHERALS {Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#USB Reset#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#SD 0#SD 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#I2C Reset#ENET Reset#UART 1#UART 1#I2C 0#I2C 0#Enet 0#Enet 0} \
   CONFIG.PCW_MIO_TREE_SIGNALS {qspi1_ss_b#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]/HOLD_B#qspi0_sclk#reset#qspi_fbclk#qspi1_sclk#qspi1_io[0]#qspi1_io[1]#qspi1_io[2]#qspi1_io[3]#cd#wp#tx_clk#txd[0]#txd[1]#txd[2]#txd[3]#tx_ctl#rx_clk#rxd[0]#rxd[1]#rxd[2]#rxd[3]#rx_ctl#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#clk#cmd#data[0]#data[1]#data[2]#data[3]#reset#reset#tx#rx#scl#sda#mdc#mdio} \
   CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP0_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP0_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP0_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP1_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP1_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_NAND_CYCLES_T_AR {1} \
   CONFIG.PCW_NAND_CYCLES_T_CLR {1} \
   CONFIG.PCW_NAND_CYCLES_T_RC {11} \
   CONFIG.PCW_NAND_CYCLES_T_REA {1} \
   CONFIG.PCW_NAND_CYCLES_T_RR {1} \
   CONFIG.PCW_NAND_CYCLES_T_WC {11} \
   CONFIG.PCW_NAND_CYCLES_T_WP {1} \
   CONFIG.PCW_NAND_GRP_D8_ENABLE {0} \
   CONFIG.PCW_NAND_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_CS0_T_PC {1} \
   CONFIG.PCW_NOR_CS0_T_RC {11} \
   CONFIG.PCW_NOR_CS0_T_TR {1} \
   CONFIG.PCW_NOR_CS0_T_WC {11} \
   CONFIG.PCW_NOR_CS0_T_WP {1} \
   CONFIG.PCW_NOR_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_CS1_T_PC {1} \
   CONFIG.PCW_NOR_CS1_T_RC {11} \
   CONFIG.PCW_NOR_CS1_T_TR {1} \
   CONFIG.PCW_NOR_CS1_T_WC {11} \
   CONFIG.PCW_NOR_CS1_T_WP {1} \
   CONFIG.PCW_NOR_CS1_WE_TIME {0} \
   CONFIG.PCW_NOR_GRP_A25_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_INT_ENABLE {0} \
   CONFIG.PCW_NOR_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_SRAM_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_RC {11} \
   CONFIG.PCW_NOR_SRAM_CS0_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WC {11} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_SRAM_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_RC {11} \
   CONFIG.PCW_NOR_SRAM_CS1_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WC {11} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS1_WE_TIME {0} \
   CONFIG.PCW_OVERRIDE_BASIC_CLOCK {0} \
   CONFIG.PCW_P2F_CAN0_INTR {0} \
   CONFIG.PCW_P2F_CAN1_INTR {0} \
   CONFIG.PCW_P2F_CTI_INTR {0} \
   CONFIG.PCW_P2F_DMAC0_INTR {0} \
   CONFIG.PCW_P2F_DMAC1_INTR {0} \
   CONFIG.PCW_P2F_DMAC2_INTR {0} \
   CONFIG.PCW_P2F_DMAC3_INTR {0} \
   CONFIG.PCW_P2F_DMAC4_INTR {0} \
   CONFIG.PCW_P2F_DMAC5_INTR {0} \
   CONFIG.PCW_P2F_DMAC6_INTR {0} \
   CONFIG.PCW_P2F_DMAC7_INTR {0} \
   CONFIG.PCW_P2F_DMAC_ABORT_INTR {0} \
   CONFIG.PCW_P2F_ENET0_INTR {0} \
   CONFIG.PCW_P2F_ENET1_INTR {0} \
   CONFIG.PCW_P2F_GPIO_INTR {0} \
   CONFIG.PCW_P2F_I2C0_INTR {0} \
   CONFIG.PCW_P2F_I2C1_INTR {0} \
   CONFIG.PCW_P2F_QSPI_INTR {0} \
   CONFIG.PCW_P2F_SDIO0_INTR {0} \
   CONFIG.PCW_P2F_SDIO1_INTR {0} \
   CONFIG.PCW_P2F_SMC_INTR {0} \
   CONFIG.PCW_P2F_SPI0_INTR {0} \
   CONFIG.PCW_P2F_SPI1_INTR {0} \
   CONFIG.PCW_P2F_UART0_INTR {0} \
   CONFIG.PCW_P2F_UART1_INTR {0} \
   CONFIG.PCW_P2F_USB0_INTR {0} \
   CONFIG.PCW_P2F_USB1_INTR {0} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY0 {0.100} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY1 {0.113} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY2 {0.111} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY3 {0.100} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_0 {-0.017} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_1 {-0.039} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_2 {-0.040} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_3 {-0.016} \
   CONFIG.PCW_PACKAGE_NAME {ffg900} \
   CONFIG.PCW_PCAP_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_PCAP_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_PERIPHERAL_BOARD_PRESET {part0} \
   CONFIG.PCW_PJTAG_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_PLL_BYPASSMODE_ENABLE {0} \
   CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 1.8V} \
   CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
   CONFIG.PCW_PS7_SI_REV {PRODUCTION} \
   CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_FBCLK_IO {MIO 8} \
   CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_IO1_IO {MIO 0 9 .. 13} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_QSPI_INTERNAL_HIGHADDRESS {0xFDFFFFFF} \
   CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
   CONFIG.PCW_SD0_GRP_CD_ENABLE {1} \
   CONFIG.PCW_SD0_GRP_CD_IO {MIO 14} \
   CONFIG.PCW_SD0_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_WP_ENABLE {1} \
   CONFIG.PCW_SD0_GRP_WP_IO {MIO 15} \
   CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
   CONFIG.PCW_SD1_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SDIO0_BASEADDR {0xE0100000} \
   CONFIG.PCW_SDIO0_HIGHADDR {0xE0100FFF} \
   CONFIG.PCW_SDIO1_BASEADDR {0xE0101000} \
   CONFIG.PCW_SDIO1_HIGHADDR {0xE0101FFF} \
   CONFIG.PCW_SDIO_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {20} \
   CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
   CONFIG.PCW_SMC_CYCLE_T0 {NA} \
   CONFIG.PCW_SMC_CYCLE_T1 {NA} \
   CONFIG.PCW_SMC_CYCLE_T2 {NA} \
   CONFIG.PCW_SMC_CYCLE_T3 {NA} \
   CONFIG.PCW_SMC_CYCLE_T4 {NA} \
   CONFIG.PCW_SMC_CYCLE_T5 {NA} \
   CONFIG.PCW_SMC_CYCLE_T6 {NA} \
   CONFIG.PCW_SMC_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SMC_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SMC_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SMC_PERIPHERAL_VALID {0} \
   CONFIG.PCW_SPI0_BASEADDR {0xE0006000} \
   CONFIG.PCW_SPI0_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI0_HIGHADDR {0xE0006FFF} \
   CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI1_BASEADDR {0xE0007000} \
   CONFIG.PCW_SPI1_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI1_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI1_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI1_HIGHADDR {0xE0007FFF} \
   CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SPI_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SPI_PERIPHERAL_FREQMHZ {166.666666} \
   CONFIG.PCW_SPI_PERIPHERAL_VALID {0} \
   CONFIG.PCW_S_AXI_ACP_ARUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_AWUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_ID_WIDTH {3} \
   CONFIG.PCW_S_AXI_GP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_GP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP1_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP2_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP2_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP3_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP3_ID_WIDTH {6} \
   CONFIG.PCW_TPIU_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_TPIU_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TPIU_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_TRACE_BUFFER_CLOCK_DELAY {12} \
   CONFIG.PCW_TRACE_BUFFER_FIFO_SIZE {128} \
   CONFIG.PCW_TRACE_GRP_16BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_2BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_32BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_4BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_8BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_INTERNAL_WIDTH {2} \
   CONFIG.PCW_TRACE_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TRACE_PIPELINE_WIDTH {8} \
   CONFIG.PCW_TTC0_BASEADDR {0xE0104000} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_HIGHADDR {0xE0104fff} \
   CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
   CONFIG.PCW_TTC1_BASEADDR {0xE0105000} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_HIGHADDR {0xE0105fff} \
   CONFIG.PCW_TTC1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART0_BASEADDR {0xE0000000} \
   CONFIG.PCW_UART0_BAUD_RATE {115200} \
   CONFIG.PCW_UART0_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART0_HIGHADDR {0xE0000FFF} \
   CONFIG.PCW_UART0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_UART1_BASEADDR {0xE0001000} \
   CONFIG.PCW_UART1_BAUD_RATE {115200} \
   CONFIG.PCW_UART1_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART1_HIGHADDR {0xE0001FFF} \
   CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_UART1_UART1_IO {MIO 48 .. 49} \
   CONFIG.PCW_UART_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {20} \
   CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
   CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
   CONFIG.PCW_UIPARAM_DDR_ADV_ENABLE {0} \
   CONFIG.PCW_UIPARAM_DDR_AL {0} \
   CONFIG.PCW_UIPARAM_DDR_BANK_ADDR_COUNT {3} \
   CONFIG.PCW_UIPARAM_DDR_BL {8} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.521} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.636} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.54} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.621} \
   CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {32 Bit} \
   CONFIG.PCW_UIPARAM_DDR_CL {7} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PACKAGE_LENGTH {92.3275} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PACKAGE_LENGTH {92.3275} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PACKAGE_LENGTH {92.3275} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PACKAGE_LENGTH {92.3275} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_STOP_EN {0} \
   CONFIG.PCW_UIPARAM_DDR_COL_ADDR_COUNT {10} \
   CONFIG.PCW_UIPARAM_DDR_CWL {6} \
   CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {2048 MBits} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PACKAGE_LENGTH {108.9255} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PACKAGE_LENGTH {131.286} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PACKAGE_LENGTH {131.83} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PACKAGE_LENGTH {108.5285} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {0.226} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {0.278} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {0.184} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {0.309} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PACKAGE_LENGTH {107.643} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PACKAGE_LENGTH {132.917} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PACKAGE_LENGTH {129.6135} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PACKAGE_LENGTH {108.6395} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {8 Bits} \
   CONFIG.PCW_UIPARAM_DDR_ECC {Disabled} \
   CONFIG.PCW_UIPARAM_DDR_ENABLE {1} \
   CONFIG.PCW_UIPARAM_DDR_FREQ_MHZ {533.333313} \
   CONFIG.PCW_UIPARAM_DDR_HIGH_TEMP {Normal (0-85)} \
   CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3} \
   CONFIG.PCW_UIPARAM_DDR_PARTNO {Custom} \
   CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {15} \
   CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_DATA_EYE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_READ_GATE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_WRITE_LEVEL {1} \
   CONFIG.PCW_UIPARAM_DDR_T_FAW {30.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {36.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RC {49.5} \
   CONFIG.PCW_UIPARAM_DDR_T_RCD {7} \
   CONFIG.PCW_UIPARAM_DDR_T_RP {7} \
   CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {1} \
   CONFIG.PCW_UIPARAM_GENERATE_SUMMARY {NONE} \
   CONFIG.PCW_USB0_BASEADDR {0xE0102000} \
   CONFIG.PCW_USB0_HIGHADDR {0xE0102fff} \
   CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB0_RESET_ENABLE {1} \
   CONFIG.PCW_USB0_RESET_IO {MIO 7} \
   CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
   CONFIG.PCW_USB1_BASEADDR {0xE0103000} \
   CONFIG.PCW_USB1_HIGHADDR {0xE0103fff} \
   CONFIG.PCW_USB1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB1_RESET_ENABLE {0} \
   CONFIG.PCW_USB_RESET_ENABLE {1} \
   CONFIG.PCW_USB_RESET_POLARITY {Active Low} \
   CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_USE_AXI_FABRIC_IDLE {0} \
   CONFIG.PCW_USE_AXI_NONSECURE {0} \
   CONFIG.PCW_USE_CORESIGHT {0} \
   CONFIG.PCW_USE_CROSS_TRIGGER {0} \
   CONFIG.PCW_USE_CR_FABRIC {1} \
   CONFIG.PCW_USE_DDR_BYPASS {0} \
   CONFIG.PCW_USE_DEBUG {0} \
   CONFIG.PCW_USE_DEFAULT_ACP_USER_VAL {0} \
   CONFIG.PCW_USE_DMA0 {0} \
   CONFIG.PCW_USE_DMA1 {0} \
   CONFIG.PCW_USE_DMA2 {0} \
   CONFIG.PCW_USE_DMA3 {0} \
   CONFIG.PCW_USE_EXPANDED_IOP {0} \
   CONFIG.PCW_USE_EXPANDED_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
   CONFIG.PCW_USE_HIGH_OCM {0} \
   CONFIG.PCW_USE_M_AXI_GP0 {1} \
   CONFIG.PCW_USE_M_AXI_GP1 {0} \
   CONFIG.PCW_USE_PROC_EVENT_BUS {0} \
   CONFIG.PCW_USE_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_S_AXI_ACP {0} \
   CONFIG.PCW_USE_S_AXI_GP0 {0} \
   CONFIG.PCW_USE_S_AXI_GP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP0 {0} \
   CONFIG.PCW_USE_S_AXI_HP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP2 {0} \
   CONFIG.PCW_USE_S_AXI_HP3 {0} \
   CONFIG.PCW_USE_TRACE {0} \
   CONFIG.PCW_USE_TRACE_DATA_EDGE_DETECTOR {0} \
   CONFIG.PCW_VALUE_SILVERSION {3} \
   CONFIG.PCW_WDT_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_WDT_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_WDT_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_WDT_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.preset {ZC706} \
 ] $processing_system7_0

  # Create instance: ps7_0_axi_periph, and set properties
  set ps7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 ps7_0_axi_periph ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {0} \
   CONFIG.NUM_MI {5} \
   CONFIG.S00_HAS_DATA_FIFO {2} \
   CONFIG.STRATEGY {2} \
   CONFIG.SYNCHRONIZATION_STAGES {2} \
 ] $ps7_0_axi_periph

  # Create instance: rst_ps7_0_100M, and set properties
  set rst_ps7_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_ps7_0_100M ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins M03_AXI] [get_bd_intf_pins ps7_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins M04_AXI] [get_bd_intf_pins ps7_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_pins DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_pins FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins ps7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M00_AXI [get_bd_intf_pins M00_AXI] [get_bd_intf_pins ps7_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M01_AXI [get_bd_intf_pins M01_AXI] [get_bd_intf_pins ps7_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M02_AXI [get_bd_intf_pins M02_AXI] [get_bd_intf_pins ps7_0_axi_periph/M02_AXI]

  # Create port connections
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins FCLK_CLK0] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins ps7_0_axi_periph/ACLK] [get_bd_pins ps7_0_axi_periph/M00_ACLK] [get_bd_pins ps7_0_axi_periph/M01_ACLK] [get_bd_pins ps7_0_axi_periph/M02_ACLK] [get_bd_pins ps7_0_axi_periph/M03_ACLK] [get_bd_pins ps7_0_axi_periph/M04_ACLK] [get_bd_pins ps7_0_axi_periph/S00_ACLK] [get_bd_pins rst_ps7_0_100M/slowest_sync_clk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_ps7_0_100M/ext_reset_in]
  connect_bd_net -net rst_ps7_0_100M_interconnect_aresetn [get_bd_pins ARESETN] [get_bd_pins ps7_0_axi_periph/ARESETN] [get_bd_pins ps7_0_axi_periph/M00_ARESETN] [get_bd_pins ps7_0_axi_periph/M01_ARESETN] [get_bd_pins ps7_0_axi_periph/M02_ARESETN] [get_bd_pins ps7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_ps7_0_100M/interconnect_aresetn]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins M03_ARESETN] [get_bd_pins ps7_0_axi_periph/M03_ARESETN] [get_bd_pins ps7_0_axi_periph/M04_ARESETN] [get_bd_pins rst_ps7_0_100M/peripheral_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: SyncReset_obuf
proc create_hier_cell_SyncReset_obuf { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_SyncReset_obuf() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I I
  create_bd_pin -dir O O_n
  create_bd_pin -dir O O_n1
  create_bd_pin -dir O O_n2
  create_bd_pin -dir O O_p
  create_bd_pin -dir O O_p1
  create_bd_pin -dir O O_p2

  # Create instance: xil_obufds_0, and set properties
  set block_name xil_obufds
  set block_cell_name xil_obufds_0
  if { [catch {set xil_obufds_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $xil_obufds_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.IOSTANDARD {LVDS_25} \
 ] $xil_obufds_0

  # Create instance: xil_obufds_1, and set properties
  set block_name xil_obufds
  set block_cell_name xil_obufds_1
  if { [catch {set xil_obufds_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $xil_obufds_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: xil_obufds_2, and set properties
  set block_name xil_obufds
  set block_cell_name xil_obufds_2
  if { [catch {set xil_obufds_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $xil_obufds_2 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create port connections
  connect_bd_net -net I_1 [get_bd_pins I] [get_bd_pins xil_obufds_0/I] [get_bd_pins xil_obufds_1/I] [get_bd_pins xil_obufds_2/I]
  connect_bd_net -net xil_obufds_0_O_n [get_bd_pins O_n2] [get_bd_pins xil_obufds_0/O_n]
  connect_bd_net -net xil_obufds_0_O_p [get_bd_pins O_p2] [get_bd_pins xil_obufds_0/O_p]
  connect_bd_net -net xil_obufds_1_O_n [get_bd_pins O_n] [get_bd_pins xil_obufds_1/O_n]
  connect_bd_net -net xil_obufds_1_O_p [get_bd_pins O_p] [get_bd_pins xil_obufds_1/O_p]
  connect_bd_net -net xil_obufds_2_O_n [get_bd_pins O_n1] [get_bd_pins xil_obufds_2/O_n]
  connect_bd_net -net xil_obufds_2_O_p [get_bd_pins O_p1] [get_bd_pins xil_obufds_2/O_p]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ATLASPix_Specific_slow_control
proc create_hier_cell_ATLASPix_Specific_slow_control { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ATLASPix_Specific_slow_control() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI1
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI6

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 Data_ib
  create_bd_pin -dir I -type clk EXT_CFG_CLK
  create_bd_pin -dir O EXT_CK1_M1ISO_N
  create_bd_pin -dir O EXT_CK1_M1ISO_P
  create_bd_pin -dir O EXT_CK1_M1_N
  create_bd_pin -dir O EXT_CK1_M1_P
  create_bd_pin -dir O EXT_CK1_M2_N
  create_bd_pin -dir O EXT_CK1_M2_P
  create_bd_pin -dir O EXT_CK2_M1ISO_N
  create_bd_pin -dir O EXT_CK2_M1ISO_P
  create_bd_pin -dir O EXT_CK2_M1_N
  create_bd_pin -dir O EXT_CK2_M1_P
  create_bd_pin -dir O EXT_CK2_M2_N
  create_bd_pin -dir O EXT_CK2_M2_P
  create_bd_pin -dir O EXT_LDM1ISO_N
  create_bd_pin -dir O EXT_LDM1ISO_P
  create_bd_pin -dir O EXT_LDM1_N
  create_bd_pin -dir O EXT_LDM1_P
  create_bd_pin -dir O EXT_LDM2_N
  create_bd_pin -dir O EXT_LDM2_P
  create_bd_pin -dir O EXT_PULSE_OUT_REF
  create_bd_pin -dir O EXT_SIN_M1ISO_N
  create_bd_pin -dir O EXT_SIN_M1ISO_P
  create_bd_pin -dir O EXT_SIN_M1_N
  create_bd_pin -dir O EXT_SIN_M1_P
  create_bd_pin -dir O EXT_SIN_M2_N
  create_bd_pin -dir O EXT_SIN_M2_P
  create_bd_pin -dir I MON1_IN
  create_bd_pin -dir I MON2_IN
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N12
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N13
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N14
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P12
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P13
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P14
  create_bd_pin -dir I -type clk pulser_clk
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn1
  create_bd_pin -dir I -from 0 -to 0 trigger_in

  # Create instance: ATLASPix_PulseInjectionCounter_0, and set properties
  set ATLASPix_PulseInjectionCounter_0 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_PulseInjectionCounter:1.0 ATLASPix_PulseInjectionCounter_0 ]

  # Create instance: ATLASPix_Pulser_0, and set properties
  set ATLASPix_Pulser_0 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_Pulser:1.0 ATLASPix_Pulser_0 ]

  # Create instance: ATLASPix_SR_FSM_0, and set properties
  set ATLASPix_SR_FSM_0 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_SR_FSM:1.0 ATLASPix_SR_FSM_0 ]

  # Create instance: SignalSyncer_0, and set properties
  set block_name SignalSyncer
  set block_cell_name SignalSyncer_0
  if { [catch {set SignalSyncer_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $SignalSyncer_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.g_Delay {2} \
 ] $SignalSyncer_0

  # Create instance: buffer_Pulser
  create_hier_cell_buffer_Pulser $hier_obj buffer_Pulser

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S00_AXI6] [get_bd_intf_pins ATLASPix_PulseInjectionCounter_0/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M01_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins ATLASPix_SR_FSM_0/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M02_AXI [get_bd_intf_pins S00_AXI1] [get_bd_intf_pins ATLASPix_Pulser_0/S00_AXI]

  # Create port connections
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_1 [get_bd_pins ATLASPix_Pulser_0/EXT_PULSE_OUT_1] [get_bd_pins buffer_Pulser/OBUF_IN4]
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_2 [get_bd_pins ATLASPix_Pulser_0/EXT_PULSE_OUT_2] [get_bd_pins buffer_Pulser/OBUF_IN2]
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_3 [get_bd_pins ATLASPix_Pulser_0/EXT_PULSE_OUT_3] [get_bd_pins buffer_Pulser/OBUF_IN3]
  connect_bd_net -net ATLASPix_Pulser_0_EXT_PULSE_OUT_REF [get_bd_pins EXT_PULSE_OUT_REF] [get_bd_pins ATLASPix_PulseInjectionCounter_0/INJ_IN] [get_bd_pins ATLASPix_Pulser_0/EXT_PULSE_OUT_REF]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK1_M1ISO_N [get_bd_pins EXT_CK1_M1ISO_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK1_M1ISO_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK1_M1ISO_P [get_bd_pins EXT_CK1_M1ISO_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK1_M1ISO_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK1_M1_N [get_bd_pins EXT_CK1_M1_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK1_M1_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK1_M1_P [get_bd_pins EXT_CK1_M1_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK1_M1_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK1_M2_N [get_bd_pins EXT_CK1_M2_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK1_M2_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK1_M2_P [get_bd_pins EXT_CK1_M2_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK1_M2_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK2_M1ISO_N [get_bd_pins EXT_CK2_M1ISO_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK2_M1ISO_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK2_M1ISO_P [get_bd_pins EXT_CK2_M1ISO_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK2_M1ISO_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK2_M1_N [get_bd_pins EXT_CK2_M1_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK2_M1_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK2_M1_P [get_bd_pins EXT_CK2_M1_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK2_M1_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK2_M2_N [get_bd_pins EXT_CK2_M2_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK2_M2_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_CK2_M2_P [get_bd_pins EXT_CK2_M2_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CK2_M2_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_LDM1ISO_N [get_bd_pins EXT_LDM1ISO_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_LDM1ISO_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_LDM1ISO_P [get_bd_pins EXT_LDM1ISO_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_LDM1ISO_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_LDM1_N [get_bd_pins EXT_LDM1_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_LDM1_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_LDM1_P [get_bd_pins EXT_LDM1_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_LDM1_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_LDM2_N [get_bd_pins EXT_LDM2_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_LDM2_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_LDM2_P [get_bd_pins EXT_LDM2_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_LDM2_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_SIN_M1ISO_N [get_bd_pins EXT_SIN_M1ISO_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_SIN_M1ISO_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_SIN_M1ISO_P [get_bd_pins EXT_SIN_M1ISO_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_SIN_M1ISO_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_SIN_M1_N [get_bd_pins EXT_SIN_M1_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_SIN_M1_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_SIN_M1_P [get_bd_pins EXT_SIN_M1_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_SIN_M1_P]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_SIN_M2_N [get_bd_pins EXT_SIN_M2_N] [get_bd_pins ATLASPix_SR_FSM_0/EXT_SIN_M2_N]
  connect_bd_net -net ATLASPix_SR_FSM_0_EXT_SIN_M2_P [get_bd_pins EXT_SIN_M2_P] [get_bd_pins ATLASPix_SR_FSM_0/EXT_SIN_M2_P]
  connect_bd_net -net EXT_CFG_CLK_1 [get_bd_pins EXT_CFG_CLK] [get_bd_pins ATLASPix_SR_FSM_0/EXT_CFG_CLK]
  connect_bd_net -net MON1_IN_1 [get_bd_pins MON1_IN] [get_bd_pins ATLASPix_PulseInjectionCounter_0/MON1_IN]
  connect_bd_net -net MON2_IN_1 [get_bd_pins MON2_IN] [get_bd_pins ATLASPix_PulseInjectionCounter_0/MON2_IN]
  connect_bd_net -net SignalSyncer_0_Data_ob [get_bd_pins ATLASPix_Pulser_0/trg_injection] [get_bd_pins SignalSyncer_0/Data_ob]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins s00_axi_aclk] [get_bd_pins ATLASPix_PulseInjectionCounter_0/s00_axi_aclk] [get_bd_pins ATLASPix_Pulser_0/s00_axi_aclk] [get_bd_pins ATLASPix_SR_FSM_0/s00_axi_aclk] [get_bd_pins SignalSyncer_0/Clk_ik]
  connect_bd_net -net pulser_clk_1 [get_bd_pins pulser_clk] [get_bd_pins ATLASPix_Pulser_0/pulser_clk]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins s00_axi_aresetn1] [get_bd_pins ATLASPix_PulseInjectionCounter_0/s00_axi_aresetn] [get_bd_pins ATLASPix_Pulser_0/s00_axi_aresetn] [get_bd_pins ATLASPix_SR_FSM_0/s00_axi_aresetn]
  connect_bd_net -net trigger_in_1 [get_bd_pins trigger_in] [get_bd_pins SignalSyncer_0/Data_ib]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_N [get_bd_pins OBUF_DS_N14] [get_bd_pins buffer_Pulser/OBUF_DS_N4]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_P [get_bd_pins OBUF_DS_P14] [get_bd_pins buffer_Pulser/OBUF_DS_P4]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_N [get_bd_pins OBUF_DS_N12] [get_bd_pins buffer_Pulser/OBUF_DS_N2]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_P [get_bd_pins OBUF_DS_P12] [get_bd_pins buffer_Pulser/OBUF_DS_P2]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_N [get_bd_pins OBUF_DS_N13] [get_bd_pins buffer_Pulser/OBUF_DS_N3]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_P [get_bd_pins OBUF_DS_P13] [get_bd_pins buffer_Pulser/OBUF_DS_P3]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins ATLASPix_PulseInjectionCounter_0/MON3_IN] [get_bd_pins xlconstant_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ATLASPIX_ReadoutBlock
proc create_hier_cell_ATLASPIX_ReadoutBlock { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ATLASPIX_ReadoutBlock() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  # Create pins
  create_bd_pin -dir I -type clk CLK
  create_bd_pin -dir I arst_in
  create_bd_pin -dir O busy
  create_bd_pin -dir I -type clk clk_in
  create_bd_pin -dir I -from 0 -to 0 data_in_from_pins_n
  create_bd_pin -dir I -from 0 -to 0 data_in_from_pins_p
  create_bd_pin -dir O locked1
  create_bd_pin -dir I ref_clock
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn
  create_bd_pin -dir I t0_in
  create_bd_pin -dir O t0_out
  create_bd_pin -dir I trigger_in

  # Create instance: ATLASPix_Readout_0, and set properties
  set ATLASPix_Readout_0 [ create_bd_cell -type ip -vlnv cern.ch:user:ATLASPix_Readout:1.0 ATLASPix_Readout_0 ]

  # Create instance: ATLASPix_Readout_DataSync
  create_hier_cell_ATLASPix_Readout_DataSync $hier_obj ATLASPix_Readout_DataSync

  # Create instance: decode_0, and set properties
  set block_name decode
  set block_cell_name decode_0
  if { [catch {set decode_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $decode_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create interface connections
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M08_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins ATLASPix_Readout_0/S00_AXI]

  # Create port connections
  connect_bd_net -net ATLASPix_Readout_0_busy [get_bd_pins busy] [get_bd_pins ATLASPix_Readout_0/busy_out]
  connect_bd_net -net ATLASPix_Readout_0_reset_out [get_bd_pins ATLASPix_Readout_0/reset_out] [get_bd_pins ATLASPix_Readout_DataSync/Op1]
  connect_bd_net -net ATLASPix_Readout_0_t0_out [get_bd_pins t0_out] [get_bd_pins ATLASPix_Readout_0/t0_out]
  connect_bd_net -net ATLASPix_Readout_data_in_to_device [get_bd_pins ATLASPix_Readout_DataSync/data_in_to_device] [get_bd_pins decode_0/datain]
  connect_bd_net -net ATLASPix_Readout_locked1 [get_bd_pins locked1] [get_bd_pins ATLASPix_Readout_0/rx_locked] [get_bd_pins ATLASPix_Readout_DataSync/locked1]
  connect_bd_net -net CLK_1 [get_bd_pins CLK] [get_bd_pins ATLASPix_Readout_0/pdata_clk] [get_bd_pins ATLASPix_Readout_DataSync/CLK]
  connect_bd_net -net DIN_M1_n_1 [get_bd_pins data_in_from_pins_n] [get_bd_pins ATLASPix_Readout_DataSync/data_in_from_pins_n]
  connect_bd_net -net DIN_M1_p_1 [get_bd_pins data_in_from_pins_p] [get_bd_pins ATLASPix_Readout_DataSync/data_in_from_pins_p]
  connect_bd_net -net arst_in_1 [get_bd_pins arst_in] [get_bd_pins ATLASPix_Readout_DataSync/arst_in]
  connect_bd_net -net clk_in_1 [get_bd_pins clk_in] [get_bd_pins ATLASPix_Readout_DataSync/clk_in]
  connect_bd_net -net decode_0_dataout [get_bd_pins ATLASPix_Readout_0/decoded_atp_data_in] [get_bd_pins decode_0/dataout]
  connect_bd_net -net decode_0_dispout [get_bd_pins decode_0/dispin] [get_bd_pins decode_0/dispout]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins s00_axi_aclk] [get_bd_pins ATLASPix_Readout_0/s00_axi_aclk]
  connect_bd_net -net ref_clock_1 [get_bd_pins ref_clock] [get_bd_pins ATLASPix_Readout_DataSync/ref_clock]
  connect_bd_net -net rst_ps7_0_100M_interconnect_aresetn [get_bd_pins s00_axi_aresetn] [get_bd_pins ATLASPix_Readout_0/s00_axi_aresetn]
  connect_bd_net -net t0_in_1 [get_bd_pins t0_in] [get_bd_pins ATLASPix_Readout_0/t0_in]
  connect_bd_net -net trigger_in_1 [get_bd_pins trigger_in] [get_bd_pins ATLASPix_Readout_0/trigger_in]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

  # Create ports
  set ATLASPix_M1ISO_ClkExt_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1ISO_ClkExt_N ]
  set ATLASPix_M1ISO_ClkExt_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1ISO_ClkExt_P ]
  set ATLASPix_M1ISO_ClkRef_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1ISO_ClkRef_N ]
  set ATLASPix_M1ISO_ClkRef_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1ISO_ClkRef_P ]
  set ATLASPix_M1_ClkExt_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1_ClkExt_N ]
  set ATLASPix_M1_ClkExt_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1_ClkExt_P ]
  set ATLASPix_M1_ClkRef_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1_ClkRef_N ]
  set ATLASPix_M1_ClkRef_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M1_ClkRef_P ]
  set ATLASPix_M2_ClkExt_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M2_ClkExt_N ]
  set ATLASPix_M2_ClkExt_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M2_ClkExt_P ]
  set ATLASPix_M2_ClkRef_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M2_ClkRef_N ]
  set ATLASPix_M2_ClkRef_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_M2_ClkRef_P ]
  set CLK_SI_8_N [ create_bd_port -dir I -type clk CLK_SI_8_N ]
  set CLK_SI_8_P [ create_bd_port -dir I -type clk CLK_SI_8_P ]
  set DATA_IN_M1_n [ create_bd_port -dir I -type data DATA_IN_M1_n ]
  set DATA_IN_M1_p [ create_bd_port -dir I -type data DATA_IN_M1_p ]
  set EXT_CK1_M1ISO_N [ create_bd_port -dir O EXT_CK1_M1ISO_N ]
  set EXT_CK1_M1ISO_P [ create_bd_port -dir O EXT_CK1_M1ISO_P ]
  set EXT_CK1_M1_N [ create_bd_port -dir O EXT_CK1_M1_N ]
  set EXT_CK1_M1_P [ create_bd_port -dir O EXT_CK1_M1_P ]
  set EXT_CK1_M2_N [ create_bd_port -dir O EXT_CK1_M2_N ]
  set EXT_CK1_M2_P [ create_bd_port -dir O EXT_CK1_M2_P ]
  set EXT_CK2_M1ISO_N [ create_bd_port -dir O EXT_CK2_M1ISO_N ]
  set EXT_CK2_M1ISO_P [ create_bd_port -dir O EXT_CK2_M1ISO_P ]
  set EXT_CK2_M1_N [ create_bd_port -dir O EXT_CK2_M1_N ]
  set EXT_CK2_M1_P [ create_bd_port -dir O EXT_CK2_M1_P ]
  set EXT_CK2_M2_N [ create_bd_port -dir O EXT_CK2_M2_N ]
  set EXT_CK2_M2_P [ create_bd_port -dir O EXT_CK2_M2_P ]
  set EXT_LDM1ISO_N [ create_bd_port -dir O EXT_LDM1ISO_N ]
  set EXT_LDM1ISO_P [ create_bd_port -dir O EXT_LDM1ISO_P ]
  set EXT_LDM1_N [ create_bd_port -dir O EXT_LDM1_N ]
  set EXT_LDM1_P [ create_bd_port -dir O EXT_LDM1_P ]
  set EXT_LDM2_N [ create_bd_port -dir O EXT_LDM2_N ]
  set EXT_LDM2_P [ create_bd_port -dir O EXT_LDM2_P ]
  set EXT_PULSE_OUT_1_N [ create_bd_port -dir O -from 0 -to 0 -type clk EXT_PULSE_OUT_1_N ]
  set EXT_PULSE_OUT_1_P [ create_bd_port -dir O -from 0 -to 0 -type clk EXT_PULSE_OUT_1_P ]
  set EXT_PULSE_OUT_2_N [ create_bd_port -dir O -from 0 -to 0 -type clk EXT_PULSE_OUT_2_N ]
  set EXT_PULSE_OUT_2_P [ create_bd_port -dir O -from 0 -to 0 -type clk EXT_PULSE_OUT_2_P ]
  set EXT_PULSE_OUT_3_N [ create_bd_port -dir O -from 0 -to 0 -type clk EXT_PULSE_OUT_3_N ]
  set EXT_PULSE_OUT_3_P [ create_bd_port -dir O -from 0 -to 0 -type clk EXT_PULSE_OUT_3_P ]
  set EXT_SIN_M1ISO_N [ create_bd_port -dir O EXT_SIN_M1ISO_N ]
  set EXT_SIN_M1ISO_P [ create_bd_port -dir O EXT_SIN_M1ISO_P ]
  set EXT_SIN_M1_N [ create_bd_port -dir O EXT_SIN_M1_N ]
  set EXT_SIN_M1_P [ create_bd_port -dir O EXT_SIN_M1_P ]
  set EXT_SIN_M2_N [ create_bd_port -dir O EXT_SIN_M2_N ]
  set EXT_SIN_M2_P [ create_bd_port -dir O EXT_SIN_M2_P ]
  set T0_in_N [ create_bd_port -dir I T0_in_N ]
  set T0_in_P [ create_bd_port -dir I T0_in_P ]
  set busy_out [ create_bd_port -dir O busy_out ]
  set ext_ATLASPix_Sync_reset_M1ISO_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1ISO_N ]
  set ext_ATLASPix_Sync_reset_M1ISO_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1ISO_P ]
  set ext_ATLASPix_Sync_reset_M1_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1_N ]
  set ext_ATLASPix_Sync_reset_M1_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1_P ]
  set ext_ATLASPix_Sync_reset_M2_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M2_N ]
  set ext_ATLASPix_Sync_reset_M2_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M2_P ]
  set led0 [ create_bd_port -dir O led0 ]
  set reset [ create_bd_port -dir I -type rst reset ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset
  set trigger_in [ create_bd_port -dir I trigger_in ]

  # Create instance: ATLASPIX_ReadoutBlock
  create_hier_cell_ATLASPIX_ReadoutBlock [current_bd_instance .] ATLASPIX_ReadoutBlock

  # Create instance: ATLASPix_Specific_slow_control
  create_hier_cell_ATLASPix_Specific_slow_control [current_bd_instance .] ATLASPix_Specific_slow_control

  # Create instance: Caribou_control_0, and set properties
  set Caribou_control_0 [ create_bd_cell -type ip -vlnv CERN:user:Caribou_control:1.0 Caribou_control_0 ]

  # Create instance: SignalSyncer_0, and set properties
  set block_name SignalSyncer
  set block_cell_name SignalSyncer_0
  if { [catch {set SignalSyncer_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $SignalSyncer_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.g_Delay {2} \
 ] $SignalSyncer_0

  # Create instance: SyncReset_obuf
  create_hier_cell_SyncReset_obuf [current_bd_instance .] SyncReset_obuf

  # Create instance: ZynqPSandAXIStuff
  create_hier_cell_ZynqPSandAXIStuff [current_bd_instance .] ZynqPSandAXIStuff

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.AXI_DRP {false} \
   CONFIG.CLKIN1_JITTER_PS {100.0} \
   CONFIG.CLKIN2_JITTER_PS {250.0} \
   CONFIG.CLKOUT1_DRIVES {BUFG} \
   CONFIG.CLKOUT1_JITTER {130.958} \
   CONFIG.CLKOUT1_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {100} \
   CONFIG.CLKOUT2_DRIVES {BUFG} \
   CONFIG.CLKOUT2_JITTER {183.243} \
   CONFIG.CLKOUT2_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {20} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_DRIVES {BUFG} \
   CONFIG.CLKOUT3_JITTER {130.958} \
   CONFIG.CLKOUT3_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {100} \
   CONFIG.CLKOUT3_USED {true} \
   CONFIG.CLKOUT4_DRIVES {BUFG} \
   CONFIG.CLKOUT4_JITTER {114.829} \
   CONFIG.CLKOUT4_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {200.000} \
   CONFIG.CLKOUT4_USED {true} \
   CONFIG.CLKOUT5_DRIVES {BUFG} \
   CONFIG.CLKOUT5_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT6_DRIVES {BUFG} \
   CONFIG.CLKOUT6_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT7_DRIVES {BUFG} \
   CONFIG.CLKOUT7_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLK_OUT1_PORT {clk_din} \
   CONFIG.CLK_OUT2_PORT {clk_pdata} \
   CONFIG.CLK_OUT3_PORT {clk_out} \
   CONFIG.CLK_OUT4_PORT {clk_idelay_200} \
   CONFIG.FEEDBACK_SOURCE {FDBK_AUTO} \
   CONFIG.JITTER_SEL {No_Jitter} \
   CONFIG.MMCM_BANDWIDTH {OPTIMIZED} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {10} \
   CONFIG.MMCM_CLKIN1_PERIOD {10.000} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.000} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {10} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {50} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {10} \
   CONFIG.MMCM_CLKOUT3_DIVIDE {5} \
   CONFIG.MMCM_COMPENSATION {ZHOLD} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {4} \
   CONFIG.PHASE_DUTY_CONFIG {false} \
   CONFIG.PRIMITIVE {PLL} \
   CONFIG.PRIM_IN_FREQ {100} \
   CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} \
   CONFIG.RESET_BOARD_INTERFACE {reset} \
   CONFIG.SECONDARY_IN_FREQ {100.000} \
   CONFIG.SECONDARY_SOURCE {Single_ended_clock_capable_pin} \
   CONFIG.USE_DYN_RECONFIG {false} \
   CONFIG.USE_FREQ_SYNTH {true} \
   CONFIG.USE_INCLK_SWITCHOVER {false} \
 ] $clk_wiz_0

  # Create instance: clock_buffers
  create_hier_cell_clock_buffers [current_bd_instance .] clock_buffers

  # Create instance: sync_reset_1_0, and set properties
  set block_name sync_reset_1
  set block_cell_name sync_reset_1_0
  if { [catch {set sync_reset_1_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $sync_reset_1_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.STAGES {2} \
 ] $sync_reset_1_0

  # Create instance: xil_ibufds_1, and set properties
  set block_name xil_ibufds
  set block_cell_name xil_ibufds_1
  if { [catch {set xil_ibufds_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $xil_ibufds_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI6_1 [get_bd_intf_pins ATLASPix_Specific_slow_control/S00_AXI6] [get_bd_intf_pins ZynqPSandAXIStuff/M03_AXI]
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins ATLASPIX_ReadoutBlock/S00_AXI] [get_bd_intf_pins ZynqPSandAXIStuff/M04_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins ZynqPSandAXIStuff/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins ZynqPSandAXIStuff/FIXED_IO]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M00_AXI [get_bd_intf_pins Caribou_control_0/axi] [get_bd_intf_pins ZynqPSandAXIStuff/M00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M01_AXI [get_bd_intf_pins ATLASPix_Specific_slow_control/S00_AXI] [get_bd_intf_pins ZynqPSandAXIStuff/M01_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M02_AXI [get_bd_intf_pins ATLASPix_Specific_slow_control/S00_AXI1] [get_bd_intf_pins ZynqPSandAXIStuff/M02_AXI]

  # Create port connections
  connect_bd_net -net ATLASPIX_ReadoutBlock_t0_out [get_bd_pins ATLASPIX_ReadoutBlock/t0_out] [get_bd_pins SyncReset_obuf/I]
  connect_bd_net -net ATLASPix_Readout_0_busy [get_bd_ports busy_out] [get_bd_pins ATLASPIX_ReadoutBlock/busy]
  connect_bd_net -net ATLASPix_Readout_locked1 [get_bd_ports led0] [get_bd_pins ATLASPIX_ReadoutBlock/locked1]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK1_M1ISO_N [get_bd_ports EXT_CK1_M1ISO_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK1_M1ISO_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK1_M1ISO_P [get_bd_ports EXT_CK1_M1ISO_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK1_M1ISO_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK1_M1_N [get_bd_ports EXT_CK1_M1_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK1_M1_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK1_M1_P [get_bd_ports EXT_CK1_M1_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK1_M1_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK1_M2_N [get_bd_ports EXT_CK1_M2_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK1_M2_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK1_M2_P [get_bd_ports EXT_CK1_M2_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK1_M2_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK2_M1ISO_N [get_bd_ports EXT_CK2_M1ISO_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK2_M1ISO_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK2_M1ISO_P [get_bd_ports EXT_CK2_M1ISO_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK2_M1ISO_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK2_M1_N [get_bd_ports EXT_CK2_M1_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK2_M1_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK2_M1_P [get_bd_ports EXT_CK2_M1_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK2_M1_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK2_M2_N [get_bd_ports EXT_CK2_M2_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK2_M2_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_CK2_M2_P [get_bd_ports EXT_CK2_M2_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CK2_M2_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_LDM1ISO_N [get_bd_ports EXT_LDM1ISO_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_LDM1ISO_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_LDM1ISO_P [get_bd_ports EXT_LDM1ISO_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_LDM1ISO_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_LDM1_N [get_bd_ports EXT_LDM1_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_LDM1_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_LDM1_P [get_bd_ports EXT_LDM1_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_LDM1_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_LDM2_N [get_bd_ports EXT_LDM2_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_LDM2_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_LDM2_P [get_bd_ports EXT_LDM2_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_LDM2_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_SIN_M1ISO_N [get_bd_ports EXT_SIN_M1ISO_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_SIN_M1ISO_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_SIN_M1ISO_P [get_bd_ports EXT_SIN_M1ISO_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_SIN_M1ISO_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_SIN_M1_N [get_bd_ports EXT_SIN_M1_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_SIN_M1_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_SIN_M1_P [get_bd_ports EXT_SIN_M1_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_SIN_M1_P]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_SIN_M2_N [get_bd_ports EXT_SIN_M2_N] [get_bd_pins ATLASPix_Specific_slow_control/EXT_SIN_M2_N]
  connect_bd_net -net ATLASPix_Specific_slow_control_EXT_SIN_M2_P [get_bd_ports EXT_SIN_M2_P] [get_bd_pins ATLASPix_Specific_slow_control/EXT_SIN_M2_P]
  connect_bd_net -net CLK_SI_8_N_1 [get_bd_ports CLK_SI_8_N] [get_bd_pins clk_wiz_0/clk_in1_n]
  connect_bd_net -net CLK_SI_8_P_1 [get_bd_ports CLK_SI_8_P] [get_bd_pins clk_wiz_0/clk_in1_p]
  connect_bd_net -net DIN_M1_n_1 [get_bd_ports DATA_IN_M1_n] [get_bd_pins ATLASPIX_ReadoutBlock/data_in_from_pins_n]
  connect_bd_net -net DIN_M1_p_1 [get_bd_ports DATA_IN_M1_p] [get_bd_pins ATLASPIX_ReadoutBlock/data_in_from_pins_p]
  connect_bd_net -net OBUF_IN1_1 [get_bd_pins ATLASPIX_ReadoutBlock/clk_in] [get_bd_pins clk_wiz_0/clk_din]
  connect_bd_net -net OBUF_IN_1 [get_bd_pins clk_wiz_0/clk_out] [get_bd_pins clock_buffers/OBUF_IN]
  connect_bd_net -net SignalSyncer_0_Data_ob [get_bd_pins ATLASPIX_ReadoutBlock/trigger_in] [get_bd_pins SignalSyncer_0/Data_ob]
  connect_bd_net -net SyncReset_obuf_O_n [get_bd_ports ext_ATLASPix_Sync_reset_M2_N] [get_bd_pins SyncReset_obuf/O_n]
  connect_bd_net -net SyncReset_obuf_O_n1 [get_bd_ports ext_ATLASPix_Sync_reset_M1ISO_N] [get_bd_pins SyncReset_obuf/O_n1]
  connect_bd_net -net SyncReset_obuf_O_n2 [get_bd_ports ext_ATLASPix_Sync_reset_M1_N] [get_bd_pins SyncReset_obuf/O_n2]
  connect_bd_net -net SyncReset_obuf_O_p [get_bd_ports ext_ATLASPix_Sync_reset_M2_P] [get_bd_pins SyncReset_obuf/O_p]
  connect_bd_net -net SyncReset_obuf_O_p1 [get_bd_ports ext_ATLASPix_Sync_reset_M1ISO_P] [get_bd_pins SyncReset_obuf/O_p1]
  connect_bd_net -net SyncReset_obuf_O_p2 [get_bd_ports ext_ATLASPix_Sync_reset_M1_P] [get_bd_pins SyncReset_obuf/O_p2]
  connect_bd_net -net T0_in_N_1 [get_bd_ports T0_in_N] [get_bd_pins xil_ibufds_1/I_n]
  connect_bd_net -net T0_in_P_1 [get_bd_ports T0_in_P] [get_bd_pins xil_ibufds_1/I_p]
  connect_bd_net -net arst_in_1 [get_bd_pins ATLASPIX_ReadoutBlock/arst_in] [get_bd_pins clk_wiz_0/locked]
  connect_bd_net -net clk_wiz_0_PDATA_CLK [get_bd_pins ATLASPIX_ReadoutBlock/CLK] [get_bd_pins ATLASPix_Specific_slow_control/pulser_clk] [get_bd_pins SignalSyncer_0/Clk_ik] [get_bd_pins clk_wiz_0/clk_pdata] [get_bd_pins sync_reset_1_0/clk]
  connect_bd_net -net clk_wiz_0_clk_idelay_200 [get_bd_pins ATLASPIX_ReadoutBlock/ref_clock] [get_bd_pins clk_wiz_0/clk_idelay_200]
  connect_bd_net -net clock_buffers_OBUF_DS_N [get_bd_ports ATLASPix_M1_ClkExt_N] [get_bd_pins clock_buffers/OBUF_DS_N]
  connect_bd_net -net clock_buffers_OBUF_DS_N1 [get_bd_ports ATLASPix_M2_ClkExt_N] [get_bd_pins clock_buffers/OBUF_DS_N1]
  connect_bd_net -net clock_buffers_OBUF_DS_N2 [get_bd_ports ATLASPix_M1ISO_ClkExt_N] [get_bd_pins clock_buffers/OBUF_DS_N2]
  connect_bd_net -net clock_buffers_OBUF_DS_N3 [get_bd_ports ATLASPix_M1ISO_ClkRef_N] [get_bd_pins clock_buffers/OBUF_DS_N3]
  connect_bd_net -net clock_buffers_OBUF_DS_N4 [get_bd_ports ATLASPix_M2_ClkRef_N] [get_bd_pins clock_buffers/OBUF_DS_N4]
  connect_bd_net -net clock_buffers_OBUF_DS_N5 [get_bd_ports ATLASPix_M1_ClkRef_N] [get_bd_pins clock_buffers/OBUF_DS_N5]
  connect_bd_net -net clock_buffers_OBUF_DS_P [get_bd_ports ATLASPix_M1_ClkExt_P] [get_bd_pins clock_buffers/OBUF_DS_P]
  connect_bd_net -net clock_buffers_OBUF_DS_P1 [get_bd_ports ATLASPix_M2_ClkExt_P] [get_bd_pins clock_buffers/OBUF_DS_P1]
  connect_bd_net -net clock_buffers_OBUF_DS_P2 [get_bd_ports ATLASPix_M1ISO_ClkExt_P] [get_bd_pins clock_buffers/OBUF_DS_P2]
  connect_bd_net -net clock_buffers_OBUF_DS_P3 [get_bd_ports ATLASPix_M1ISO_ClkRef_P] [get_bd_pins clock_buffers/OBUF_DS_P3]
  connect_bd_net -net clock_buffers_OBUF_DS_P4 [get_bd_ports ATLASPix_M2_ClkRef_P] [get_bd_pins clock_buffers/OBUF_DS_P4]
  connect_bd_net -net clock_buffers_OBUF_DS_P5 [get_bd_ports ATLASPix_M1_ClkRef_P] [get_bd_pins clock_buffers/OBUF_DS_P5]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins ATLASPIX_ReadoutBlock/s00_axi_aclk] [get_bd_pins ATLASPix_Specific_slow_control/EXT_CFG_CLK] [get_bd_pins ATLASPix_Specific_slow_control/s00_axi_aclk] [get_bd_pins Caribou_control_0/aclk] [get_bd_pins ZynqPSandAXIStuff/FCLK_CLK0]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins clk_wiz_0/reset]
  connect_bd_net -net rst_ps7_0_100M_interconnect_aresetn [get_bd_pins ATLASPIX_ReadoutBlock/s00_axi_aresetn] [get_bd_pins Caribou_control_0/aresetN] [get_bd_pins ZynqPSandAXIStuff/ARESETN]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins ATLASPix_Specific_slow_control/s00_axi_aresetn1] [get_bd_pins ZynqPSandAXIStuff/M03_ARESETN]
  connect_bd_net -net t0_in_1 [get_bd_pins ATLASPIX_ReadoutBlock/t0_in] [get_bd_pins sync_reset_1_0/rst_out]
  connect_bd_net -net trigger_in_1 [get_bd_ports trigger_in] [get_bd_pins ATLASPix_Specific_slow_control/trigger_in] [get_bd_pins SignalSyncer_0/Data_ib]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_N [get_bd_ports EXT_PULSE_OUT_1_N] [get_bd_pins ATLASPix_Specific_slow_control/OBUF_DS_N14]
  connect_bd_net -net util_ds_buf_7_OBUF_DS_P [get_bd_ports EXT_PULSE_OUT_1_P] [get_bd_pins ATLASPix_Specific_slow_control/OBUF_DS_P14]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_N [get_bd_ports EXT_PULSE_OUT_2_N] [get_bd_pins ATLASPix_Specific_slow_control/OBUF_DS_N12]
  connect_bd_net -net util_ds_buf_8_OBUF_DS_P [get_bd_ports EXT_PULSE_OUT_2_P] [get_bd_pins ATLASPix_Specific_slow_control/OBUF_DS_P12]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_N [get_bd_ports EXT_PULSE_OUT_3_N] [get_bd_pins ATLASPix_Specific_slow_control/OBUF_DS_N13]
  connect_bd_net -net util_ds_buf_9_OBUF_DS_P [get_bd_ports EXT_PULSE_OUT_3_P] [get_bd_pins ATLASPix_Specific_slow_control/OBUF_DS_P13]
  connect_bd_net -net xil_ibufds_1_O [get_bd_pins sync_reset_1_0/arst_in] [get_bd_pins xil_ibufds_1/O]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins ATLASPix_Specific_slow_control/Data_ib] [get_bd_pins ATLASPix_Specific_slow_control/MON1_IN] [get_bd_pins ATLASPix_Specific_slow_control/MON2_IN] [get_bd_pins xlconstant_0/dout]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x43C00000 [get_bd_addr_spaces ZynqPSandAXIStuff/processing_system7_0/Data] [get_bd_addr_segs ATLASPix_Specific_slow_control/ATLASPix_PulseInjectionCounter_0/S00_AXI/S00_AXI_reg] SEG_ATLASPix_PulseInjectionCounter_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C10000 [get_bd_addr_spaces ZynqPSandAXIStuff/processing_system7_0/Data] [get_bd_addr_segs ATLASPix_Specific_slow_control/ATLASPix_Pulser_0/S00_AXI/S00_AXI_reg] SEG_ATLASPix_Pulser_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C70000 [get_bd_addr_spaces ZynqPSandAXIStuff/processing_system7_0/Data] [get_bd_addr_segs ATLASPIX_ReadoutBlock/ATLASPix_Readout_0/S00_AXI/S00_AXI_reg] SEG_ATLASPix_Readout_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C20000 [get_bd_addr_spaces ZynqPSandAXIStuff/processing_system7_0/Data] [get_bd_addr_segs ATLASPix_Specific_slow_control/ATLASPix_SR_FSM_0/S00_AXI/S00_AXI_reg] SEG_ATLASPix_SR_FSM_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C30000 [get_bd_addr_spaces ZynqPSandAXIStuff/processing_system7_0/Data] [get_bd_addr_segs Caribou_control_0/interface_aximm/reg0] SEG_Caribou_control_0_reg0


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


