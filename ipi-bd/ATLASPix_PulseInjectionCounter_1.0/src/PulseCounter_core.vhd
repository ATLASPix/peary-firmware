----------------------------------------------------------------------------------
-- Company: BNL
-- Engineer: Hongbin
-- 
-- Create Date: 11/05/2015 06:10:29 PM
-- Design Name: 
-- Module Name: ccpd_inj_mon_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PulseCounter_core is
  PORT(
        clk  :in std_logic;
        rst  :in std_logic;
        
        cnt_rst:in std_logic_vector(3 downto 0);
        
        mon1_in:in std_logic;
        mon2_in:in std_logic;
        mon3_in:in std_logic;
       
        inj_in:in std_logic;
        
        inj_cnt_out :out std_logic_vector(15 downto 0);
        mon1_cnt_out :out std_logic_vector(15 downto 0);
        mon2_cnt_out :out std_logic_vector(15 downto 0);
        mon3_cnt_out :out std_logic_vector(15 downto 0)       
        );
end PulseCounter_core;

architecture Behavioral of PulseCounter_core is
signal inj_cnt_i : integer := 0;
signal mon1_cnt_i : integer := 0;
signal mon2_cnt_i : integer := 0;
signal mon3_cnt_i : integer := 0;

signal inj_in_sync  :std_logic;
signal inj_in_sync_1:std_logic;

signal mon1_in_sync   :std_logic;
signal mon1_in_sync_1 :std_logic;

signal mon2_in_sync   :std_logic;
signal mon2_in_sync_1 :std_logic;

signal mon3_in_sync   :std_logic;
signal mon3_in_sync_1 :std_logic;

--attribute MARK_DEBUG : string;
--attribute MARK_DEBUG of inj_in_sync,mon_in_sync,inj_cnt_i,mon_cnt_i : signal is "TRUE";


begin

input_sync:process(clk, rst)
begin
if rst = '1' then
  inj_in_sync <= '0';
  inj_in_sync_1 <= '0';
  mon1_in_sync <= '0';
  mon1_in_sync_1 <= '0';
  mon2_in_sync <= '0';
  mon2_in_sync_1 <= '0';  
  mon3_in_sync <= '0';
  mon3_in_sync_1 <= '0';
    
elsif rising_edge(clk) then 
  inj_in_sync   <= inj_in;
  inj_in_sync_1 <= inj_in_sync;
  mon1_in_sync   <= mon1_in;
  mon1_in_sync_1 <= mon1_in_sync;
  mon2_in_sync   <= mon2_in;
  mon2_in_sync_1 <= mon2_in_sync;
  mon3_in_sync   <= mon3_in;
  mon3_in_sync_1 <= mon3_in_sync;
    
  
end if;
end process;

inj_counter:process(clk,rst)
begin
if rst = '1' then
  inj_cnt_i <= 0;

elsif cnt_rst(0) = '1' then
  inj_cnt_i <= 0;

elsif rising_edge(clk) then
  if inj_in_sync = '1' and inj_in_sync_1 = '0' then
    inj_cnt_i <= inj_cnt_i + 1;
  end if;
end if;
end process;


mon1_counter:process(clk,rst)
begin
if rst = '1' then
  mon1_cnt_i <= 0;
elsif cnt_rst(1) = '1' then
  mon1_cnt_i <= 0;
elsif rising_edge(clk) then
  if mon1_in_sync = '1' and mon1_in_sync_1 = '0' then
    mon1_cnt_i <= mon1_cnt_i + 1;
  end if;
end if;
end process;


mon2_counter:process(clk,rst)
begin
if rst = '1' then
  mon2_cnt_i <= 0;
elsif cnt_rst(2) = '1' then
  mon2_cnt_i <= 0;
elsif rising_edge(clk) then
  if mon2_in_sync = '1' and mon2_in_sync_1 = '0' then
    mon2_cnt_i <= mon2_cnt_i + 1;
  end if;
end if;
end process;



mon3_counter:process(clk,rst)
begin
if rst = '1' then
  mon3_cnt_i <= 0;
elsif cnt_rst(3) = '1' then
  mon3_cnt_i <= 0;
elsif rising_edge(clk) then
  if mon3_in_sync = '1' and mon3_in_sync_1 = '0' then
    mon3_cnt_i <= mon3_cnt_i + 1;
  end if;
end if;
end process;


inj_cnt_out  <= STD_LOGIC_VECTOR(TO_UNSIGNED(inj_cnt_i,16));
mon1_cnt_out  <= STD_LOGIC_VECTOR(TO_UNSIGNED(mon1_cnt_i,16));  
mon2_cnt_out  <= STD_LOGIC_VECTOR(TO_UNSIGNED(mon2_cnt_i,16));  
mon3_cnt_out  <= STD_LOGIC_VECTOR(TO_UNSIGNED(mon3_cnt_i,16));  
 
end Behavioral;
