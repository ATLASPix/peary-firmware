----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.09.2017 09:45:21
-- Design Name: 
-- Module Name: CounterSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CounterSim is
--  Port ( );
end CounterSim;

architecture Behavioral of CounterSim is

constant PERIOD : time := 6.25ns;

signal global_reset	: std_logic;
signal cnt_rst    : std_logic_vector(1 downto 0);
signal inj_cnt_out_buf : std_logic_vector(15 downto 0);
signal mon1_cnt_out_buf : std_logic_vector(15 downto 0);
signal mon2_cnt_out_buf : std_logic_vector(15 downto 0);
signal mon3_cnt_out_buf : std_logic_vector(15 downto 0);

signal clk      :std_logic; -- H35DEMO config. LD_CMOS
signal EXT_MON1_IN : std_logic;
signal EXT_MON2_IN : std_logic;
signal EXT_MON3_IN : std_logic;

signal EXT_INJ_IN : std_logic;

begin     

clk_process : process
   begin
       clk <= '0';
       wait for PERIOD /2; 
       clk <= '1';
       wait for PERIOD/2;   
end process;




ATLASPix_PulseCounter:entity  work.PulseCounter_wrapper
    port map (
            CLK  => clk,
            RST  => global_reset,
    
            CNT_RST => cnt_rst,
    
            MON1_IN => EXT_MON1_IN,
            MON2_IN => EXT_MON2_IN,
            MON3_IN => EXT_MON3_IN,
            
            INJ_IN => EXT_INJ_IN,
    
            INJ_CNT_OUT => inj_cnt_out_buf,
            MON1_CNT_OUT => mon1_cnt_out_buf,
            MON2_CNT_OUT => mon2_cnt_out_buf,
            MON3_CNT_OUT => mon3_cnt_out_buf
           
    );
    
    
      stim_proc : process
    begin
       EXT_MON1_IN <='0';
       global_reset <= '1';
       cnt_rst <= B"11";
       wait for 100 ns;
       global_reset <= '0';
       cnt_rst <= B"00";
       EXT_MON1_IN <='0';
       wait for 77 ns;
       EXT_MON1_IN <='1';
       wait for 100 ns;
       EXT_MON1_IN <='0';
       wait for 105 ns;
       EXT_MON2_IN <='1';
       wait for 66 ns;
       EXT_MON2_IN <='0';       
       wait for 57 ns;
       EXT_MON1_IN <='1';
       wait for 110 ns;
       EXT_MON1_IN <='0';

       wait for 2000000000 ns;

    end process;
      

end Behavioral;
