----------------------------------------------------------------------------------
-- Company: BNL
-- Engineer: Hongbin
-- 
-- Create Date: 11/05/2015 06:10:29 PM
-- Design Name: 
-- Module Name: ccpd_inj_mon_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ccpd_inj_mon_counter is
  Port ( 
        clk  :in std_logic;
        rst  :in std_logic;
        
        cnt_rst:in std_logic_vector(1 downto 0);
        
        mon_in:in std_logic;
        inj_in:in std_logic;
        
        inj_cnt_out :out std_logic_vector(15 downto 0);
        mon_cnt_out :out std_logic_vector(15 downto 0)
        );
end ccpd_inj_mon_counter;

architecture Behavioral of ccpd_inj_mon_counter is
signal inj_cnt_i : integer := 0;
signal mon_cnt_i : integer := 0;

signal inj_in_sync  :std_logic;
signal inj_in_sync_1:std_logic;

signal mon_in_sync   :std_logic;
signal mon_in_sync_1 :std_logic;

--attribute MARK_DEBUG : string;
--attribute MARK_DEBUG of inj_in_sync,mon_in_sync,inj_cnt_i,mon_cnt_i : signal is "TRUE";


begin

input_sync:process(clk, rst)
begin
if rst = '1' then
  inj_in_sync <= '0';
  inj_in_sync_1 <= '0';
  mon_in_sync <= '0';
  mon_in_sync_1 <= '0';
elsif rising_edge(clk) then 
  inj_in_sync   <= inj_in;
  inj_in_sync_1 <= inj_in_sync;
  mon_in_sync   <= mon_in;
  mon_in_sync_1 <= mon_in_sync;
end if;
end process;

inj_counter:process(clk,rst)
begin
if rst = '1' then
  inj_cnt_i <= 0;

elsif cnt_rst(0) = '1' then
  inj_cnt_i <= 0;

elsif rising_edge(clk) then
  if inj_in_sync = '1' and inj_in_sync_1 = '0' then
    inj_cnt_i <= inj_cnt_i + 1;
  end if;
end if;
end process;


mon_counter:process(clk,rst)
begin
if rst = '1' then
  mon_cnt_i <= 0;
elsif cnt_rst(1) = '1' then
  mon_cnt_i <= 0;
elsif rising_edge(clk) then
  if mon_in_sync = '1' and mon_in_sync_1 = '0' then
    mon_cnt_i <= mon_cnt_i + 1;
  end if;
end if;
end process;

inj_cnt_out  <= STD_LOGIC_VECTOR(TO_UNSIGNED(inj_cnt_i,16));
mon_cnt_out  <= STD_LOGIC_VECTOR(TO_UNSIGNED(mon_cnt_i,16));  
 
end Behavioral;
