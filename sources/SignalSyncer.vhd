--============================================================================================\\
--##################################   Module Information   ##################################\\
--============================================================================================\\
--
-- Company: CERN (BE-BI)
--
-- File Name: SignalSyncer.vhd
--
-- File versions history:
--
--  DATE        VERSION  AUTHOR                            DESCRIPTION
--  12.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
--
-- Language: VHDL
--
-- Targeted device:
--
--     - Vendor:  agnostic
--     - Model:   agnostic
--
-- Description:
--      Signal Synchronizer
--      
--      Synchronizes several distinct signals in CDC
--      
--      !!! Cannot be used for synchronizing a bus !!!
--      Use another/proper synchronization for bus signals.
--
--============================================================================================\\
--############################################################################################\\
--============================================================================================\\

library ieee;
use ieee.std_logic_1164.all;

entity SignalSyncer is
    generic (
        g_Width: integer := 1;
        g_Delay: integer := 5  -- at least 2
    );
    port (
        Clk_ik: in std_logic;
        Data_ib: in std_logic_vector(g_Width-1 downto 0);
        Data_ob: out std_logic_vector(g_Width-1 downto 0)
    );
end entity;

-- make Altera Quartus quiet regarding unknown (Xilinx) attributes:
-- altera message_off 10335
architecture syn of SignalSyncer is

    subtype t_Stage is std_logic_vector(g_Width-1 downto 0);
    type t_StageArray is array (natural range <>) of t_Stage;

    signal FirstStage_b: t_Stage;
    signal OtherStages_b: t_StageArray(0 to g_Delay-1);

    -- Xilinx attributes
    attribute ASYNC_REG: string;
    attribute ASYNC_REG of FirstStage_b: signal is "TRUE";
    attribute ASYNC_REG of OtherStages_b: signal is "TRUE";
    
    -- Altera attributes
    attribute altera_attribute: string;
    attribute altera_attribute of FirstStage_b: signal is "-name SYNCHRONIZER_IDENTIFICATION FORCED_IF_ASYNCHRONOUS; -name DONT_MERGE_REGISTER ON; -name PRESERVE_REGISTER ON; -name SDC_STATEMENT ""set_false_path -to [get_keepers {*|SignalSyncer:*|FirstStage_b*}]"" ";
    attribute altera_attribute of OtherStages_b: signal is "-name SYNCHRONIZER_IDENTIFICATION FORCED_IF_ASYNCHRONOUS; -name DONT_MERGE_REGISTER ON; -name PRESERVE_REGISTER ON";

begin
    
    assert (g_Delay >= 2)
        report "g_Delay has to be >= 2!"
        severity failure;

    pDelay: process (Clk_ik) begin
        if rising_edge(Clk_ik) then
            FirstStage_b <= Data_ib;
            OtherStages_b(0) <= FirstStage_b;
            for i in 1 to g_Delay-1 loop
                OtherStages_b(i) <= OtherStages_b(i-1);
            end loop;
        end if;
    end process pDelay;

    Data_ob <= OtherStages_b(g_Delay-1);

end architecture;