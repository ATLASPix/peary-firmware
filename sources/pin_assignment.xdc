set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets CLICpix2_SPI_transceiver_rx]


#TLU signals
#set_property PACKAGE_PIN AG21 [get_ports TLU_CLK_p]
#set_property PACKAGE_PIN AH21 [get_ports TLU_CLK_n]
#set_property PACKAGE_PIN AH23 [get_ports TLU_TRG_p]
#set_property PACKAGE_PIN AH24 [get_ports TLU_TRG_n]
#set_property PACKAGE_PIN AD21 [get_ports TLU_BSY_p]
#set_property PACKAGE_PIN AE21 [get_ports TLU_BSY_n]
#set_property PACKAGE_PIN AA22 [get_ports TLU_RST_p]
#set_property PACKAGE_PIN AA23 [get_ports TLU_RST_n]

#Clock signal from SI5345
#set_property PACKAGE_PIN AE22 [get_ports SI5345_CLK_OUT8_clk_p]
#set_property PACKAGE_PIN AF22 [get_ports SI5345_CLK_OUT8_clk_n]
#set_property DIFF_TERM true [get_ports SI5345_CLK_OUT8_*]
#set_property IOSTANDARD LVDS_25 [get_ports SI5345_CLK_OUT8_*]


#connect_debug_port dbg_hub/clk [get_nets clk_wiz_1_transceiver_rx]


#Matrix 2
set_property PACKAGE_PIN AH26 [get_ports EXT_SIN_M2_P]
set_property PACKAGE_PIN AH27 [get_ports EXT_SIN_M2_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_SIN_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_SIN_M2_N]

set_property PACKAGE_PIN AK28 [get_ports EXT_CK1_M2_N]
set_property PACKAGE_PIN AK27 [get_ports EXT_CK1_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK1_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK1_M2_N]

set_property PACKAGE_PIN AH29 [get_ports EXT_CK2_M2_N]
set_property PACKAGE_PIN AH28 [get_ports EXT_CK2_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK2_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK2_M2_N]

# Matrix 1

set_property PACKAGE_PIN AE25 [get_ports EXT_SIN_M1_P]
set_property PACKAGE_PIN AF25 [get_ports EXT_SIN_M1_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_SIN_M1_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_SIN_M1_N]

set_property PACKAGE_PIN AD25 [get_ports EXT_CK1_M1_P]
set_property PACKAGE_PIN AE26 [get_ports EXT_CK1_M1_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK1_M1_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK1_M1_N]

set_property PACKAGE_PIN AC29 [get_ports EXT_CK2_M1_P]
set_property PACKAGE_PIN AD29 [get_ports EXT_CK2_M1_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK2_M1_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK2_M1_N]


#Matrix 1 ISO
set_property PACKAGE_PIN P23 [get_ports EXT_SIN_M1ISO_P]
set_property PACKAGE_PIN P24 [get_ports EXT_SIN_M1ISO_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_SIN_M1ISO_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_SIN_M1ISO_N]

set_property PACKAGE_PIN N26 [get_ports EXT_CK1_M1ISO_P]
set_property PACKAGE_PIN N27 [get_ports EXT_CK1_M1ISO_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK1_M1ISO_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK1_M1ISO_N]

set_property PACKAGE_PIN P21 [get_ports EXT_CK2_M1ISO_P]
set_property PACKAGE_PIN R21 [get_ports EXT_CK2_M1ISO_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK2_M1ISO_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_CK2_M1ISO_N]



#Load signals

set_property PACKAGE_PIN AG29 [get_ports EXT_LDM2_N]
set_property PACKAGE_PIN AF29 [get_ports EXT_LDM2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_LDM2_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_LDM2_N]


set_property PACKAGE_PIN N29 [get_ports EXT_LDM1_P]
set_property PACKAGE_PIN P29 [get_ports EXT_LDM1_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_LDM1_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_LDM1_N]

set_property PACKAGE_PIN P30 [get_ports EXT_LDM1ISO_P]
set_property PACKAGE_PIN R30 [get_ports EXT_LDM1ISO_N]
set_property IOSTANDARD LVDS_25 [get_ports EXT_LDM1ISO_P]
set_property IOSTANDARD LVDS_25 [get_ports EXT_LDM1ISO_N]


#clocks

set_property PACKAGE_PIN AD23 [get_ports {ATLASPix_M1_ClkExt_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1_ClkExt_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1_ClkExt_N[0]}]

set_property PACKAGE_PIN AF20 [get_ports {ATLASPix_M2_ClkExt_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M2_ClkExt_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M2_ClkExt_N[0]}]

set_property PACKAGE_PIN AJ26 [get_ports {ATLASPix_M1ISO_ClkExt_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1ISO_ClkExt_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1ISO_ClkExt_N[0]}]

set_property PACKAGE_PIN AF15 [get_ports {ATLASPix_M1_ClkRef_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1_ClkRef_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1_ClkRef_N[0]}]

set_property PACKAGE_PIN AG22 [get_ports {ATLASPix_M2_ClkRef_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M2_ClkRef_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M2_ClkRef_N[0]}]

set_property PACKAGE_PIN AJ28 [get_ports {ATLASPix_M1ISO_ClkRef_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1ISO_ClkRef_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ATLASPix_M1ISO_ClkRef_N[0]}]


#M2
set_property PACKAGE_PIN AG19 [get_ports {ext_ATLASPix_Sync_reset_M2_N[0]}]
set_property PACKAGE_PIN AF19 [get_ports {ext_ATLASPix_Sync_reset_M2_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ext_ATLASPix_Sync_reset_M2_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ext_ATLASPix_Sync_reset_M2_N[0]}]

#set_property PACKAGE_PIN AJ15 [get_ports ext_ATLASPix_Trigger_M2_clk_p]
#set_property PACKAGE_PIN AK15 [get_ports ext_ATLASPix_Trigger_M2_clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M2_clk_p]
#set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M2_clk_n]



#M1
set_property PACKAGE_PIN AH19 [get_ports {ext_ATLASPix_Sync_reset_M1_P[0]}]
set_property PACKAGE_PIN AJ19 [get_ports {ext_ATLASPix_Sync_reset_M1_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ext_ATLASPix_Sync_reset_M1_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ext_ATLASPix_Sync_reset_M1_N[0]}]

#set_property PACKAGE_PIN AH19 [get_ports t0_out_p]
#set_property PACKAGE_PIN AJ19 [get_ports t0_out_n]
#set_property IOSTANDARD LVDS_25 [get_ports t0_out_p]
#set_property IOSTANDARD LVDS_25 [get_ports t0_out_n]

#set_property PACKAGE_PIN AF23 [get_ports ext_ATLASPix_Trigger_M1_clk_p]
#set_property PACKAGE_PIN AF24 [get_ports ext_ATLASPix_Trigger_M1_clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1_clk_p]
#set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1_clk_n]



#M1ISO
set_property PACKAGE_PIN AA24 [get_ports {ext_ATLASPix_Sync_reset_M1ISO_P[0]}]
set_property PACKAGE_PIN AB24 [get_ports {ext_ATLASPix_Sync_reset_M1ISO_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ext_ATLASPix_Sync_reset_M1ISO_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ext_ATLASPix_Sync_reset_M1ISO_N[0]}]

#set_property PACKAGE_PIN AB15 [get_ports ext_ATLASPix_Trigger_M1ISO_clk_p]
#set_property PACKAGE_PIN AB14 [get_ports ext_ATLASPix_Trigger_M1ISO_clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1ISO_clk_p]
#set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1ISO_clk_n]





#set_property PACKAGE_PIN T30 [get_ports EXT_PULSE_OUT_1_P]
set_property PACKAGE_PIN AE17 [get_ports {EXT_PULSE_OUT_1_N[0]}]
set_property PACKAGE_PIN AE18 [get_ports {EXT_PULSE_OUT_1_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {EXT_PULSE_OUT_1_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {EXT_PULSE_OUT_1_N[0]}]

set_property PACKAGE_PIN W30 [get_ports {EXT_PULSE_OUT_2_N[0]}]
set_property PACKAGE_PIN W29 [get_ports {EXT_PULSE_OUT_2_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {EXT_PULSE_OUT_2_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {EXT_PULSE_OUT_2_N[0]}]


set_property PACKAGE_PIN AB27 [get_ports {EXT_PULSE_OUT_3_P[0]}]
set_property PACKAGE_PIN AC27 [get_ports {EXT_PULSE_OUT_3_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {EXT_PULSE_OUT_3_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {EXT_PULSE_OUT_3_N[0]}]


#set_property PACKAGE_PIN AD18 [get_ports MON1_IN]
#set_property PACKAGE_PIN AD19 [get_ports MON2_IN]
#set_property IOSTANDARD LVCMOS25 [get_ports MON1_IN]
#set_property IOSTANDARD LVCMOS25 [get_ports MON2_IN]





#set_property PACKAGE_PIN AJ18 [get_ports fmc_i2c_sda_io]
#set_property IOSTANDARD LVCMOS25 [get_ports fmc_i2c_sda_io]

#set_property PACKAGE_PIN AJ14 [get_ports fmc_i2c_scl_io]
#set_property IOSTANDARD LVCMOS25 [get_ports fmc_i2c_scl_io]


#DATA from Matrices
#M1
set_property PACKAGE_PIN AG24 [get_ports DATA_IN_M1_p]
set_property PACKAGE_PIN AG25 [get_ports DATA_IN_M1_n]
set_property IOSTANDARD LVDS_25 [get_ports DATA_IN_M1_p]
set_property IOSTANDARD LVDS_25 [get_ports DATA_IN_M1_n]


#M1ISO
#set_property PACKAGE_PIN Y22 [get_ports DATA_IN_M1ISO_clk_p]
#set_property PACKAGE_PIN Y23 [get_ports DATA_IN_M1ISO_clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports DATA_IN_M1ISO_clk_p]
#set_property IOSTANDARD LVDS_25 [get_ports DATA_IN_M1ISO_clk_n]

#M2
#set_property PACKAGE_PIN AJ23 [get_ports DATA_IN_M2_clk_p]
#set_property PACKAGE_PIN AJ24 [get_ports DATA_IN_M2_clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports DATA_IN_M2_clk_p]
#set_property IOSTANDARD LVDS_25 [get_ports DATA_IN_M2_clk_n]


#Fast Clock out
#set_property PACKAGE_PIN AG21 [get_ports FAST_CLK_OUT_P]
#set_property PACKAGE_PIN AH21 [get_ports FAST_CLK_OUT_N]
#set_property IOSTANDARD LVDS_25 [get_ports FAST_CLK_OUT_P]
#set_property IOSTANDARD LVDS_25 [get_ports FAST_CLK_OUT_N]

#LEDs

set_property PACKAGE_PIN A17 [get_ports led0]
set_property IOSTANDARD LVCMOS15 [get_ports led0]

#set_property PACKAGE_PIN G2 [get_ports leds[1]]
#set_property IOSTANDARD LVCMOS15 [get_ports leds[1]]



#DEBUG pins at PMOD1
#GPIO PMOD1

#set_property PACKAGE_PIN AJ21 [get_ports debug_out[0]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[0]]
#set_property PACKAGE_PIN AK21 [get_ports debug_out[1]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[1]]
#set_property PACKAGE_PIN AB21 [get_ports debug_out[2]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[2]]
#set_property PACKAGE_PIN AB16 [get_ports debug_out[3]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[3]]
#set_property PACKAGE_PIN  Y20 [get_ports debug_out[4]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[4]]
#set_property PACKAGE_PIN AA20 [get_ports debug_out[5]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[5]]
#set_property PACKAGE_PIN AC18 [get_ports debug_out[6]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[6]]
#set_property PACKAGE_PIN AC19 [get_ports debug_out[7]]
#set_property IOSTANDARD LVCMOS25 [get_ports debug_out[7]]


# pulse injection
#set_property PACKAGE_PIN ZZ99 [get_ports pulse_gen]
#set_property IOSTANDARD LVCMOS25 [get_ports pulse_gen]


# trigger and busy for ATLASPIX telescope
set_property PACKAGE_PIN AD18 [get_ports trigger_in]
set_property IOSTANDARD LVCMOS25 [get_ports trigger_in]
set_property PACKAGE_PIN AD19 [get_ports busy_out]
set_property IOSTANDARD LVCMOS25 [get_ports busy_out]


#T0 and clock for SPIDR telescope
set_property IOSTANDARD LVDS_25 [get_ports T0_in_P]
set_property PACKAGE_PIN AA22 [get_ports T0_in_P]
set_property PACKAGE_PIN AA23 [get_ports T0_in_N]
set_property IOSTANDARD LVDS_25 [get_ports T0_in_N]

#set_property IOSTANDARD LVDS_25 [get_ports CLK_TLU_P]
#set_property PACKAGE_PIN AG21 [get_ports CLK_TLU_P]
#set_property PACKAGE_PIN AH21 [get_ports CLK_TLU_N]
#set_property IOSTANDARD LVDS_25 [get_ports CLK_TLU_N]


#Clock signal from SI5345
set_property PACKAGE_PIN AE22 [get_ports CLK_SI_8_P]
set_property PACKAGE_PIN AF22 [get_ports CLK_SI_8_N]
set_property DIFF_TERM true [get_ports CLK_SI_8_*]
set_property IOSTANDARD LVDS_25 [get_ports CLK_SI_8_*]

#PCIE
#set_property PACKAGE_PIN AK22 [get_ports PCIE_WAKE_B_LS]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_WAKE_B_LS]
#set_property PACKAGE_PIN AK23 [get_ports PCIE_PERST_LS]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_PERST_LS]
#set_property PACKAGE_PIN N7 [get_ports PCIE_CLK_QO_N]
#set_property PACKAGE_PIN N8 [get_ports PCIE_CLK_QO_P]
#set_property PACKAGE_PIN P5 [get_ports PCIE_RX0_N]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX0_N]
#set_property PACKAGE_PIN P6 [get_ports PCIE_RX0_P]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX0_P]
#set_property PACKAGE_PIN T5 [get_ports PCIE_RX1_N]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX1_N]
#set_property PACKAGE_PIN T6 [get_ports PCIE_RX1_P]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX1_P]
#set_property PACKAGE_PIN U3 [get_ports PCIE_RX2_N]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX2_N]
#set_property PACKAGE_PIN U4 [get_ports PCIE_RX2_P]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX2_P]
#set_property PACKAGE_PIN V5 [get_ports PCIE_RX3_N]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX3_N]
#set_property PACKAGE_PIN V6 [get_ports PCIE_RX3_P]
#set_property IOSTANDARD LVCMOS25 [get_ports PCIE_RX3_P]
#set_property PACKAGE_PIN N3 [get_ports PCIE_TX0_N]
#set_property PACKAGE_PIN N4 [get_ports PCIE_TX0_P]
#set_property PACKAGE_PIN P1 [get_ports PCIE_TX1_N]
#set_property PACKAGE_PIN P2 [get_ports PCIE_TX1_P]
#set_property PACKAGE_PIN R3 [get_ports PCIE_TX2_N]
#set_property PACKAGE_PIN R4 [get_ports PCIE_TX2_P]
#set_property PACKAGE_PIN T1 [get_ports PCIE_TX3_N]
#set_property PACKAGE_PIN T2 [get_ports PCIE_TX3_P]


