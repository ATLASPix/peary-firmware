`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.03.2018 10:59:13
// Design Name: 
// Module Name: xil_obufds
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module xil_ibufgds # (
    parameter DIFF_TERM    = "FALSE",
    parameter IBUF_LOW_PWR = "TRUE",
    parameter IOSTANDARD   = "DEFAULT"
)
(
    input  I_p,
    input  I_n,
    output O
    );
/*
    parameter DIFF_TERM    = "FALSE";
    parameter IBUF_LOW_PWR = "TRUE";
    parameter IOSTANDARD   = "DEFAULT";
*/

    wire Os;

    assign O = Os;
   
   IBUFGDS #(
      .DIFF_TERM    (DIFF_TERM),     // Differential Termination
      .IBUF_LOW_PWR (IBUF_LOW_PWR),  // Low power="TRUE", Highest performance="FALSE" 
      .IOSTANDARD   (IOSTANDARD)     // Specify the input I/O standard
   ) IBUFGDS_inst (
      .O  (Os),  // Buffer output
      .I  (I_p), // Diff_p buffer input (connect directly to top-level port)
      .IB (I_n)  // Diff_n buffer input (connect directly to top-level port)
   );

					
					


endmodule
